var aventurier = Aventurier.getLaSession();
aventurier.phrases = aventurier.getPhrases();
aventurier.salon = aventurier.getSalon();

console.log(aventurier.phrases, aventurier.salon);

let phrasesConnues;
let descriptionSalon;
let reservoir;

$(document).ready(function() {
    descriptionSalon = $("#description_salon")[0];
    getTextesPhrases(aventurier.phrases, setupPage, true);
    getTextesPhrases(aventurier.salon, setupSalon, true)

    $(document).on("click", ".ramassable", togglePhrase);
});

/*
    setup du DOM :
    en-tête
    reservoir
        murs
        fenetres
        sols
        ...
    en-tête
    reservoir
        murs
        fenetres
        sols
        ...
    ...
    salon
*/
function setupPage(data) {
    phrasesConnues = data;
    console.log(phrasesConnues)

    //Reservoir de phrases possédées
    reservoir = $('#reservoir');
    for (let key in phrasesConnues) {
        if (phrasesConnues[key].length == 0) continue;

        //En-tête : type de phrase (mur, sol, fenetre...)
        let titre = jQuery("<h2>").html(key).appendTo(reservoir);

        //Div contenant les phrases de ce type
        let group = jQuery("<div>", {
            id: key
        }).appendTo(reservoir);

        //Affichage des phrases.
        for (let p of phrasesConnues[key]) {
            console.log(p)
            DOMPhrase(p.id, key, p.text, group);
        }
    }
}

function setupSalon(data) {
    console.log(data)
    if (aventurier.salon == null) return;
    for (let key in data) {
        for (let p of data[key]) {
            DOMPhrase(p.id, key, p.text, descriptionSalon);
        }
    }
}

function DOMPhrase(id, key, text, group) {
    let paragraph = jQuery("<p>", {
        id: id,
        class: "ramassable",
        value: key
    }).html(text).appendTo(group);
}

/*
    Ajoute phrase au salon ou la retire, selon sa situation
*/
function togglePhrase(e) {
    let phrase = e.target;
    if (phrase.parentNode != descriptionSalon) {
        descriptionSalon.append(phrase);
        console.log(phrase)
        aventurier.salon.push(parseInt(phrase.id));
    } else {
        //trouver dans reservoir l'enfant dont l'id == la value de la phrase
        for (let c of reservoir[0].children) {
            if (c.id == phrase.getAttribute("value")) {
                c.append(phrase);
                aventurier.salon.splice(aventurier.salon.indexOf(c.id), 1);
                break;
            }
        }
    }

    aventurier.sauvegardeSalon();
    aventurier.sauvegardePhrases();
}