var textes;

$(document).ready(function() {
    //$('#page').css('visibility', 'visible');
    $('#page').addClass("chargee");

    $(document).on('click', '#quitterMaison', function() { quitterMaison() });
    $(document).on('click', '.fermerPopUp', function() { fermerPopUp() });

    if ($("#quitterMaison").length > 0)
        new Tooltip($('#quitterMaison'), {
            placement: 'left',
            html: true,
            title: 'quitter la maison perchée',
        });
});



function quitterMaison() {

    let div = document.createElement("div");
    let p = document.createElement("p");

    p.innerHTML = 'Êtes vous sur de vouloir quitter la maison perchée ?<br/> <a href="index.php"> oui</a> <input type="button" value="non" class="fermerPopUp"/>';

    div.appendChild(p);



    //Ouverture
    ouvrirPopUp(div);
}

function parseJsonTexte(_string) {
    //return _string.replace(/\n/g, '<br\/>');
}

function ouvrirPopUp(_texte) {
    $('#popup .contenu').html(_texte /* + "</br><input type='button' id='fermerPopup' value='fermer' onclick='fermerPopUp()''>"*/ );
    $('#popup').addClass('ouvert')
    $('#popup').removeClass('ferme');
    $(".ombre").addClass("montree");
    $(".ombre").removeClass("cachee");
}

function fermerPopUp() {
    $('#popup').removeClass('ouvert')
    $('#popup').addClass('ferme');
    $(".ombre").removeClass("montree");
    $(".ombre").addClass("cachee");
}

function getTextes(_page, _callback) {
    let response = $.ajax({
        url: 'medias/textes.json',
        type: 'get',
        dataType: 'json',
        error: function(data) {
            textes = data[_page];
            if (_callback != null) _callback();
        },
        success: function(data) {
            textes = data[_page];
            if (_callback != null) _callback();
        }
    });
}

/*
    Texte contient des ==féminin--masculin==
    Genre binaire : 0 = féminin, 1 = masculin
*/
function accorderTexte(texte, genreBinaire, nom, variable) {
    // console.log(texte)
    if (nom != null) texte = texte.replace("_ingredient_", nom);
    if (variable != null) texte = texte.replace("[var]", variable);
    // console.log(texte)

    let res = texte.split("==");
    for (let j = 0; j < res.length; j++) {
        if (res[j].includes("--")) {
            var genders = res[j].split('--');
            res[j] = genders[genreBinaire];
        }
    }
    res = res.join('');

    // console.log(res)

    return res;
}

function random(max) {
    let x = Math.floor(Math.random() * max);
    return x;
}

function log() {
    if (Constants.debug) {
        for (let i = 0; i < arguments.length; i++) {
            console.log(arguments[i])
        }
    }
}

/*
    Fetcher les phrases de description et les filtrer pour ne garder que celles que le joueur possède.
    Ptet pas le + optimisé...
*/
function getTextesPhrases(ids, callback, preserveStructure = false) {
    let response = $.ajax({
        url: 'medias/pieces.json',
        type: 'get',
        dataType: 'json',
        error: function(data) {
            console.log(data);
        },
        success: function(data) {
            console.log(data);
            for (let key in data) {
                data[key] = data[key].filter(x => ids.includes(x.id));
            }

            if (preserveStructure)
                callback(data);
            else {
                let merged = [];
                for (let key in data) {
                    merged.push(data[key]);
                }
                let flattened = [].concat.apply([], merged);

                console.log(merged, flattened);
                callback(flattened);
            }
        }
    });
}

function boutonFermerPopup(texte) {
    if (texte == null || texte == "") texte = "Fermer";

    let fermerButton = jQuery("<input>", {
        type: "button",
        class: "fermerPopUp",
        value: texte,
        click: fermerPopUp
    });

    return fermerButton;
}

function DOMTip(texte, id) {
    let dom = jQuery("<p>", {
        class: "tip",
        id: id
    }).html(texte);

    return dom;
}

/*
$('.montrerPopup .contenu').on('click', function(event){
        event.stopPropagation();
    });

    $('.cacherPopup .contenu').on('click', function(event){
        event.stopPropagation();
    });

    $('.montrerPopup').click(function(){
        $('#popup').toggleClass('ouvert').toggleClass('ferme');
    });

    $('.cacherPopup').click(function(){
        $('#popup').toggleClass('ouvert').toggleClass('ferme');
    });*/