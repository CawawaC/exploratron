import { Aventurier } from './aventurier';
import { Hotelvestiaire } from './Hotelvestiaire';
import { Objet } from './objets';

var aventurier = Aventurier.getLaSession();
let debarrasVisible = 10;
let debarras = getDebarras();


$(document).ready(function() {	
	getTextes("room",parseTextes);
});

function parseTextes() {   
	//$("#title")[0].innerHTML = textes.title;
	$("#consigne_debarras").html("Retirer et stocker des objets dans le débarras");
}

inscrireEquipement();
inscrireVestiaire();


$(document).on('click', '#stocker', stocker);
$(document).on('click', '#retirer', retirer);


function getDebarras() {
	let stock = [];
	stock = Hotelvestiaire.getHotelvestiaire(debarrasVisible);
	console.log(stock);
	var debarras = Objet.getObjetsByIds(stock.map(x => x.id));
	console.log(debarras);

	return debarras;
}


function inscrireEquipement() {
	//Nettoyage
	var select = $("#equipement")[0];
	while(select.options.length > 0) {
		select.options.remove(0);
	}

	console.log(select);
	console.log(aventurier.equipement);

	for(var e of aventurier.equipement) {
		var x = document.createElement("OPTION");
		x.setAttribute("value", e.id);
		var t = document.createTextNode(e.nom);
		x.appendChild(t);
		document.getElementById("equipement").appendChild(x);
	}
}

function inscrireVestiaire() {
	//Nettoyage
	var select = $("#vestiaire-hotel")[0];
	while(select.options.length > 0) {
		select.options.remove(0);
	}

	for(var e of debarras) {
		console.log(e);
		var x = document.createElement("OPTION");
		x.setAttribute("value", e.id);
		var t = document.createTextNode(e.nom);
		x.appendChild(t);
		document.getElementById("vestiaire-hotel").appendChild(x);
	}
}

function stocker() {
	var select = document.getElementById("equipement");
	var i = document.getElementById("equipement").selectedIndex;

	if(i >= 0) {
		var option = select.options[i];
		var objet = aventurier.equipement.filter(x => x.id == option.value);
		var iObjet = aventurier.equipement.indexOf(objet);

		objet = aventurier.equipement.splice(iObjet, 1)[0];
		Hotelvestiaire.ajouter(objet.id, 1);
		debarras = getDebarras();
		console.log(debarras);

		inscrireEquipement();
		inscrireVestiaire();
	}

	aventurier.sauvegarde();
}

function retirer() {
	if(!aventurier.equipementCheck(null)) {
		$("body").append("<p>Tu as trop d'équipement pour pouvoir en rajouter</p>"); 
		return;
	}

	var select = document.getElementById("vestiaire-hotel");
	var i = select.selectedIndex;

	if(i >= 0) {
		var option = select.options[i];
		var objet = debarras.find(x => x.id == option.value);

		if(!aventurier.equipementCheck(objet.categorie)) {
			$("body").append("<p>Tu as déjà équipé un objet de ce type, tu ne peux pas en équiper un autre</p>"); 
			return;
		}
		
		var iObjet = debarras.indexOf(objet);

		objet = debarras.splice(iObjet, 1)[0];
		console.log(select, i, option, objet, iObjet);
		aventurier.equipement.push(objet);
		Hotelvestiaire.retirer(objet.id, 1);
		debarras = getDebarras();

		inscrireEquipement();
		inscrireVestiaire();
	}

	aventurier.sauvegarde();
}