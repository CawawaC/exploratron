/*
    File: Aventurier.js
    Author: Appeau Studio
    Description: Classe représentant le joueur
    Usage: 
*/

class Aventurier {
    constructor(nom, description) {
        this.nom = nom;
        this.description = description;
        this.stats = new Stats(0, 0, 0); //Stats intrinsèques, indépendantes de l'équipement
        this.equipement = [];
        this.inventaire = [];
        this.regionActuelle = -1;

        //Bonus temporaire, valable pour la durée d'un donjon. Typiquement issu de la consommation d'un ingrédient.
        this.bonusDonjon = new Stats(0, 0, 0);
        this.chenil = [];
        this.tuto = false;
    }

    reset() {
        this.objets = [];
        this.bonusDonjon = new Stats(0, 0, 0);
    }

    // vie représente les points de temps disponibles, et vieMax le maxipmum de cette jauge
    setupExploration() {
        this.vie = this.vieMax;
    }

    get vieMax() {
        return this.getLesStatsTotales().moral;
    }

    perteDeTemps(magnitude) {
        let diviseur = 1;
        if (this.pouvoirs.reductionCoutExploration != null)
            diviseur = this.pouvoirs.reductionCoutExploration;
        this.vie -= Constants.coutDeplacement(magnitude / diviseur);

        if (Math.round(this.vie) <= 0)
            return false;
        else
            return true;
    }

    gainDeTemps(x) {
        this.vie += x;
        if (this.vie > this.vieMax) this.vie = this.vieMax;

        console.log(this.vie)
    }

    getLesStatsTotales() {
        var stats = this.stats.ajoute(new Stats(0, 0, 0));

        //Bonus equipement
        for (var o of this.equipement) {
            stats = stats.ajoute(o.stats);
            if (o.pouvoirs.tailleDuSac != null)
                stats.sac += o.pouvoirs.tailleDuSac;
        }

        //Bonus familiers
        for (var f of this.familiers)
            stats = stats.ajoute(f.stats);

        //Bonus donjon
        stats = stats.ajoute(this.bonusDonjon);

        return stats;
    }

    equipementCheck(categorie) {
        //Si tous les slots sont pris, on ne pourra pas équiper de nouvel objet
        if (this.equipement.length >= Objet.Categories.length) return false;
        else if (categorie == null) return true;

        //Si on a déjà un objet de ce type équipé, on ne peut pas en équiper un autre
        if (this.equipement.map(e => e.categorie).indexOf(categorie) >= 0) return false;

        return true;
    }

    familierCheck() {
        if (!this.pouvoirs.apprivoiser)
            return false
        else if (this.familiers.length > 0)
            return false;
        else
            return true;
    }

    ajouteTemps(qte) {
        this.stats.moral = Number(this.stats.moral) + qte;
        if (this.stats.moral < 0) this.stats.moral = 0;
    }

    obtenir(ingredient, scalable = true) {
        //TODO scaler la quantité ramassage avec le pouvoir multiplicateur de récolte
        var nombre = 0;

        if (scalable) {
            var multiplicateur = 1;
            if (this.pouvoirs != null && this.pouvoirs.grosseRecolte != null)
                multiplicateur = this.pouvoirs.grosseRecolte;

            while (nombre <= 0)
                nombre = Constants.cueillette(multiplicateur);
        } else
            nombre = 1;

        if (!this.placeDansLeSac())
            return "inventory_full";

        let nombreObtenu = 0;
        let i = 0;
        while (this.placeDansLeSac() && i < nombre) {
            this.inventaire.push(ingredient)
            i++;
        }
        nombreObtenu = i;

        return nombreObtenu;
    }

    placeDansLeSac() {
        console.log(this.getLesStatsTotales().sac, this.inventaire.length)
        if (this.getLesStatsTotales().sac > this.inventaire.length)
            return true;
        else
            return false;
    }

    consommer_callback(event) {
        console.log(event.data);
        let miurge = event.data["ing"];
        let aventurier = event.data["me"];
        let callbackFichePerso = event.data["callbackFichePerso"];
        aventurier.consommer(miurge)
        callbackFichePerso();
    }

    consommer(ingredient) {
        //Empecher de trop manger.
        if (this.repu != null && this.repu == true)
            return -1;
        this.repu = true;

        let bonus = null;
        if (this.pouvoirs.bonusConsommation != null)
            bonus = this.pouvoirs.bonusConsommation.find(x => x.indexRegne == ingredient.iRegne);
        // bonus = this.pouvoirs.bonusConsommation;

        let multiplicateur = 1;
        if (bonus != null) multiplicateur = bonus.intensite;
        console.log("multiplicateur de consommation : " + multiplicateur);

        let gainTemps = Constants.consommation(ingredient) * multiplicateur;
        console.log(this.vie, "Gain de temps : ", gainTemps)
        this.gainDeTemps(gainTemps)
        console.log(this.vie)

        this.inventaire.splice(this.inventaire.indexOf(ingredient), 1)
        return gainTemps;
    }

    /*  pouvoirsTotaux() {
            var drapeauTotal;
            for(let e of this.equipement) {
                drapeauTotal = drapeauTotal | e.pouvoirs.drapeaux;
            }
            return drapeauTotal;
        }*/

    get pouvoirs() {
        if (this.equipement == null || this.equipement.length == 0)
            return new Pouvoirs(0);
        let pvr = new Pouvoirs(0);
        for (let e of this.equipement) {
            pvr = pvr.somme(e.pouvoirs);
        }
        return pvr;
    }

    peutApprivoiser(ingredient) {
        //TODO Séparer en comparaisons par catégorie
        let iRegne = ingredient.regne;
        if (iRegne < 0)
            return false;
        if (this.pouvoirs.apprivoiser != null && this.pouvoirs.apprivoiser.indexOf(iRegne) >= 0)
            return true;
    }

    apprivoiser(ingredient) {
        this.familiers.push(ingredient);
        return true;
    }

    static getLaSession() {
        var response = $.ajax({ data: "", type: "POST", url: "exposeSession.php", async: false }).responseText;

        if (response == "NULL")
            return null;

        // document.write(response);
        var objSession = JSON.parse(response);

        var aventurier = new Aventurier(objSession.nom, objSession.description);

        aventurier.stats = new Stats(
            Number(objSession.vie),
            Number(objSession.vit),
            Number(objSession.vol));

        if (objSession.objets != false && objSession.objets != null) {
            aventurier.equipement = Objet.getObjetsByIds(objSession.objets.filter(o => typeof o == "string"), false);
            for(let o of objSession.objets) {
                o.id = parseInt(o.id)
                let obj;
                if(o.id < 0) {
                    obj = Objet.objetDepuisLigneBDD(o, false)
                } else {
                    obj = Objet.getObjetsByIds([o.id])[0];
                }
                aventurier.equipement.push(obj)
            }
        } else {
            aventurier.equipement = [];
        }

        if (objSession.vestiaire != false && objSession.vestiaire != null) {
            aventurier.vestiaire = Objet.getObjetsByIds(objSession.vestiaire.filter(o => typeof o == "string"), false);
            for(let o of objSession.vestiaire) {
                o.id = parseInt(o.id)
                let obj;
                if(o.id < 0) {
                    obj = Objet.objetDepuisLigneBDD(o, false)
                } else {
                    obj = Objet.getObjetsByIds([o.id])[0];
                }
                aventurier.vestiaire.push(obj)
            }
        } else {
            aventurier.vestiaire = [];
        }

        aventurier.regionActuelle = objSession.idVoronoiActuel;

        if (objSession.coffre != false && objSession.coffre != null) {
            aventurier.coffre = Ingredient.getIngredientsByIds(objSession.coffre, false);
        } else {
            aventurier.coffre = [];
        }

        if (objSession.chenil != false && objSession.chenil != null) {
            aventurier.chenil = Ingredient.getIngredientsByIds(objSession.chenil, false);
        } else {
            aventurier.chenil = [];
        }

        if (objSession.poeme != false && objSession.poeme != null) {
            aventurier.poeme = objSession.poeme.map(x => parseInt(x))
        } else {
            aventurier.poeme = [];
        }

        if (objSession.familiers != false && objSession.familiers != null) {
            aventurier.familiers = Ingredient.getIngredientsByIds(objSession.familiers, false);
        } else {
            aventurier.familiers = [];
        }

        if (objSession.inventaire != false && objSession.inventaire != null) {
            aventurier.inventaire = Ingredient.getIngredientsByIds(objSession.inventaire, false);
        } else {
            aventurier.inventaire = [];
        }

        if (objSession.tuto != false && objSession.tuto != null) {
            aventurier.quetes = objSession.tuto;
            for (let i in aventurier.quetes)
                aventurier.quetes[i] = parseInt(aventurier.quetes[i]);
        } else {
            aventurier.initQuetes();
        }
        aventurier.verifierQuetes();


        aventurier.equipement = aventurier.equipement.filter(x => x != null)
        aventurier.vestiaire = aventurier.vestiaire.filter(x => x != null)

        // aventurier.quetes = {};
        // aventurier.quetes.explore = QueteFlag.STARTED;
        // aventurier.quetes.ramener3Miurges = QueteFlag.UKNOWN;

        return aventurier;
    }

    verifierQuetes() {
        if (this.quetes.explore == null)
            this.quetes.explore = QueteFlag.UNKNOWN;
        if (this.quetes.ramener3Miurges == null)
            this.quetes.ramener3Miurges = QueteFlag.UNKNOWN;
        if (this.quetes.crafter == null)
            this.quetes.crafter = QueteFlag.UNKNOWN;
        if (this.quetes.upZir == null)
            this.quetes.upZir = QueteFlag.UNKNOWN;
        if (this.quetes.creerIngredient == null)
            this.quetes.creerIngredient = QueteFlag.UNKNOWN;
     if (this.quetes.nothing == null)
            this.quetes.nothing = QueteFlag.UNKNOWN;
    }

    initQuetes() {
        this.quetes = {
            explore: QueteFlag.UNKNOWN,
            ramener3Miurges: QueteFlag.UNKNOWN,
            crafter: QueteFlag.UNKNOWN,
            upZir: QueteFlag.UNKNOWN,
            creerIngredient: QueteFlag.UNKNOWN,
            nothing: QueteFlag.UNKNOWN
        };
    }

    finirQuetes() {
        this.quetes = {
            explore: QueteFlag.DONE,
            ramener3Miurges: QueteFlag.DONE,
            crafter: QueteFlag.DONE,
            upZir: QueteFlag.DONE,
            creerIngredient: QueteFlag.DONE
        };
    }

    listeInventaire(list, callbackFichePerso) {
        var inventaire = this.inventaire;
        var ordre = {};
        for (var ing of inventaire) {
            if (ordre[ing.nom] == undefined) ordre[ing.nom] = 1;
            else ordre[ing.nom] += 1;
        }

        let func = this.consommer_callback;
        let me = this

        $.each(ordre, function(index, value) {
            var li = $('<li/>')
                .text(value + ' ' + index)
                .appendTo(list);
            let button = jQuery("<input>", {
                type: "button",
                value: "consommer",
                disabled: this.repu
            });

            button.click({ ing, me, callbackFichePerso }, func);
            button.appendTo(li)
        });
    }

    sauvegarde() {
        for(let e of this.equipement.concat(this.vestiaire)) {
            if(typeof e.ingredients[0] == "object")
                e.ingredients = e.ingredients.map(i => i.id)
        }
        // var idsObjets = this.equipement.map(e => e.id >= 0 ? parseInt(e.id) : e);
        // var idsVestiaire = this.vestiaire.map(e => e.id >= 0 ? parseInt(e.id) : e);
        var idsCoffre = this.coffre.map(e => parseInt(e.id));
        var idsChenil = this.chenil.map(e => parseInt(e.id));
        var idsFamiliers = this.familiers.map(e => parseInt(e.id));
        var idsInventaire = this.inventaire.map(e => parseInt(e.id));

        let idsObjets = [];
        for(let o of this.equipement) {
            if(o.id >= 0) {
                idsObjets.push({id: parseInt(o.id), description: o.description});
            } else {
                idsObjets.push(o);
            }
        }

        let idsVestiaire = [];
        for(let o of this.vestiaire) {
            if(o.id >= 0) {
                idsVestiaire.push({id: parseInt(o.id), description: o.description});
            } else {
                idsVestiaire.push(o);
            }
        }

        $.ajax({
            url: 'settersBDD/sauvegardeAventurier.php',
            type: 'POST',
            data: {
                nom: this.nom,
                vie: this.stats.moral,
                vol: this.stats.volonte,
                vit: this.stats.sac,
                objets: idsObjets,
                vestiaire: idsVestiaire,
                coffre: idsCoffre,
                chenil: idsChenil,
                familiers: idsFamiliers,
                poeme: this.poeme,
                inventaire: idsInventaire,
                tuto: this.quetes
            },
            async: false,
            dataType: 'html', // On désire recevoir du HTML
            complete: function() {},
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
                // console.log(code_html, statut);
            }
        });
    }

    getRencontres() {
        let response = $.ajax({
            url: 'gettersBDD/getJoueurRencontres.php',
            type: 'GET',
            data: {
                nomJoueur: this.nom
            },
            async: false,
            dataType: 'html', // On désire recevoir du HTML
            complete: function() {},
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
            }
        });

        var data = response.responseText;
        if (data == "false")
            return [];
        else {
            var rencontres = JSON.parse(data);
            rencontres = rencontres.map(x => parseInt(x))
            return rencontres;
        }
    }

    getPhrases() {
        let response = $.ajax({
            url: 'gettersBDD/getJoueurPhrases.php',
            type: 'GET',
            data: {
                nomJoueur: this.nom
            },
            async: false,
            dataType: 'html', // On désire recevoir du HTML
            complete: function() {},
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
            }
        });

        var data = response.responseText;
        if (data == "false")
            return [];
        else {
            let phrases = JSON.parse(data);
            phrases = phrases.map(x => parseInt(x))
            return phrases;
        }
    }

    sauvegardePhrases() {
        $.ajax({
            url: 'settersBDD/setJoueurPhrases.php',
            type: 'GET',
            data: {
                nomJoueur: this.nom,
                phrases: this.phrases
            },
            async: false,
            dataType: 'html', // On désire recevoir du HTML
            complete: function() {},
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
                console.log(code_html);
            }
        });
    }

    getSalon() {
        let response = $.ajax({
            url: 'gettersBDD/getJoueurSalon.php',
            type: 'GET',
            data: {
                nomJoueur: this.nom
            },
            async: false,
            dataType: 'html', // On désire recevoir du HTML
            complete: function() {},
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
            }
        });

        var data = response.responseText;
        if (data == "false")
            return [];
        else {
            let salon = JSON.parse(data);
            if (salon == null) salon = [];

            salon = salon.map(x => parseInt(x));
            return salon;
        }
    }

    sauvegardeSalon() {
        $.ajax({
            url: 'settersBDD/setJoueurSalon.php',
            type: 'GET',
            data: {
                nomJoueur: this.nom,
                salon: this.salon
            },
            async: false,
            dataType: 'html', // On désire recevoir du HTML
            complete: function() {},
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
                console.log(code_html);
            }
        });
    }

    sauvegardeRencontres() {
        $.ajax({
            url: 'settersBDD/setJoueurRencontres.php',
            type: 'GET',
            data: {
                nomJoueur: this.nom,
                rencontres: this.rencontres
            },
            async: false,
            dataType: 'html', // On désire recevoir du HTML
            complete: function() {},
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
                console.log(code_html);
            }
        });
    }

    /*
        Action de rencontrer un ingrédient
    */
    rencontre(id) {
        if (this.rencontres == null) {
            this.rencontres = this.getRencontres();
        }

        if (this.premiereRencontre(id)) {
            this.rencontres.push(id)
            this.sauvegardeRencontres();
        }
    }

    /*
        Retourne true si le miurge avec cet id n'a jamais été rencontré
    */
    premiereRencontre(id) {
        if (this.rencontres == null) {
            this.rencontres = this.getRencontres();
        }

        return this.rencontres.indexOf(id) < 0;
    }

    getRecettes() {
        let response = $.ajax({
            url: 'gettersBDD/getJoueurRecettes.php',
            type: 'GET',
            data: {
                nomJoueur: this.nom
            },
            async: false,
            dataType: 'html', // On désire recevoir du HTML
            complete: function() {},
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
            }
        });

        var data = response.responseText;
        if (data == "false")
            return [];
        else {
            var recettes = JSON.parse(data);
            recettes = recettes.map(x => parseInt(x))
            // recettes = recettes.filter(function(x, index, arr) { return (arr.findIndex(x) === index); })
            return recettes;
        }
    }

    sauvegardeRecettes() {
        $.ajax({
            url: 'settersBDD/setJoueurRecettes.php',
            type: 'GET',
            data: {
                nomJoueur: this.nom,
                recettes: this.recettes
            },
            async: false,
            dataType: 'html', // On désire recevoir du HTML
            complete: function() {},
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
            }
        });
    }

    recetteConnue(id) {
        if (this.recettes == null) {
            this.recettes = this.getRecettes();
        }

        return this.recettes.indexOf(id) >= 0;
    }

    apprendRecette(id) {
        var _return = false;
        if (this.recettes == null) {
            this.recettes = this.getRecettes();
            //_return = true;
        }
        if (!this.recetteConnue(id)) {
            this.recettes.push(id)
            this.sauvegardeRecettes();
             _return = true;
        }

        return _return;
    }

    apprendVers(index) {
        if(this.poeme.indexOf(index) < 0) {
            this.poeme.push(index);
            this.poeme.push(parseInt(index)+1);
            this.poeme.push(parseInt(index)+2);
        }
    }
}

var QueteFlag = {
    UNKNOWN: -1, //Quete pas encore découverte
    STARTED: 0, //Quete tout juste commencée
    IN_PROGRESS: 1, //Quete en train d'être réalisée
    FINISHED: 2, //On vient de terminer --> annonce
    DONE: 3 //TOut fini, plus rien à dire
};