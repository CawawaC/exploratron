/*
    File: scripts_exploration.js
    Author: Appeau Studio
    Description: Script exécuté pendant l'exploration
    Usage: Interaction class = "interaction" id="nomGeneriqueInteraction" value="nomSpecifiqueInteraction"
*/

//A remplacer par la variable aventurier réelle
var aventurier = Aventurier.getLaSession();
aventurier.repu = false;
aventurier.rencontres = aventurier.getRencontres();
if (Constants.ramassagePhrases) aventurier.phrases = aventurier.getPhrases();
aventurier.recettes = aventurier.getRecettes();

var opposantActuel;


//A exécuter qu'un fois, à l'entrée du donjon
var maison = new Maison( /*place, 7, 3, 10,*/ [0, 0]); // /*id, largeur, hauteur, nombre pièces,*/ position de départ du joueur
var carte;

if(Constants.debug && maison.salleSecrete) console.log(maison.departJoueur, maison.salleSecrete, maison.salleSecrete.salleAcces)

//A exécuter à chaque nouvelle salle. 
//Remplacer les coordonnées par les coordonnées de la salle désirée.
//Possibilité d'obtenir l'une des salles adjacentes à la salle actuelle de la manière suivante : 
//maison.voisine(xSalleActuelle, ySalleActuelle, Salle.NORD);
var salle;
var pieceVisitees; //tableau des pièces visitées par le joueurs, pas utilisé pour l'instant
var positionJoueur;
var derniereDirection;
var contenuCoteOuvert;
var ingredientActuel;
var genreBinaire;
var combatEnCours = false;

var pouvoirSortie;
pouvoirSortie = Constants.debug;

let encounter;


$(document).ready(function() {


    getTextes("exploration", parseTextes);

    if (aventurier.quetes.explore == QueteFlag.STARTED)
        aventurier.quetes.explore = QueteFlag.IN_PROGRESS;

    // cout de l'entrée dans le donjon
    // changerPointsDeTemps(-1);

    positionJoueur = maison.departJoueur;
    salle = maison.plan[positionJoueur[0]][positionJoueur[1]];
    pieceVisitees = [];
    //carte de la pièce
    carte = new Carte();

    if (aventurier.pouvoirs.carteDonjon || Constants.debug) {

        carte.majPlan(maison.plan);
    }
    //si pouvoir de carte tout réveller
    // aventurier.pouvoirs.carteDonjon
    // carte.majPlan(maison.plan);

    //si pouvoir de carte, afficher la salle secrète
    console.log(maison.salleSecrete)
     if (aventurier.pouvoirs.positionSalleSecrete || Constants.debug){
    if (maison.salleSecrete != null) {
        carte.majPlan({
            x: maison.salleSecrete.x,
            y: maison.salleSecrete.y,
            accessibiliteVoisines: maison.salleSecrete.accessibiliteVoisines
        });
    }}

    contenuCoteOuvert = false;

    ajoutePiece(
        10,
        salle.accessibiliteVoisines,
        positionJoueur[0],
        positionJoueur[1],
        salle.description);

    aventurier.setupExploration();


    //ajoute une nouvelle pièce à la liste des pièces
    $("#pieces").on("click", ".direction", function() {

        if (contenuCoteOuvert) return;

        //si créature agressive
        for (var i = 0; i < salle.contenu.length; i++) {
            let miurge = salle.contenu[i];

            if (miurge.agressif == 1 && Ingredient.Regnes[miurge.iRegne].combat) {
                ouvrePanneauInteraction(i);
                return;
            }
        }

        if ($(this).hasClass("desactive")) return;
        // deséctive les anciennes pièces
        $(".direction").prop("disabled", true);

        if ($(this).hasClass("sortie")) {
            sortir(textes.text_exit);
        } else {
            // change la position du joueur dans le donjon
            if ($(this).hasClass("sud")) {
                positionJoueur[1]++;
                derniereDirection = "sud";
            }
            if ($(this).hasClass("nord")) {
                positionJoueur[1]--;
                derniereDirection = "nord";
            }
            if ($(this).hasClass("est")) {
                positionJoueur[0]++;
                derniereDirection = "est";
            }
            if ($(this).hasClass("ouest")) {
                positionJoueur[0]--;
                derniereDirection = "ouest";
            }


            //Dépense de temps pour le déplacement
            if (aventurier.perteDeTemps(distanceRegionActuelle) == false) {
                ejection();
            }
            actualiserJaugeTemps();
            //met à jour la pièce
            salle = maison.plan[positionJoueur[0]][positionJoueur[1]];

            //TODO si la pièce n'est pas dèja ajoutée, l'ajouter    
            ajoutePiece(11,
                salle.accessibiliteVoisines,
                positionJoueur[0],
                positionJoueur[1],
                salle.description);

        }
    });

    if (Constants.ramassagePhrases)
        $(document).on("click", ".ramassable", ramasserPhrase)

    $(document).on('click', '#sortir', function() { sortir(textes.text_kick) });

    //ouverture panneau interaction
    $("#pieces").on("click", ".ingredient", function(event) {

        event.preventDefault();
        ouvrePanneauInteraction(event.target.dataset.index);
    });



    // interactions avec ingrédient
    $("#pieces").on("click", ".interaction", function(event) {
        // var interaction = event.target.value.replace('• ', '').replace(' •', '');
        var interaction = $(event.target).attr("id");
        var nomOuregne = "";

        // if (Ingredient.Regnes.map(e => e.interactionAcquerir.toLowerCase()).indexOf(interaction.toLowerCase()) > -1) {
        //     obtenir(ingredientActuel);
        // }

    });

    // interactions combat
    $("#pieces").on("click", ".label-action", function(event) {

        //desactive les autres interactions
        $(event.target).parent().parent().find(".input-action").prop("disabled", true);

        //active l'interaction séléctionnée
        $(event.target).parent().find(".input-action").prop("disabled", false);

    });

    // ingredient hover
    $("#pieces").on("mouseenter", ".ingredient", function(event) {
        if (contenuCoteOuvert) return;
        let ing = salle.contenu[$(this).data('index')];
        let regne = Ingredient.Regnes[ing.iRegne];
        let ingGenreBinaire = ing.genre == "feminin" ? 0 : 1;
        let nomAccorde = ingGenreBinaire == 0 ? regne.nom_genre_0 : regne.nom_genre_1;
        let info =
            accorderTexte("C'est ==une--un", ingGenreBinaire) + " " +
            nomAccorde + " : " +
            // "Un précieux : " +
            '<i>' +
            ing.description +
            '</i><br />' +
            accorderTexte(textes.intro_creator, ing.genre == "masculin" ? 0 : 1) +
            ' <b>' +
            salle.contenu[$(this).data('index')].joueurCreateur +
            '</b>';

        $(".contenu-description").html(info);
        $(".contenu-description").fadeIn();
        $(".contenu-description").height($(".contenu-principal p:first").height());
    });

    $("#pieces").on("mouseleave", ".ingredient", function(event) {
        //if(contenuCoteOuvert) return;
        $(".contenu-description").fadeOut();
    });

    // animation panneaux latéraux
    $("#menu-exploration a").click(function(event) {
        event.preventDefault();
        $(".panneau-lateral#" + $(this).attr('href')).addClass('ouvert');
        $("#pieces").css("filter", "blur(2.5px)");
        $(".ombre").addClass("montree");
        $(".ombre").removeClass("cachee");
        if ($(this).attr('href') == "personnage")
            $("#stats-joueur").removeClass("transparent");
        afficherFichePerso();
    });

    $(".panneau-lateral .fermeture").click(function(event) {
        event.preventDefault();
        $(".panneau-lateral#" + $(this).closest('.panneau-lateral').attr('id')).removeClass("ouvert");
        $("#pieces").css("filter", "blur(0px)");
        $(".ombre").removeClass("montree");
        $(".ombre").addClass("cachee");
        $("#stats-joueur").addClass("transparent");
    });

    $(".panneau-lateral").on("click", ".personnage-item-inventaire", function(event) {

        var ingredientActuel;

        //get ingredient
        for (var e of aventurier.inventaire) {
            if (e.id == $(this).data('id')) {
                ingredientActuel = e;
                break;
            }
        }


        var resultat = aventurier.consommer(ingredientActuel);

        let div = jQuery("<div>");

        if (resultat > -1)
            div.html(textes.text_consume_ok + " " + ingredientActuel.nom + "</br></br>");
        else
            div.html(textes.text_consume_failure + "</br></br>");


        jQuery("<input>", {
            type: "button",
            id: "fermer_popup",
            value: "Fermer",
            click: fermerPopUp
        }).appendTo(div);

        ouvrirPopUp(div);

        afficherFichePerso();

    });



    $("#coffre").click(ouvrirCoffre);

    $("#interrupteur").click(handleClickInterrupteur);
});

function handleClickInterrupteur() {
    console.log("test");
}

function ouvrirCoffre() {

    function boutonFermerLocal(texteFermeture) {
        return jQuery("<input>", {
            type: "button",
            id: "fermer_popup",
            value: texteFermeture,
            click: fermerPopUp
        });
    }

    let div = jQuery("<div>");

    if($("#coffre").prop("opened")) {
        //Déjà été ouvert
        div.html("C'est vide. Ne l'avais pas déjà ouvert ?")
        boutonFermerLocal("Si").appendTo(div)
    } else {
        //Première ouverture : obtention objet

        // Création d'un objet
        let loot = Objet.getObjetsRandom(1)[0];

        // Vérification si cet objet existe déjà (nécessaire pour qu'il ait un id)
        let objetExistant = Objet.getObjetByRecette(loot.ingredients.map(x => x.id));

        // Sinon, on l'ajoute à la BDD objets pour qu'il ait un id.
        if (objetExistant == null) {
            Objet.ajouteObjet(loot, null);
        }


        if (aventurier.equipementCheck(loot.categorie)) {
            // div.html(textes.inventory_taken).append(errorButton); //'<div>Vous ne pouvez pas sélectionner le même ingrédient plusieurs fois !</div>'
            div.html("Vous obtenez : " + loot.nom + ". Propriétés : ").append(afficherPouvoirsEtStats(loot.stats, loot.pouvoirs, false));
            aventurier.equipement.push(loot);
        } else {
            div.html("Vous portez déjà un " + loot.categorie + ". Vous prenez le nouveau " + loot.nom + " à la main et le déposerez dans votre chambre à votre retour.");
            if (aventurier.objets == null)
                aventurier.objets = [];
            aventurier.objets.push(loot);
        }   
        
        $("#coffre").prop("opened", true)
        boutonFermerLocal("Ooooooh !").appendTo(div)
    }

    ouvrirPopUp(div);
}

function afficherFichePerso() {
    var fichePerso = document.getElementById("fiche");
    while (fichePerso.firstChild) fichePerso.removeChild(fichePerso.firstChild);

    $("#stats-joueur .contenu h2").html(aventurier.nom);
    $("#pouvoirs").html(afficherPouvoirsEtStats(aventurier.getLesStatsTotales(), aventurier.pouvoirs));
    var i = 0;
    $("#equipement-chambre").empty();
    for (var e of Objet.Categories) {
        var id = e.replace(/[^\w\s]/gi, "-");
        id = id.replace(/\s/g, '-');
        $('<form id="' + id + '"><input type="radio"></input> <label>' + e + ' (vide)</label></form>').appendTo($("#equipement-chambre"));
    }
    for (var e of aventurier.equipement) {
        var id = e.categorie.replace(/[^\w\s]/gi, "-");
        id = id.replace(/\s/g, '-');
        $("#" + id).empty();
        //  $('<input type="radio" class="objet" name="equipement" data-id="' + e.id + '" id=e "'+ i + '"></input> <label for= e"'+ i + '" >' + e.nom + '</label>').appendTo( $("#"+id));

        inscrireObjet(e.nom, e.id, i, textePopUpStats(e), "equipement", "e", $("#" + id))
        // inscrireObjet(label, id, i, title, destinationName, forPrefix, destination) 
        i++;
    }

    $("#inventaire").empty();
    var i = 0;
    for (var e of aventurier.inventaire) {

        $('<h3><img src="medias/images/picto-'+  Ingredient.Regnes[e.iRegne].nom +'.png">' + e.nom + '</h3><input type="button" class="personnage-item-inventaire" name="inventaire" data-id="' + e.id + '"  class="itemInventaire" value="' + textes.text_consume_button + '"></input></br>').appendTo('#inventaire');
        i++;
    }
    actualiserRemplissageSac();
}

function actualiserRemplissageSac() {
    var divRemplissage = $("#remplissage-sac");
    divRemplissage.empty();
    divRemplissage.append(aventurier.inventaire.length + "/" + aventurier.getLesStatsTotales().sac);
}

function inscrireObjet(label, id, i, title, destinationName, forPrefix, destination) {
    $('<input type="radio" class="objet" name="' + destinationName + '" data-id="' + id + '" id="' + forPrefix + '' + i + '"></input> <label for="' + forPrefix + i + '" >' + label + '</label>').appendTo(destination);

    new Tooltip(destination.children().last(), {
        placement: 'right',
        html: true,
        title: title,
        boundariesElement: "conteneur-flex",
    });
}

function textePopUpStats(e) {
    var textePopUp;
    var flecheTemps, flecheZir, flecheSac;
    if (e.stats.moral > 0) flecheTemps = "&#8599;";
    else if (e.stats.moral < 0) flecheTemps = "&#8600;";
    else flecheTemps = "&#8594;";

    if (e.stats.volonte > 0) flecheZir = "&#8599;";
    else if (e.stats.volonte < 0) flecheZir = "&#8600;";
    else flecheZir = "&#8594;"

    if (e.stats.sac > 0) flecheSac = "&#8599;";
    else if (e.stats.sac < 0) flecheSac = "&#8600;";
    else flecheSac = "&#8594;";

    textePopUp = 'Temps ' + flecheTemps + e.stats.moral.toString() + '<br>Zir ' + flecheZir + e.stats.volonte.toString() + '<br>Sac à dos ' + flecheSac + e.stats.sac.toString();

    return textePopUp;
}

function ramasserPhrase(e) {
    if (!Constants.ramassagePhrases)
        return;
    let target = e.currentTarget;
    aventurier.phrases.push(parseInt(target.id));
}

function ouvrePanneauInteraction(_index) {
    $(".contenu-description").css("display", "none");
    ingredientActuel = salle.contenu[_index];
    genreBinaire = ingredientActuel.genre == "masculin" ? 1 : 0;

    aventurier.rencontre(ingredientActuel.id);

    let regne = Ingredient.Regnes[ingredientActuel.iRegne];
    encounter = textes["encounter_" + regne.refString]
    encounter = accorderTextePlus(encounter, genreBinaire, ingredientActuel.nom);
    let disable;

    if (contenuCoteOuvert) return;
    $(".contenu-principal").addClass("desactive");
    $("#bandeau-central-exploration").addClass("desactive");
    $('.contenu-principal a').addClass("desactive");
    $('.contenu-principal .direction').addClass("desactiveTemp");

    //$('#contenu-cote-tiret').css('top',$(this).offset().top - $(this).parent().offset().top + $(this).height()/2);

    $(".contenu-principal").on('webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd', function() {

        $("#contenu-cote").off('webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd');
        $('#contenu-cote').addClass("opaque");

        initInteractionsCote(_index);
    })
}

function initInteractionsCote(_index) {
    if (_index == null) _index = 0;

    contenuCoteOuvert = true;


    let introTexte = "",
        boutonTexte = "",
        disabled = false;

    ////Ecriture de la description ou du nom de l'ingrédient, s'il existe, dans le texte
    textes.recipe_tip = accorderTextePlus(textes.recipe_tip, 0, ingredientActuel)

    let regne = Ingredient.Regnes[ingredientActuel.iRegne];
    let div = texteApproche(ingredientActuel.nom, regne, genreBinaire, ingredientActuel.description);

    $("#contenu-cote .description").empty();
    $("#contenu-cote .description").append(div);

    $('#contenu-cote .description').attr('data-index', _index);

    var formInteractions = document.getElementById("interactions");

    //On efface d'abord les boutons d'interactions (sinon ils s'ajoutent à ceux des instances précédentes)
    while (formInteractions.firstChild)
        formInteractions.removeChild(formInteractions.firstChild);


    //Ajout du bouton de nommage si l'ingrédient n'est pas nommé
    if (ingredientActuel.nom == "" || ingredientActuel.nom == null) {
        introTexte = accorderTexte(textes.text_name_creature, genreBinaire);
        boutonTexte = accorderTexte(textes.interaction_name_button, genreBinaire);
        introButtonPair(introTexte, boutonTexte, setupNommage, formInteractions, false, "nommer");
    } else {
        let regne = Ingredient.Regnes[ingredientActuel.iRegne];
        //L'ingrédient est nommé, on ajoute les interactions permises spécifiquement pour un ingrédient nommé

        if (regne.combat && !ingredientActuel.chantReussi) {
            introButtonPair(
                encounter["sing_intro"],
                encounter["sing_button"],
                attaquer,
                formInteractions,
                false,
                "chanter");
        }

        if (regne.indiceRecette) {
            introButtonPair(
                textes.recipe_tip["intro"],
                textes.recipe_tip["speak_button"],
                indiceRecette,
                formInteractions,
                false,
                "indice");
        }

        if (!regne.indiceRecette && !regne.combat) {
            introButtonPair(
                encounter["pick_intro"],
                encounter["pick_button"],
                obtenir_normal,
                formInteractions,
                false,
                "prendre");

            introButtonPair(
                encounter["consume_intro"],
                encounter["consume_button"],
                consommer,
                formInteractions,
                aventurier.repu,
                "consommer");

            if (aventurier.repu) {
                jQuery("<p>", { class: "tip" }).html(accorderTexte(textes.tip_noeat, genreBinaire)).appendTo(formInteractions);
            }
        }

        if (regne.indicePieceSecrete) {
            introButtonPair(
                accorderTexte(textes.secret_room_tip.intro, genreBinaire),
                textes.secret_room_tip.button,
                indiceSalleSecrete,
                formInteractions,
                false,
                "indiceSecret");
        }

        //Bouton apprivoiser
        /* introTexte = encounter["tame_intro"];
         boutonTexte = encounter["tame_button"];
         disabled = !aventurier.peutApprivoiser(ingredientActuel);
         if (!disabled) {
         introButtonPair(introTexte, boutonTexte, apprivoiser, formInteractions, disabled, "tame");

         
             //Bouton apprivoiser grisé --> explication
             let tip = textes.interaction_tame_impossible_explain.replace("_regne_", Ingredient.Regnes[ingredientActuel.iRegne].nom);
             jQuery("<p>", {
                 class: "tip",
                 id: "tame_explain"
             }).html(tip).appendTo(formInteractions);
         }*/
    }


    //Ajout du bouton fuir si la créature n'est pas agressive
    introTexte = encounter["leave_intro"];
    boutonTexte = encounter["leave_button"];
    disabled = ingredientActuel.agressif == 1;
    introButtonPair(introTexte, boutonTexte, fuir, formInteractions, disabled, "fuire");
    if (disabled) {
        //Petit texte explicatif
        jQuery("<p>", { class: "tip" }).html(accorderTexte(textes.tip_agressif, genreBinaire)).appendTo(formInteractions);
    }
}

function accorderTextePlus(encounter, genreBinaire, nom) {
    let accorde = [];
    for (let e of Object.keys(encounter)) {
        accorde[e] = accorderTexte(encounter[e], genreBinaire).replace("_ingredient_", nom);
    }
    return accorde;
}

function indiceSalleSecrete() {
    let indice = textes.secret_room_tip.tip;
    let boutonFuir = "";

    if (maison.salleSecrete == null || maison.salleSecrete.salleAcces == null) {
        indice = textes.secret_room_tip.tip_no_secret;
        boutonFuir = textes.secret_room_tip.leave_button_no_secret;
    } else {
        let direction = maison.getDirectionSalleSecrete({ x: positionJoueur[0], y: positionJoueur[1] });
        direction = Salle.direction(direction);
        indice = accorderTexte(indice, genreBinaire, "", direction);
        boutonFuir = textes.secret_room_tip.leave_button;
    }


    let div = jQuery("<div>", {
        class: "description",
        style: "display: none;"
    });

    div.append(indice);

    //Bouton fuir
    jQuery("<input>", {
        type: "button",
        class: "interaction",
        id: "fuire",
        value: boutonFuir,
        click: fuir
    }).appendTo(div);

    changerContenuCote(div);
    enleverTexteIngredient(ingredientActuel);
}

function indiceRecette() {
    //Indice recette
    let indice = getIndiceRecetteLocale();

    let div = jQuery("<div>", {
        class: "description",
        style: "display: none;"
    });

    div.append(indice);

    //Bouton fuir
    jQuery("<input>", {
        type: "button",
        class: "interaction",
        id: "fuire",
        value: textes.eat_creature_button_done,
        click: fuir
    }).appendTo(div);

    changerContenuCote(div);
    enleverTexteIngredient(ingredientActuel);
}

function getIndiceRecetteLocale() {
    let objet;
    let texte = "";

    console.log(aventurier.getLesStatsTotales().volonte, Constants.offsetVolonte, distanceRegionActuelle)

    //Si on est dans la région centrale et le joueur n'a rien d'équipé, donner la recette première
    if (aventurier.getLesStatsTotales().volonte == Constants.offsetVolonte && distanceRegionActuelle == 0) {
        objet = Objet.getPremiereRecette();
        texte = textes.recipe_tip.tip_naked
    } else {
        objet = Objet.recetteAleatoire(ingredientsDisponibles, aventurier.recettes);
        texte = textes.recipe_tip.tip;
    }

    if(aventurier.apprendRecette(objet.id)){
        //message de nouvelle recette
        texte += "</br>" + "</br>" + textes.new_recipe;
    }

    let ingredients = objet.ingredients;

    console.log(ingredients);

    if (ingredients == null)
        texte = "Pas de recettes avec les objets d'ici (ça devrait pas être possible)";
    else {
        texte = texte.replace("_ingredient1_", ingredients[0].nom);
        texte = texte.replace("_ingredient2_", ingredients[1].nom);
        texte = texte.replace("_ingredient3_", ingredients[2].nom);
    }

    dom = $("<div>").append(texte).append(DOMTip(textes.tip_new_recipe));


    return dom;
}

function consommer(event) {
    let plat = ingredientActuel;

    let gainTemps = aventurier.consommer(plat);

    actualiserJaugeTemps();

    if (gainTemps >= 0) {
        var texteEffets = "";

        if (gainTemps > 0)
            texteEffets += textes.eat_creature_gain_time;
        else if (gainTemps == 0)
            texteEffets += textes.eat_creature_nothing_happens;
        else
            texteEffets += textes.eat_creature_lose_time;

        if (plat.nom != null && plat.nom != "")
            nomOuregne = plat.nom;
        else
            nomOuregne = Ingredient.Regnes[plat.iRegne].nom;

        let div = jQuery("<div>", {
            class: "description",
            style: "display: none;"
        });
        let eat = textes.eat_creature_generic;
        let leave = textes.eat_creature_button_done;
        switch (ingredientActuel.iRegne) {
            case 0:
                eat = textes.eat_creature_botanique;
                leave = textes.eat_creature_botanique_button_done;
                break;

            case 1:
                eat = textes.eat_creature_egaree;
                leave = textes.eat_creature_egaree_button_done;
                break;

            case 2:
                eat = textes.eat_creature_onirique;
                leave = textes.eat_creature_onirique_button_done;
                break;

            case 3:
                eat = textes.eat_creature_precieux;
                leave = textes.eat_creature_precieux_button_done;
                break;

            case 4:
                eat = textes.eat_creature_utile;
                leave = textes.eat_creature_utile_button_done;
                break;
        }
        div.append(document.createTextNode(accorderTexte(eat, genreBinaire).replace("_ingredient_", nomOuregne)));
        jQuery("<br/>").appendTo(div);
        div.append(document.createTextNode(texteEffets));
        jQuery("<br/>").appendTo(div);

        jQuery("<p>", {
            class: "tip",
            id: "eat_explain"
        }).html("Vous gagnez " + gainTemps + " points de temps (temps : " + Math.round(aventurier.vie) + "/" + aventurier.vieMax + ")").appendTo(div);

        //Bouton fuir
        jQuery("<input>", {
            type: "button",
            class: "interaction",
            id: "fuire",
            value: textes.eat_creature_button_done,
            click: fuir
        }).appendTo(div);


        changerContenuCote(div);
        // supprimer phrase ingrédient du texte de la pièce
        enleverTexteIngredient(plat);
    }
}

function obtenir_normal() {
    let regne = Ingredient.Regnes[ingredientActuel.iRegne];
    encounter = textes["encounter_" + regne.refString]
    let introTexte = "",
        boutonTexte = "",
        disabled = false;

    let div = jQuery("<div>", {
        class: "description",
        style: "display: none;"
    });

    var nombreObtenu = obtenir();
    console.log(nombreObtenu)

    if (nombreObtenu) {
        //Bouton fuir
        introTexte = encounter["pick_conclusion"];
        boutonTexte = encounter["pick_button_leave"];
        disabled = false;

        let nomIng = ingredientActuel.nom;

        jQuery("<p>").html(accorderTexte(
            introTexte,
            genreBinaire,
            nomIng)).appendTo(div);

        let obtenu = true;
        if (nombreObtenu == "inventory_full")
            obtenu = false;

        if (obtenu) {
            jQuery("<p>", {
                class: "tip",
                id: "obtained"
            }).html(accorderTexte(
                textes.encounter_generic.tip_obtained,
                genreBinaire,
                nomIng,
                nombreObtenu)).appendTo(div);
        } else {
            jQuery("<p>", {
                class: "tip",
                id: "obtained"
            }).html(textes.encounter_generic.tip_inventory_full).appendTo(div);
        }

        jQuery("<input>", {
            type: "button",
            class: "interaction",
            id: "fuire",
            value: boutonTexte,
            disabled: disabled,
            click: fuir
        }).appendTo(div);
    }

    changerContenuCote(div);
}

function obtenir() {
    let miurge = ingredientActuel;

    var nombreRecolte = aventurier.obtenir(miurge);
    console.log("n récolte", nombreRecolte)
    if (nombreRecolte) {
        let div = jQuery("<div>", {
            class: "description",
            style: "display: none;"
        });

        // supprimer phrase ingrédient du texte de la pièce
        enleverTexteIngredient(miurge);

        return nombreRecolte;
    }

    return 0;
}

function apprivoiser(miurge) {
    let regne = Ingredient.Regnes[miurge.iRegne];
    encounter = textes["encounter_" + regne.refString]
    let introTexte = "",
        boutonTexte = "",
        disabled = false;


    if (aventurier.peutApprivoiser(miurge)) {
        let texteApprivoisement;
        if (aventurier.familierCheck()) {
            //Peut apprivoiser et l'emmener avec soi
            aventurier.apprivoiser(miurge);

            texteApprivoisement = textes.tame_creature_generic;
        } else {
            //Peut apprivoiser, mais a déjà un familier
            var familier = aventurier.familiers.splice(0, 1)[0];
            aventurier.chenil.push(familier);
            aventurier.apprivoiser(miurge);

            texteApprivoisement = textes.tame_creature_replace;
        }

        let div = jQuery("<div>", {
            class: "description",
            style: "display: none;"
        });
        div.append(document.createTextNode(texteApprivoisement));
        jQuery("<br/>").appendTo(div);

        //Bouton fuir
        introTexte = encounter["tame_intro"];
        boutonTexte = encounter["tame_button"];
        introButtonPair(introTexte, boutonTexte, apprivoiser_fin, div);

        changerContenuCote(div);
        // supprimer phrase ingrédient du texte de la pièce
    }
}

function apprivoiser_fin() {
    enleverTexteIngredient(ingredientActuel);

    let regne = Ingredient.Regnes[ingredientActuel.iRegne];
    encounter = textes["encounter_" + regne.refString]

    let introTexte = encounter["tame_intro"];
    let boutonTexte = encounter["tame_button"];
    introButtonPair(introTexte, boutonTexte, fuir, div);
}

function setupNommage() {
    //TODO : verifier verrouillage écriture
    //Verrouiller écriture
    ingredientActuel.verrouillerEcriture();

    let div = jQuery("<div>", {
        class: "description",
        style: "display: none;"
    });
    div.append(document.createTextNode(ingredientActuel.description));
    jQuery("<br/>").appendTo(div);

    let fonctionNommer = nommer;

    //Input texte
    let inputField = jQuery("<input>", {
        type: "text",
        name: "nom_ingredient",
        class: "nom_ingredient",
        id: "input_nom_ingredient",
        autocomplete: "off",
        keyup: function(event) {
            if ((event.keyCode == 13 || event.which == 13) && texteValide($(this))) {
                event.preventDefault();
                fonctionNommer();
            } else {
                if (!texteValide($(this)))
                    $('#nommer').prop("disabled", true);
                else
                    $('#nommer').prop("disabled", false);
            }
        }
    });
    inputField.appendTo(div);

    texteValide = function(element) {
        let value = element[0].value;
        return (value != "" && value.length >= 3 && value.length <= 30)
    }

    //Bouton nommer
    let boutonNommer = jQuery("<input>", {
        type: "button",
        class: "interaction",
        id: "nommer",
        disabled: "true",
        value: textes.interaction_name_button,
        click: fonctionNommer
    });
    boutonNommer.appendTo(div);

    //Bouton fuir
    jQuery("<input>", {
        type: "button",
        class: "interaction",
        id: "fuire",
        value: textes.text_name_creature_button_close,
        click: fuir
    }).appendTo(div);

    changerContenuCote(div);
}

function nommer() {
    let nom = $(".nom_ingredient").val();
    let uniciteNom = nomUnique(nom);
    if (nom == "") {
        let div = jQuery("<div>", {
            class: "description",
            style: "display: none;",
            text: textes.text_name_creature_error_01
        });
        jQuery("<input>", {
            type: "text",
            name: "nom_ingredient",
            class: "nom_ingredient",
            value: ""
        }).appendTo(div);
        jQuery("<input>", {
            type: "button",
            class: "interaction",
            id: "nommer",
            value: textes.interaction_name_button,
            click: nommer
        }).appendTo(div);

        changerContenuCote(div);
    } else if (uniciteNom == false) {
        let div = jQuery("<div>", {
            class: "description",
            style: "display: none;",
            text: textes.text_name_creature_error_02
        });
        jQuery("<input>", {
            type: "text",
            name: "nom_ingredient",
            class: "nom_ingredient",
            value: ""
        }).appendTo(div);
        jQuery("<input>", {
            type: "button",
            class: "interaction",
            id: "nommer",
            value: textes.interaction_name_button,
            click: nommer
        }).appendTo(div);

        changerContenuCote(div);
    } else {
        ingredientActuel.nom = nom;
        Ingredient.updateIngredientNom(ingredientActuel.nom, ingredientActuel.id, aventurier.nom, ingredientActuel);
        interactionReussie();

        let div = jQuery("<div>", {
            class: "description",
            style: "display: none;"
        });
        div.append(document.createTextNode(textes.text_name_creature_done));

        jQuery("<input>", {
            type: "button",
            class: "interaction",
            id: "fuire",
            disabled: false,
            value: textes.text_name_creature_button_close,
            click: initInteractionsCote
        }).appendTo(div);

        changerContenuCote(div);

        // changer le nom affiché dans le texte
        let divsIngredient = document.getElementsByClassName("ingredient");
        for (var i = 0; i < divsIngredient.length; i++) {
            if (divsIngredient[i].dataset.id == ingredientActuel.id) {
                divsIngredient[i].innerHTML = ingredientActuel.nom;
            }
        }
    }
}

function fuir() {
    fermerPanneauLateral();
    combatEnCours = false;
}

function texteApproche(nom, regne, genreBinaire, description) {
    let nommé = true;

    if (nom == "")
        nommé = false;

    if (nom == "")
        nom = regne.nom;

    let approche = accorderTextePlus(textes.encounter_generic, genreBinaire, "<label class='ingredient' title='" + description + "'>" + nom + "</label>");


    let texte;
    if (!nommé)
        texte = jQuery("<p>").html(approche.intro_unnamed);
    else if (aventurier.premiereRencontre(ingredientActuel.id)) {
        texte = jQuery("<p>").html(approche.intro_named_first);
        aventurier.rencontre(ingredientActuel.id);
    } else
        texte = jQuery("<p>").html(approche.intro_named);

    /*texte += accorderTexte(textes.text_creature_01a, genreBinaire);
    texte += "<label id='ingredient'>" + nom + "</label>";
    texte += accorderTexte(textes.text_creature_01b, genreBinaire);
    texte += "</p><form id='interactions'></form>";*/
    jQuery("</p><form id='interactions'></form>").appendTo(texte)

    return texte;
}

function nomUnique(nomPropose) {
    var ingsAvecNom = Ingredient.getIngredientsByNames([nomPropose.toLowerCase()]);
    return ingsAvecNom.length == 0;
}

function fermerPanneauLateral() {
    $('#contenu-cote').removeClass("opaque");
    $("#contenu-cote").on('webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd', function() {
        $(".contenu-principal").off('webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd');
        $("#bandeau-central-exploration").removeClass("desactive");
        $(".contenu-principal").removeClass("desactive");
        $('.contenu-principal a').removeClass("desactive");
        $('.contenu-principal .direction').removeClass("desactiveTemp");
        contenuCoteOuvert = false;
    });
}

function ajoutePiece(_id, _sorties, _x, _y, _description) {
    // si la pièce n'a pas encore été visitée l'ajouter aux pièces visitées
    var pieceExiste = false;

    for (var i = 0; i < pieceVisitees.length; i++) {
        if (pieceVisitees[i][0] == _description) {
            pieceExiste = true;
            break;
        }
    }
    if (!pieceExiste) {
        pieceVisitees.push([_description, _x, _y]);

    }
    // carte.ajoutePiece(_x, _y, derniereDirection);
    carte.majPlan({ x: _x, y: _y, accessibiliteVoisines: _sorties })

    //si pouvoir de carte, afficher la position du joueur
     if (aventurier.pouvoirs.positionJoueur || Constants.debug) {
    carte.dessinePieceActive(_x,_y);
}


    let sortieDansPiece = ((maison.sortie.x == _x && maison.sortie.y == _y) || pouvoirSortie);


    //Générer description interactive
    let grandeDescription = "<p>";
    for (let d of _description) {
        if (d.id != null)
            grandeDescription += "<span class='ramassable' id='" + d.id + "'>" + d.text + "</span>";
        else
            grandeDescription += "<span class=''>" + d.text + "</span>";

        grandeDescription += " ";
    }
    grandeDescription += "</p>";



    // requête ajax
    $.ajax({
        url: 'piece.php',
        type: 'GET',
        data: {
            numeroPiece: _id, //identifiant unique de la pièce
            sorties: _sorties, // identifiants des pièces adjecentes [nord, est, sud, ouest], -1 si pas d'adjacente dans une direction
            x: _x, // coordonnée x de la pièce
            y: _y, // coordonnée y de la pièce
            description: grandeDescription,
            sortieDonjon: sortieDansPiece
        },
        dataType: 'html', // On désire recevoir du HTML
        success: function(code_html, statut) { // code_html contient le HTML renvoyé
            $("#pieces").append(code_html);
            $("#bouton_nord").attr("value", textes.north);
            $("#bouton_sud").attr("value", textes.south);
            $("#bouton_est").attr("value", textes.east);
            $("#bouton_ouest").attr("value", textes.west);
            $("#bouton_sortie").attr("value", textes.exit);
            mettreAJourNomIngredient();
        },
        complete: function() {
            //transition
            if ($("#pieces").children().length > 1) {
                if (derniereDirection == "sud") {
                    $("#pieces .piece:eq(-1)").addClass("arriveSud");
                    $("#pieces .piece:eq(-2)").addClass("departSud");
                }

                if (derniereDirection == "est") {
                    $("#pieces .piece:eq(-1)").addClass("arriveEst");
                    $("#pieces .piece:eq(-2)").addClass("departEst");
                }

                if (derniereDirection == "ouest") {
                    $("#pieces .piece:eq(-1)").addClass("arriveOuest");
                    $("#pieces .piece:eq(-2)").addClass("departOuest");
                }

                if (derniereDirection == "nord") {
                    $("#pieces .piece:eq(-1)").addClass("arriveNord");
                    $("#pieces .piece:eq(-2)").addClass("departNord");
                }

                $("#pieces .piece:eq(-1)").on('webkitTransitionEnd oTransitionEnd transitionend msTransitionEnd', function() {
                    $("#pieces .piece:eq(-2)").remove();
                });
            }
        }
    });
}
//garder les parties commentées ou les archiver dans une branche du git pour la version lovecraft (avec combat)
function attaquer(event) {
    combatEnCours = true;

    let div = jQuery("<div>", {
        class: "description",
        style: "display: none;"
    });
    document.createTextNode(ingredientActuel.chant)
    div.html(ingredientActuel.chant);
    jQuery("<br/><br/>").appendTo(div);

    //Suggestion si pouvoir.chant
    let pouvoirChant = aventurier.pouvoirs.chant;
    if (pouvoirChant > 0) {
        let inputs = div.find(".input-chant");
        for (let i = 0; i < inputs.length; i++) {
            let sub = inputs[i].name.substring(0, Constants.pouvoirChantSuggestion(pouvoirChant));
            inputs[i].setAttribute("placeholder", sub);
        }
    }

    //Bouton finCombat
    jQuery("<input>", {
        type: "button",
        class: "interaction",
        id: "finCombat",
        value: encounter.sing_button_validation,
        click: combatFini
    }).appendTo(div);
    changerContenuCote(div);
}

function enleverVieOpposant() {
    let typeAttaque;
    let nombreAttaque = 0;
    let texteEstChange = false;
    //regarder si un type d'attaque est actif
    if (!($('.input-action[name=convaincre]').prop("disabled"))) {
        typeAttaque = 0;
        if ($('.input-action[name=convaincre]').val() != $('.input-action[name=convaincre]').attr('value'))
            texteEstChange = true;
        nombreAttaque++;
    }
    if (!($('.input-action[name=intimider]').prop("disabled"))) {
        typeAttaque = 1;
        if ($('.input-action[name=intimider]').val() != $('.input-action[name=intimider]').attr('value'))
            texteEstChange = true;
        nombreAttaque++;
    }
    if (!($('.input-action[name=seduire]').prop("disabled"))) {
        typeAttaque = 2;
        if ($('.input-action[name=seduire]').val() != $('.input-action[name=seduire]').attr('value'))
            texteEstChange = true;
        nombreAttaque++;
    }

    // si un seul type d'attaque : enlever PV
    if (nombreAttaque == 1) {

        let degats = ingredientActuel.stats_combat[typeAttaque] - aventurier.getLesStatsTotales().volonte;
        if (degats < 0) {
            //ingredientActuel.pointsDeTemps += degats;
            ingredientActuel.pointsDeTemps -= 1;
        }
        // else
        //changerPointsDeTemps(-degats);
        // changerPointsDeTemps(-10);
    }

}

function combatFini() {
    let bonsTextes = true;
    let tousLesChamps = $(".input-chant").toArray();

    //déterminer si les champs sont bien remplis
    if (!Constants.debug) {
        for (var i = 0; i < tousLesChamps.length; i++) {
            if (tousLesChamps[i].value.toLowerCase() != tousLesChamps[i].name.toLowerCase())
                bonsTextes = false;
        }
    }


    let genreBinaire = ingredientActuel.genre == "masculin" ? 1 : 0;
    let regne = Ingredient.Regnes[ingredientActuel.iRegne];
    encounter = textes["encounter_" + regne.refString]
    encounter = accorderTextePlus(encounter, genreBinaire, ingredientActuel.nom);
    let introTexte = "",
        boutonTexte = "",
        disabled = false;

    let div = jQuery("<div>", {
        class: "description",
        style: "display: none;"
    });

    if (bonsTextes) {
        ingredientActuel.chantReussi = true;
        let versNum = tousLesChamps[0].getAttribute("versNum")
        aventurier.apprendVers(versNum)
        interactionReussie()

        div.append(encounter["sing_conclusion_success"]);

        //Bouton apprivoiser
        /* introTexte = encounter["tame_intro"];
         boutonTexte = encounter["tame_button"];
         disabled = !aventurier.peutApprivoiser(ingredientActuel);
         introButtonPair(introTexte, boutonTexte, apprivoiser, div, disabled, "tame");

         if (disabled) {
             //Bouton apprivoiser grisé --> explication
             let tip = textes.interaction_tame_impossible_explain.replace("_regne_", Ingredient.Regnes[ingredientActuel.iRegne].nom);
             jQuery("<p>", {
                 class: "tip",
                 id: "tame_explain"
             }).html(tip).appendTo(div);
         }*/

        //Bouton acquisition
        introTexte = encounter["pick_intro"];
        boutonTexte = encounter["pick_button"];
        introButtonPair(introTexte, boutonTexte, obtenir_normal, div);

        //Bouton fuir
        introTexte = encounter["sing_conclusion_success_leave_intro"];
        boutonTexte = encounter["sing_conclusion_success_leave_button"];
        introButtonPair(introTexte, boutonTexte, fuir, div);
    } else {
        ingredientActuel.chantReussi = false;

        div.append(encounter["sing_conclusion_failure"]);

        //Bouton fuir
        introTexte = encounter["sing_conclusion_failure_leave_intro"];
        boutonTexte = encounter["sing_conclusion_failure_leave_button"];
        introButtonPair(introTexte, boutonTexte, fuir, div);
        enleverTexteIngredient(ingredientActuel);
    }
    changerContenuCote(div);
}

function introButtonPair(intro, button, callback, div, disabled = false, id = "fuire") {
    jQuery("<p>").html(intro).appendTo(div);
    jQuery("<input>", {
        type: "button",
        class: "interaction",
        id: id,
        value: button,
        disabled: disabled,
        click: callback
    }).appendTo(div);
}

function interactionReussie() {
    if (aventurier.pouvoirs.rechargeTempsAInteraction != null) {
        aventurier.gainTemps(distanceRegionActuelle * aventurier.pouvoirs.rechargeTempsAInteraction)
    }
}

function updateColorPreviewHSV(_h, _s, _l) {
    return "hsl(" + _h + "," + _s + "%," + _l + "%)";
}
/*
function random(_min, _max) {
    return Math.floor(Math.random() * Math.floor(_max - _min) + _min);
}
*/
function ejection() {
    sortir(textes.text_kick);
}

function sortir(annonce) {
    //changer le voronoiId pour empècher l'actualisation du donjon
    $.ajax({
        url: 'modifierSession.php',
        type: 'GET',
        async: false,
        data: {
            idVoronoiActuel: -1,
            distanceVoronoiActuel: -1
        }
    });

    if (aventurier.quetes.explore == QueteFlag.IN_PROGRESS)
        aventurier.quetes.explore = QueteFlag.FINISHED;

    // let div = '<div">C\'est gagné !\
    //  </br><a href="hub.php">Retourner à l\'hôtel</a>\
    //   </div>';

    let div = document.createElement("div");
    let p = document.createElement("p");

    p.innerHTML = annonce;

    div.appendChild(p);
    div.innerHTML += "<br/>";


    let texteSortie = "";

    //Actions de stockage
    /*
    if (aventurier.inventaire.length > 0) {

        //p.innerHTML = "C\'est gagné ! Que voulez-vous faire des ingrédients collectés ?";

        var divActions = jQuery("<div></div>");
        divActions.appendTo(div);

        jQuery("<input/>", {
            type: "button",
            value: 'Stocker les miurges dans mon coffre',
            click: stockerIngredientsChambre
        }).appendTo(divActions);

        jQuery("<br>", {}).appendTo(divActions);
    }*/

    if (aventurier.objets != null) {
        for (let o of aventurier.objets) {
            aventurier.vestiaire.push(o);
        }
    }

    texteSortie = textes.button_exit;

    //Retour hotel
    jQuery("<input/>", {
        type: "button",
        value: texteSortie,
        click: retourHotel
    }).appendTo(div);

    //Ouverture
    ouvrirPopUp(div);

    aventurier.sauvegarde();
}

function stockerIngredientsChambre() {
    aventurier.coffre = aventurier.coffre.concat(aventurier.inventaire);
    aventurier.inventaire = [];

    aventurier.sauvegarde();
    aventurier.sauvegardePhrases();

    window.location.href = "hub.php";
}

function retourHotel() {
    aventurier.sauvegarde();
    aventurier.reset();

    //aventurier.sauvegardePhrases();

    window.location = "hub.php";
}

function mettreAJourNomIngredient() {

    //regader si il y a un ingrédient dans la pièce
    let divsIngredient = document.getElementsByClassName("ingredient");
    let ingredientID;
    let infosIngredient;

    for (var i = 0; i < divsIngredient.length; i++) {
        //récuperer les éventuelles nouvelles infos sur l'ingrédient
        ingredientID = divsIngredient[i].dataset.id;
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && (xmlhttp.status == 200 || xmlhttp.status == 0)) {

                let divsIngredient = document.getElementsByClassName("ingredient");
                let ingredientID;
                let infosIngredient;
                let divPiece;

                infosIngredient = JSON.parse(xmlhttp.responseText)[0].nom;




                // mettre à jour la description de l'ingrédient
                for (var i = 0; i < divsIngredient.length; i++) {

                    divPiece = divsIngredient[i].closest(".piece");
                    if (divPiece.dataset.x != positionJoueur[0] || divPiece.dataset.y != positionJoueur[1])
                        continue;

                    if (infosIngredient != "" && infosIngredient != divsIngredient[i].innerHTML) {
                        //si l'ingredient n'est pas dans la pièce actuelle

                        //met à jour le texte affiché
                        divsIngredient[i].innerHTML = infosIngredient;
                        // met à jour les infos stockées coté js (client)
                        salle.contenu[divsIngredient[i].dataset.index].nom = infosIngredient;
                    }
                }
            }
        };

        xmlhttp.open("GET", "gettersBDD/getIngredientById.php?id=" + ingredientID, true);
        xmlhttp.send(null);
    }
}

function changerContenuCote(_texte) {
    $("#contenu-cote .description:eq(-1)").fadeOut("slow", function() {
        $("#contenu-cote .description:eq(-1)").remove();
        $("#contenu-cote").append(_texte);
        $("#contenu-cote .description:eq(-1)").fadeIn("slow", function() {});
    });
}


function enleverTexteIngredient(ingredient) {
    let divsIngredient = document.getElementsByClassName("ingredient");
    let index = 0;
    for (var i = 0; i < divsIngredient.length; i++) {
        if (ingredient.id == divsIngredient[i].dataset.id)
            index = divsIngredient[i].dataset.index;
        divsIngredient[i].parentNode.parentNode.remove();
    }

    // Suppression de l'ingrédient depuis le contenu de la pièce, et sa liste de descriptions
    salle.contenu.splice(0, 1);

    let a = salle.findIndexDescriptionIngredient();
    salle.description.splice(a, 1);
}

function actualiserJaugeTemps() {
    $("#myBar").css("width", aventurier.vie / aventurier.vieMax * 100 + "%");
    if (aventurier.vie / aventurier.vieMax < .25)
        $("#myBar").css("background-color", "#dd4b4b");

}

function parseTextes() {
    $("#bouton_nord").attr("value", textes.north);
    $("#bouton_sud").attr("value", textes.south);
    $("#bouton_est").attr("value", textes.east);
    $("#bouton_ouest").attr("value", textes.west);
    /* $("#nav_bouton_personnage").html(textes.nav_button_player);
     $("#nav_bouton_carte").html(textes.nav_button_map);*/

    $("#titre-suite").html("SUITE n°" + regionActuelle);
    $("#titre-temps").html("TEMPS DE VISITE");

    new Tooltip($('#nav_bouton_personnage'), {
        placement: 'right',
        html: true,
        title: textes.nav_button_player,
    });

    new Tooltip($('#nav_bouton_carte'), {
        placement: 'right',
        html: true,
        title: textes.nav_button_map,
    });
}