const MAX_TAILLE_SALLE = 5;
const CONNECTIVITE = 0.8; //A quel point les salles sont entreconnectées. 1 : tout connecté. 0 : rien.

var regionActuelle = -1; //TODO à remplacer par la région sélectionnée sur la carte pour l'exploration.
var distanceRegionActuelle = 0;

var chant;

var pieceIntro,
    pieceSols,
    pieceMurs,
    pieceFenetres,
    piecePhraseCreature,
    pieceInterrupteur,
    pieceSecrete;

var ingredientsDisponibles = [];
var frequencesTotal;

// utiliser quand les zones seront peuplées, en attendant on laisse l'id 'en dur'
getIdVoronoi();

getLesDescriptions();

getChant();


function chance(ratio) {
    return Math.random() < ratio;
}

function getIdVoronoi() {
    var objSession = JSON.parse($.ajax({ data: "", type: "POST", url: "exposeSession.php", async: false }).responseText);
    regionActuelle = parseInt(objSession.idVoronoiActuel);
    distanceRegionActuelle = parseInt(objSession.distanceVoronoiActuel);
}

function getLesDescriptions() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && (xmlhttp.status == 200 || xmlhttp.status == 0)) {
            var obj = JSON.parse(this.responseText);
            pieceIntro = obj.base;
            pieceSols = obj.sol;
            pieceMurs = obj.mur;
            pieceFenetres = obj.fenetre;
            piecePhraseCreature = obj.creature;
            pieceInterrupteur = obj.interrupteur;
            pieceSecrete = obj.secret;
            //accordeLesDescriptions();
        }
    };
    xmlhttp.open("GET", "medias/pieces.json", false);
    xmlhttp.send(null);
}

function getChant() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && (xmlhttp.status == 200 || xmlhttp.status == 0)) {
            var obj = JSON.parse(this.responseText);
            chant = obj.vers;
        }
    };
    xmlhttp.open("GET", "medias/chant.json", false);
    xmlhttp.send(null);
}

function genereBlocCreature(ingredient, index) {
    // var creatureAleatoire = random(ingredientsEndemiques.length);
    if (ingredient == null) return;
    var gender = ingredient.genre == "masculin" ? 0 : 1;
    var str = piecePhraseCreature[random(piecePhraseCreature.length)].text; //piecePhraseCreature[i];
    var res = str.split("==");

    for (var j = 0; j < res.length; j++) {
        // mots à accorder
        if (res[j].includes("--")) {
            var genders = res[j].split('--');
            res[j] = genders[gender];
        }


        var str = piecePhraseCreature[random(piecePhraseCreature.length)].text; //piecePhraseCreature[i];
        var res = str.split("==");
        for (var j = 0; j < res.length; j++) {
            // mots à accorder
            if (res[j].includes("--")) {
                var genders = res[j].split('--');
                res[j] = genders[gender];
            } else if (res[j] == "creature" || res[j] == "créature") {
                var nom = ingredient.nom;

                res[j] = "";
 let regne = Ingredient.Regnes[ingredient.iRegne];
                if (nom == "" || nom == null) { //Si l'ingrédient n'est pas nommé, écrire sa définition 
                   
                    res[j] += "<span class='span_ingredient'> <a class='ingredient "+regne.nom+"' href='' data-id='" + ingredient.id + "'  data-index='" + index + "'>" + regne.nom + "</a></span>";
                } else //Si l'ingrédient est nommé, écrire son nom
                    res[j] += "<span class='span_ingredient'><a class='ingredient "+regne.nom+"' href='' data-id='" + ingredient.id + "' data-index='" + index + "'>" + nom + "</a></span>";
                // res[j] = ingredientsEndemiques[creatureAleatoire].def;
            }
        }
    }


    return ('<span class="phraseIngredient">' + res.join('') + '</span>');
}

function getLesIngredients(maison) {
    //ingredientsDisponibles contient les ingrédients de la région (en double pour + de probabilité) et les ingrédients des régions adjacentes.
    var ingredientsEndemiques = Ingredient.getIngredientsByRegion(regionActuelle);
    // ingredientsDisponibles = Ingredient.getIngredientsByRegions(getRegionsAdjacentes());
    // ingredientsDisponibles = ingredientsDisponibles.concat(ingredientsEndemiques).concat(ingredientsEndemiques);

    ingredientsDisponibles = ingredientsEndemiques;


    for (let ing of ingredientsDisponibles) {
        let intensite = 0;
        if (aventurier.pouvoirs.chancesApparitionIng != null) {
            let bonus = aventurier.pouvoirs.chancesApparitionIng.find(x => x.indexRegne == ing.iRegne);
            intensite = bonus != null ? bonus.intensite : 0;
        }
        ing.frequence = Ingredient.Regnes[ing.iRegne].frequence + 0.02 * intensite;
    }

    frequencesTotal = 0;
    for (let r of ingredientsDisponibles) frequencesTotal += r.frequence;

    definirInteractions(ingredientsDisponibles);
}

function getRegionsAdjacentes() {
    var data = $.ajax({
        type: "GET",
        url: "gettersBDD/getRegions.php",
        async: false
    });

    return JSON.parse(data.responseText);
}

function definirInteractions(ings) {
    for (var ing of ings) {

        // ing.interactions = getLesInteractions(ing.type);

        // définir le nombre de points de temps
        ing.pointsDeTemps = ing.stats.moral;
    }
}

function populerLesSalles(maison, ingredients) {
    for (var i in maison.plan) {
        for (var j in maison.plan[i]) {
            if (maison.plan[i][j] == null) continue;

            var salle = maison.plan[i][j];

            salle.genererDescription(ingredients, maison.magnitude);
        }
    }
}

function genererChant(magnitude) {
    let chantIngredient = "";
    let limite = chant.length - 3;
    if (magnitude > 0)
        limite = Math.max(limite * magnitude / Constants.magnitudeMaximale, 3);
    else limite = 3;

    let debutChant = Math.floor(Math.random()**1.4 * limite);
    let chantTableau = chant.slice(debutChant, debutChant + 3);
    
    for (var i = 0; i < chantTableau.length; i++) {

        chantIngredient = chantIngredient.concat(chantTableau[i]);
        if (i < chantTableau.length - 1)
            chantIngredient = chantIngredient.concat("</br>");

    }

    let _str = chantIngredient.split("==");
    // trouve combien de trous il y a
    let trous = [];
    for (var j = 0; j < _str.length; j++) {
        if (_str[j].charAt(0) == ">") {
            trous.push(j);
            _str[j] = _str[j].substr(1);
        }
    }

    trous = shuffle(trous);
    trous = trous.slice(0, 2);
    for (var j = 0; j < _str.length; j++) {
        for (var k = 0; k < trous.length; k++) {
            if (j == trous[k]) {
                _str[j] =
                    '<input class="input-action input-chant" type="text" name="'
                    .concat(_str[j])
                    .concat('" value=""')
                    .concat("versNum=")
                    .concat(debutChant)
                    .concat(">")
            }
        }
    }
    chantIngredient = _str.join("");


    return chantIngredient;
}

function shuffle(array) {
    for (let i = array.length - 1; i > 0; i--) {
        let j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

function choisirLesIngredients(salle, ingredients) {
    var a = [];
    for (var i = 0; i < salle.taille; i++)
        a[i] = ingredients[random(ingredients.length)];
    return a;
}

function Maison( /*region, largeur, hauteur,_nombrePieces,*/ _departJoueur) {
    //regionActuelle = region;

    Salle.NORD = 0;
    Salle.SUD = 1;
    Salle.EST = 2;
    Salle.OUEST = 3;
    Salle.NONE = 4;

    this.magnitude = distanceRegionActuelle;
    this.region = regionActuelle;
    let taille = Constants.tailleDonjon(distanceRegionActuelle);
    this.largeur = taille;
    this.hauteur = taille;
    this.nombrePieces = taille;

    console.log(taille)


    getLesIngredients(this);

    this.plan = new Array(this.largeur);
    for (var i = 0; i < this.largeur; i++) {
        this.plan[i] = new Array(this.hauteur);
    }
    /*this.largeur = largeur;
    this.hauteur = hauteur;*/

    // générateur version B
    this.ajoutePiece = function(x, y) {
        // ajoute une pièce
        planEmplacements[x][y] = PIECE;

        // met à jour la liste des pièces possibles
        if (x - 1 >= 0 && planEmplacements[x - 1][y] == VIDE)
            planEmplacements[x - 1][y] = MUR;
        if (x + 1 < this.largeur && planEmplacements[x + 1][y] == VIDE)
            planEmplacements[x + 1][y] = MUR;
        if (y - 1 >= 0 && planEmplacements[x][y - 1] == VIDE)
            planEmplacements[x][y - 1] = MUR;
        if (y + 1 < this.hauteur && planEmplacements[x][y + 1] == VIDE)
            planEmplacements[x][y + 1] = MUR;
    }

    // let departJoueur = _departJoueur // remplacer par paramètre

    let departJoueur = [random(this.largeur), random(this.hauteur)];
    this.departJoueur = departJoueur;

    let planEmplacements = new Array(this.largeur); // array de int 0==>pièce impossible 1==>pièce possible 2==>pièce existante
    let listeEmplacementsLibres = [];

    if (this.nombrePieces > this.largeur * this.hauteur)
        this.nombrePieces = this.largeur * this.hauteur;

    //crée un plan vide
    for (var i = 0; i < this.largeur; i++) {
        planEmplacements[i] = new Array(this.hauteur);
    }

    for (var i = 0; i < this.largeur; i++) {
        for (var j = 0; j < this.hauteur; j++) {
            planEmplacements[i][j] = 0;
        }
    }

    //crée l'emplacement de la première pièce
    this.ajoutePiece(departJoueur[0], departJoueur[1]);

    // ajoute les emplacements des autres pièces
    for (var i = 0; i < this.nombrePieces - 1; i++) {
        // liste les emplacements libre
        listeEmplacementsLibres = new Array();
        for (var k = 0; k < this.largeur; k++) {
            for (var j = 0; j < this.hauteur; j++) {
                if (planEmplacements[k][j] == 1)
                    listeEmplacementsLibres.push([k, j]);
            }
        }
        // ajoute une pièce dans un des emplacements libre
        let rand = random(listeEmplacementsLibres.length);
        this.ajoutePiece(listeEmplacementsLibres[rand][0], listeEmplacementsLibres[rand][1]);
    }

    //préparation d'une case sortie
    this.sortie = { x: departJoueur[0], y: departJoueur[1] };
    // var aleatoire = random(this.nombrePieces);

    //crée les pièces à partir des emplacements définis et  définition de la sortie
    let aleatoire = random(this.largeur * this.hauteur - listeEmplacementsLibres.length);
    for (var i = 0; i < this.largeur; i++) {
        for (var j = 0; j < this.hauteur; j++) {
            if (planEmplacements[i][j] == 2) {
                this.plan[i][j] = new Salle(random(MAX_TAILLE_SALLE) + 1, i, j);
                if (aleatoire == 0)
                    this.sortie = { x: i, y: j };
                aleatoire -= 1;
            } else
                this.plan[i][j] = null;
        }
    }

    //Création des connexions entre salles
    for (var i = 0; i < this.largeur; i++) {
        for (var j = 0; j < this.hauteur; j++) {
            if (this.plan[i][j] == null) continue;

            if (this.plan[i][j].salleSecrete == true) {
                // debugger;
                continue;
            }

            for (var k = 0; k < this.plan[i][j].accessibiliteVoisines.length; k++) {
                this.plan[i][j].accessibiliteVoisines[k] = false;
            }

            if (i - 1 >= 0 && planEmplacements[i - 1][j] == 2)
                this.plan[i][j].accessibiliteVoisines[3] = true; //OUEST
            if (i + 1 < this.largeur && planEmplacements[i + 1][j] == 2)
                this.plan[i][j].accessibiliteVoisines[2] = true; //EST
            if (j - 1 >= 0 && planEmplacements[i][j - 1] == 2)
                this.plan[i][j].accessibiliteVoisines[0] = true; //NORD
            if (j + 1 < this.hauteur && planEmplacements[i][j + 1] == 2)
                this.plan[i][j].accessibiliteVoisines[1] = true; //SUD
        }
    }


    //Redéfinition de l'entrée pour la priver d'ingrédients
    // var entree = this.plan[0][0];


    let me = this;

    //Populer les salles d'ingrédients
    populerLesSalles(this, ingredientsDisponibles);

    //Creer des salles secretes, pièces accessibles pas un interrupteur et contenant du loot
    creerSallesSecretes(1, planEmplacements);

    function creerSallesSecretes(nbr, planEmplacements) {
        // debugger;
        let candidats = [];
        for (let i = 0; i < planEmplacements.length; i++) {
            for (let j = 0; j < planEmplacements[i].length; j++) {
                if (planEmplacements[i][j] == 1) {
                    candidats.push({ x: i, y: j });
                }
            }
        }

        if (candidats.length <= 0) return;

        let elui = random(candidats.length);
        let elu = candidats[elui];
        let i = elu.x;
        let j = elu.y;
        me.plan[i][j] = new SalleSecrete(i, j);
        let salleSecrete = me.plan[i][j]

        me.salleSecrete = me.plan[i][j];

        //// Créer interrupteur d'accès à la salle secrète.
        //Choisir salle adjacente accessible (2)
        let salleAcces;

        candidats = [];
        if (i - 1 >= 0 && planEmplacements[i - 1][j] == PIECE) {
            candidats.push({
                salle: me.plan[i - 1][j],
                porteSecrete: EST,
                sortieSalleSecrete: OUEST
            });
        }
        if (i + 1 < me.largeur && planEmplacements[i + 1][j] == PIECE) {
            candidats.push({
                salle: me.plan[i + 1][j],
                porteSecrete: OUEST,
                sortieSalleSecrete: EST
            });
        }
        if (j - 1 >= 0 && planEmplacements[i][j - 1] == PIECE) {
            candidats.push({
                salle: me.plan[i][j - 1],
                porteSecrete: SUD,
                sortieSalleSecrete: NORD
            });
        }
        if (j + 1 < me.hauteur && planEmplacements[i][j + 1] == PIECE) {
            candidats.push({
                salle: me.plan[i][j + 1],
                porteSecrete: NORD,
                sortieSalleSecrete: SUD
            });
        }

        if (candidats.length == 0) return;

        elu = candidats[random(candidats.length)];
        salleAcces = elu.salle;
        salleSecrete.accessibiliteVoisines[elu.sortieSalleSecrete] = true
        salleAcces.porteSecrete = elu.porteSecrete;
        me.plan[i][j].salleAcces = salleAcces;

        //Ajouter un mot au texte, qui au clic débloque la coordonnée (N/S/E/O) menant à la salle secrète 
        me.plan[i][j].ajouteInterrupteur(salleAcces);
        console.log(pieceSecrete[0])
        me.plan[i][j].ajouterElementDescriptif(pieceSecrete[0].text);
        me.plan[i][j].ajouteCoffre();
    }

    this.getDirectionSalleSecrete = function(posJoueur) {
        let salleSecrete = this.salleSecrete.salleAcces;


        let xS = salleSecrete.x,
            yS = salleSecrete.y,
            xJ = posJoueur.x,
            yJ = posJoueur.y;
        let deltaX = xS - xJ,
            deltaY = yS - yJ;

        // Axe EST-OUEST
        if (xS == xJ && yS == yJ) {
            return Salle.NONE;
        } else if (Math.abs(deltaX) > Math.abs(deltaY)) {
            if (deltaX > 0) return Salle.EST;
            else return Salle.OUEST;
        } else {
            if (deltaY > 0) return Salle.SUD;
            else return Salle.NORD;
        }
    }

}

function map_range(value, low1, high1, low2, high2) {
    return Math.floor(low2 + (high2 - low2) * (value - low1) / (high1 - low1));
}


function Salle(taille, x, y) {
    this.description = [];
    this.taille = taille;
    this.x = x;
    this.y = y;

    //Obtenir contenu aléatoire, puis générer la description en fonction.
    this.contenu = [];

    this.accessibiliteVoisines = new Array(4);

    let r = Math.random().toString(36).substring(7);
    this.nom = r;
}

Salle.direction = function(directionEnum) {
    switch (directionEnum) {
        case Salle.NORD:
            return "au nord d'ici";
        case Salle.SUD:
            return "au sud d'ici";
        case Salle.EST:
            return "à l'est d'ici";
        case Salle.OUEST:
            return "à l'ouest d'ici";
        case Salle.NONE:
            return "ici-même";
    }
}

Salle.prototype.findIndexDescriptionIngredient = function() {
    return this.description.findIndex(x => x.text.includes("phraseIngredient"));
}

Salle.prototype.ajouterElementDescriptif = function(texte) {
    this.description[this.description.length] = { text: texte };
}

Salle.prototype.genererDescription = function(ingredients, magnitude) {
    var description = "";
    var tableauDescription = [];
    //Texte intro
    description = pieceIntro[random(pieceIntro.length)];

    //Sol
    if (chance(0.6))
        tableauDescription.push(pieceSols[random(pieceSols.length)]);

    //Mur
    if (chance(0.6))
        tableauDescription.push(pieceMurs[random(pieceMurs.length)]);

    //Fenetre
    if (chance(0.6))
        tableauDescription.push(pieceFenetres[random(pieceFenetres.length)]);

    //créature
    if (chance( /*0.6*/ 1)) {
        let rand = random(frequencesTotal * 100) / 100;
        let sum = 0;
        let _ingredient;
        for (let r of ingredientsDisponibles) {
            sum += r.frequence;
            if (rand < sum) {
                _ingredient = Object.create(r);
                break;
            }
        }

        // var _ingredient = Object.create(ingredients[random(ingredients.length)]);
        _ingredient.chant = genererChant(magnitude);
        if (_ingredient.agressif && !Constants.agressivite) _ingredient.agressif = false;
        this.contenu.push(_ingredient);

        tableauDescription.push({ text: genereBlocCreature(_ingredient, this.contenu.length - 1) });
    }

    shuffle(tableauDescription);

    /*for (var i = 0; i < tableauDescription.length; i++) {
        description += tableauDescription[i];
    }*/

    description = tableauDescription;

    this.description = description;
}


function SalleSecrete(x, y) {
    Salle.call(this, 1, x, y);

    this.salleSecrete = true;
    for (let i = 0; i < 4; i++)
        this.accessibiliteVoisines[i] = false
}

//Inherit the properties from SuperType
SalleSecrete.prototype = new Salle();

SalleSecrete.prototype.ajouteCoffre = function() {
    let boutonCoffre = '<input type="button" id="coffre" value="coffre" onClick=ouvrirCoffre(this)>';
    this.ajouterElementDescriptif(boutonCoffre);
}


SalleSecrete.prototype.ajouteInterrupteur = function(salle) {
    if (salle == null) return;

    /*let button = jQuery("<input>", {
        type: "button",
        id: "interrupteur",
        value: "interrupteur",
        click: function() { alert("hi"); }
    });*/

    let i = random(pieceInterrupteur.length);

    let interrupteur = pieceInterrupteur[i].text.replace("__portei__", salle.porteSecrete);

    // let button = '<input type="button" id="interrupteur" value="interrupteur" porteSecrete=' + salle.porteSecrete + '>';

    let rand = random(salle.description.length);
    salle.description.splice(rand, 0, {
        text: interrupteur
    });

    salle.indexDescriptionInterrupteur = rand;
}


/*SalleSecrete.prototype.ouvrirAccesSalleSecrete = function() {
    console.log(this);
}


function ouvrirAccesSalleSecrete(button) {
    let porteSecrete = parseInt(button.getAttribute("porteSecrete"));

    //Obtenir le bouton correspondant
    let id = "bouton_";
    if (porteSecrete == 0)
        id += "nord"
    else if (porteSecrete == 1)
        id += "sud"
    else if (porteSecrete == 2)
        id += "est"
    else if (porteSecrete == 3)
        id += "ouest"

    //salleActuelle update accessibiliite voisines
    salle.accessibiliteVoisines[porteSecrete] = true;

    //Enlever class desactive
    $('#'+id).removeClass("desactive");
    $('#'+id).addClass("active");


    var boutonCardinal = document.getElementById(id);
    boutonCardinal.classList.remove("desactive");
    boutonCardinal.classList.add("active");
    boutonCardinal.disabled = false;


    $('#'+id).load("demo_test.txt"); 

    // button.disabled = true;


    console.log(boutonCardinal, document.getElementById(id));
}
*/
$(document).ready(function() {
    $("#pieces").on("click", "#interrupteur", function() {

        console.log(maison, salle)
        // maison.salleSecrete.salleAcces.description[0]

        let porteSecrete = parseInt($(this)[0].getAttribute("porteSecrete"));

        //Obtenir le bouton correspondant
        let id = "#bouton_";
        if (porteSecrete == 0)
            id += "nord"
        else if (porteSecrete == 1)
            id += "sud"
        else if (porteSecrete == 2)
            id += "est"
        else if (porteSecrete == 3)
            id += "ouest"
        $(id).removeClass("desactive");
        $(id).addClass("active");

        //animation
        particlesInit($(id).offset().top + $(id).width() / 2, $(id).offset().left + $(id).height() / 2);

        let interrupteur = $('#interrupteur')
        let parent = interrupteur.parent();
        parent.html(interrupteur.attr("newText"));

        if(salle.indexDescriptionInterrupteur != null && salle.indexDescriptionInterrupteur >= 0) {
            salle.description[salle.indexDescriptionInterrupteur] = { text: interrupteur.attr("newText")};
            console.log(salle.description)

            salle.accessibiliteVoisines[porteSecrete] = true
        }
       
        console.log(parent)
    });
});


var OUEST = 3,
    EST = 2,
    NORD = 0,
    SUD = 1;

var PIECE = 2,
    MUR = 1,
    VIDE = 0






// source https://css-tricks.com/adding-particle-effects-to-dom-elements-with-canvas/

//var btn = document.querySelector("button");
//var ctx;

//var colorInfoElem = document.querySelector("#colorInfo");
//html2canvas(btn).then(canvas => {
// ctx = canvas.getContext("2d");

createParticleCanvas();

let reductionFactor = 17;
/*btn.addEventListener("click", e =>*/
function particlesInit(x, y) {
    // Get the color data for our button
    /*let width = btn.offsetWidth;
    let height = btn.offsetHeight
    let colorData = ctx.getImageData(0, 0, width, height).data;*/

    // Keep track of how many times we've iterated (in order to reduce
    // the total number of particles create)
    let count = 0;

    console.log(x, y);
    // Go through every location of our button and create a particle

    for (let i = 0; i < 80; i++) {
        //if(count % reductionFactor === 0) {

        createParticleAtPoint(y, x, [58, 58, 58]);
        // }
        // count++;
    }

} /*)*/ ;
//});


/* An "exploding" particle effect that uses circles */
var ExplodingParticle = function() {
    // Set how long we want our particle to animate for
    this.animationDuration = 1000; // in ms

    // Set the speed for our particle
    this.speed = {
        x: -5 + Math.random() * 10,
        y: -5 + Math.random() * 10
    };

    // Size our particle
    this.radius = 5 + Math.random() * 5;

    // Set a max time to live for our particle
    this.life = 30 + Math.random() * 10;
    this.remainingLife = this.life;

    // This function will be called by our animation logic later on
    this.draw = ctx => {
        let p = this;

        if (this.remainingLife > 0 &&
            this.radius > 0) {
            // Draw a circle at the current location
            ctx.beginPath();
            ctx.arc(p.startX, p.startY, p.radius, 0, Math.PI * 2);
            ctx.fillStyle = "rgba(" + this.rgbArray[0] + ',' + this.rgbArray[1] + ',' + this.rgbArray[2] + ", 1)";
            ctx.fill();

            // Update the particle's location and life
            p.remainingLife--;
            p.radius -= 0.25;
            p.startX += p.speed.x;
            p.startY += p.speed.y;
        }
    }
}

var particles = [];

function createParticleAtPoint(x, y, colorData) {
    let particle = new ExplodingParticle();
    particle.rgbArray = colorData;
    particle.startX = x;
    particle.startY = y;
    particle.startTime = Date.now();

    particles.push(particle);
}


var particleCanvas, particleCtx;

function createParticleCanvas() {
    // Create our canvas
    particleCanvas = document.createElement("canvas");
    particleCtx = particleCanvas.getContext("2d");

    // Size our canvas
    particleCanvas.width = window.innerWidth;
    particleCanvas.height = window.innerHeight;

    // Position out canvas
    particleCanvas.style.position = "absolute";
    particleCanvas.style.top = "0";
    particleCanvas.style.left = "0";

    // Make sure it's on top of other elements
    particleCanvas.style.zIndex = "1001";

    // Make sure other elements under it are clickable
    particleCanvas.style.pointerEvents = "none";

    // Add our canvas to the page
    document.body.appendChild(particleCanvas);
}



function update() {
    // Clear out the old particles
    if (typeof particleCtx !== "undefined") {
        particleCtx.clearRect(0, 0, window.innerWidth, window.innerHeight);
    }

    // Draw all of our particles in their new location
    for (let i = 0; i < particles.length; i++) {
        particles[i].draw(particleCtx);

        // Simple way to clean up if the last particle is done animating
        if (i === particles.length - 1) {
            let percent = (Date.now() - particles[i].startTime) / particles[i].animationDuration;

            if (percent > 1) {
                particles = [];
            }
        }
    }

    // Animate performantly
    window.requestAnimationFrame(update);
}
window.requestAnimationFrame(update);