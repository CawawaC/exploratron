var aventurier = Aventurier.getLaSession();

$(document).ready(function() {
    getTextes("room", parseTextes);


    $("#vestiaire-chambre").change(function(evt) {
        updateBoutonsVestiaire();
    });
    $("#equipement-chambre").change(function() {
        updateBoutonsEquipement();
    });

    $("#pouvoirs").html(afficherPouvoirsEtStats(aventurier.getLesStatsTotales(), aventurier.pouvoirs));
});

function parseTextes() {
    //$("#title")[0].innerHTML = textes.title;
    $("#consigne_penderie").html(textes.order_01);
}


inscrireEquipement();
inscrireVestiaire();


$(document).on('click', '#stocker-vestiaire', stockerVestiaire);
$(document).on('click', '#retirer-vestiaire', retirerVestiaire);
// $(document).on('click', '#suppr', detruire);

function clicSupprimer(evt) {
    $($(this).parent()[0].control).addClass('supprimer')
    supprimerVestiaire();
}

function updateBoutonsVestiaire() {
    if ($("input[name='vestiaire']:checked").length > 0) {
        $("#retirer-vestiaire").attr("disabled", false);
        $("#stocker-vestiaire").attr("disabled", true);
        $("#suppr").attr("disabled", false);
    } else {
        $("#retirer-vestiaire").attr("disabled", true);
        $("#suppr").attr("disabled", true);
    }
    $('#equipement-chambre').find(":radio:checked").prop('checked', false);
}

function updateBoutonsEquipement() {
    if ($("input[name='equipement']:checked").length > 0) {
        $("#stocker-vestiaire").attr("disabled", false);
        $("#retirer-vestiaire").attr("disabled", true);
    } else
        $("#stocker-vestiaire").attr("disabled", true);

    $('#vestiaire-chambre').find(":radio:checked").prop('checked', false);
}


function inscrireEquipement() {
    //Nettoyage
    $("#equipement-chambre").empty();
    for (var e of Objet.Categories) {
        var id = e.replace(/[^\w\s]/gi, "-");
        id = id.replace(/\s/g, '-');
        $('<form id="' + id + '"><input type="radio"></input> <label>' + e + ' (vide)</label></form>').appendTo($("#equipement-chambre"));
    }


    var i = 0;

    for (var e of aventurier.equipement) {
        var id = e.categorie.replace(/[^\w\s]/gi, "-");
        id = id.replace(/\s/g, '-');
        $("#" + id).empty();
        inscrireObjet(e.nom, e.id, i, textePopUpStats(e), "equipement", "e", $("#" + id))

        i++;
    }

    updateBoutonsEquipement();
}

function inscrireVestiaire() {
    //Nettoyage
    $("#vestiaire-chambre").empty();

    var i = 0;
    for (var e of aventurier.vestiaire) {
        inscrireObjet(e.nom, e.id, i, textePopUpStats(e), "vestiaire", "v", $("#vestiaire-chambre"))
        i++;
    }

    updateBoutonsVestiaire();
}

function textePopUpStats(e) {
    var textePopUp;
    var flecheTemps, flecheZir, flecheSac, texteTemps, texteZir, texteSac, textesPouvoirs;
    let flecheHaut = "&#8599;",
        flecheBas = "&#8600;",
        flecheDroite = "&#8594;";

    // Flèches indiquent si la stat est positives ou négative
    /*
        if (e.stats.moral == undefined || e.stats.moral == 0) {
            flecheTemps = "&#8594;";
            texteTemps = 0;
        } else if (e.stats.moral > 0) {
            flecheTemps = "&#8599;";
            texteTemps = e.stats.moral.toString();
        } else if (e.stats.moral < 0) {
            flecheTemps = "&#8600;";
            texteTemps = e.stats.moral.toString();
        }
    */

    let objetCompare = aventurier.equipement.find(x => x.categorie == e.categorie)

    function compare(stat, compareStat, arrow) {
        if(stat == null) stat = 0
        if(compareStat == null) compareStat = 0
        if (stat == compareStat) {
            return flecheDroite;
        } else if (stat > compareStat) {
            return flecheHaut;
        } else if (stat < compareStat) {
            return flecheBas;
        }
    }

    if(objetCompare == null) {
        flecheTemps = compare(e.stats.moral, 0);
        texteTemps = e.stats.moral.toString();

        flecheZir = compare(e.stats.volonte, 0);
        texteZir = e.stats.volonte.toString();

        flecheSac = compare(e.pouvoirs.tailleDuSac, 0);
        texteSac = e.pouvoirs.tailleDuSac ? e.pouvoirs.tailleDuSac.toString() : "0";
    } else {
        // v2 : flèche indique si stat est > ou < aux stats de l'objet de même type équipé par l'aventurier
        flecheTemps = compare(e.stats.moral, objetCompare.stats.moral);
        texteTemps = e.stats.moral.toString();

        flecheZir = compare(e.stats.volonte, objetCompare.stats.volonte);
        texteZir = e.stats.volonte.toString();

        flecheSac = compare(e.pouvoirs.tailleDuSac, objetCompare.pouvoirs.tailleDuSac);
        texteSac = e.pouvoirs.tailleDuSac ? e.pouvoirs.tailleDuSac.toString() : "0";    
    }


    textesPouvoirs = String(e.pouvoirs.toString());
    let lignesPouvoirs = textesPouvoirs.split(",");
    /*for(let i in lignesPouvoirs) {
        let fleche = compare(e.pouvoirs.tailleDuSac, objetCompare.pouvoirs.tailleDuSac);
        lignesPouvoirs[i] = lignesPouvoirs[i].replace(":", flecheHaut)
        console.log(lignesPouvoirs[i])
    }*/

let descr = e.description;
if (descr.length>1){
  descr+="<br><br>" ;
}
    textesPouvoirs = lignesPouvoirs.join("<br>");
    textePopUp =
    descr +
        'Temps ' + flecheTemps + texteTemps +
        '<br>Zir ' + flecheZir + texteZir +
        '<br>Taille du sac' + flecheSac + texteSac +
        '<br><br>' + textesPouvoirs;


    return textePopUp;
}

function inscrireObjet(label, id, i, title, destinationName, forPrefix, destination) {
    let btnSuppr;
    btnSuppr = "";
    if (destinationName == "vestiaire") { btnSuppr = '<img src="medias/images/picto-poubelle.png" alt="supprimer" class="pictoPoubelle">'; }
    $('<input type="radio" class="objet" name="' + destinationName + '" data-id="' + id + '" id="' + forPrefix + '' + i + '"></input> <label for="' + forPrefix + i + '" >' + label + btnSuppr + '</label>').appendTo(destination);

    $(".pictoPoubelle").on("click", clicSupprimer);

    new Tooltip(destination.children().last(), {
        placement: 'right',
        html: true,
        title: title,
        boundariesElement: "conteneur-flex",
    });
}

function stockerVestiaire() {
    let value;
    $("input[name='equipement']:checked").each(function() {
        if (!$(this).prop('checked'))
            return true;

        let value = $(this).data("id");
        let iObjet = aventurier.equipement.findIndex(x => x.id == value);

        let objet;
        objet = aventurier.equipement.splice(iObjet, 1)[0];
        aventurier.vestiaire.push(objet);
    });

    inscrireEquipement();
    inscrireVestiaire();

    $("#stocker-vestiaire").attr("disabled", true);
    $("#retirer-vestiaire").attr("disabled", true);

    aventurier.sauvegarde();

    $("#pouvoirs").html(afficherPouvoirsEtStats(aventurier.getLesStatsTotales(), aventurier.pouvoirs));
}

function retirerVestiaire() {
    if (!aventurier.equipementCheck(null)) {
        // $("body").append("<p>Tu as trop d'équipement pour pouvoir en rajouter</p>");
        return;
    }
    let value = $("input[name='vestiaire']:checked").data("id");

    var objet = aventurier.vestiaire.find(x => x.id == value);


    if (!aventurier.equipementCheck(objet.categorie)) {
        /*  $("body").append("<p>Tu as déjà équipé un objet de ce type, tu ne peux pas en équiper un autre</p>");
          return;*/
        var objetPorte = aventurier.equipement.filter(x => x.id == value);
        var iObjetPorte = aventurier.equipement.indexOf(objetPorte);

        objetPorte = aventurier.equipement.splice(iObjetPorte, 1)[0];
        aventurier.vestiaire.push(objetPorte);
    }

    var iObjet = aventurier.vestiaire.indexOf(objet);

    objet = aventurier.vestiaire.splice(iObjet, 1)[0];
    aventurier.equipement.push(objet);

    inscrireEquipement();
    inscrireVestiaire();

    $("#stocker-vestiaire").attr("disabled", true);
    $("#retirer-vestiaire").attr("disabled", true);

    aventurier.sauvegarde();

    $("#pouvoirs").html(afficherPouvoirsEtStats(aventurier.getLesStatsTotales(), aventurier.pouvoirs));
}

function supprimerVestiaire() {

    $('input:radio.objet').each(function() {

        if (!$(this).prop('checked') || !$(this).hasClass('supprimer')) {
            return true;
        }

        id = $(this).data("id");

        let objet = aventurier.vestiaire.filter(x => x.id == id)[0];
        let iObjet = aventurier.vestiaire.indexOf(objet);
        aventurier.vestiaire.splice(iObjet, 1)[0];

        inscrireVestiaire();

        $("#stocker-vestiaire").attr("disabled", true);
        $("#retirer-vestiaire").attr("disabled", true);

        aventurier.sauvegarde();
    });
}

/*function detruire() {
    let value = $("input[name='vestiaire']:checked").data("id");

    var iObjet = aventurier.vestiaire.findIndex(x => x.id == value);
    aventurier.vestiaire.splice(iObjet, 1)
    aventurier.sauvegarde();
    inscrireEquipement();
    inscrireVestiaire();
}*/