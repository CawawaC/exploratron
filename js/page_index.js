var textes;
let faute = 0;

// valider formulaire en appuyant sur 'entrée'
$(document).ready(function() {

    $('#aventurier').keypress(function(event) {
        if (event.keyCode == 13 || event.which == 13) {
            event.preventDefault();
            $('#mdp')[0].focus();
            console.log("entrée aventurier")
        }
    });

    $('#mdp').keypress(function(event) {
        if (event.keyCode == 13 || event.which == 13) {
            event.preventDefault();
            connexion();
        }
    });

    getTextes("login", parseTextes);
});

function connexion() {
    let nom = $('#aventurier')[0].value;
    let mdp = $('#mdp')[0].value;

    console.log(textes.wrong_ids);


    $.ajax({
        url: 'settersBDD/joueurs_connexion.php',
        type: 'GET',
        data: {
            nom: nom,
            mdp: mdp
        },
        dataType: 'json',
        success: function(data, statut) {
            if (data.status == "failure") {
                if (faute == 0)
                    $('#login')[0].innerHTML = textes.wrong_ids;
                else
                    $('#login')[0].innerHTML = textes.wrong_ids_2;

                faute += 1;
            } else {
                $('#aventurier').remove();
                $('#mdp').remove();
                $('#sub').remove();

                /*let hub = document.createElement('a');
                hub.href =  'hub.php';
                hub.innerHTML = "C'est parti !"
                $(".colonneCentrale").append(hub);*/
                document.location.href = "hub.php";
            }
        }
    });
}

function parseTextes() {
    $("#title")[0].innerHTML = textes.title;
    $("#subtitle")[0].innerHTML = textes.subtitle[random(textes.subtitle.length)];
    $("#intro")[0].innerHTML = textes.intro;
    if ($("#new_user")[0]) $("#new_user")[0].innerHTML = textes.new_user;
    if ($("#button_new_user")[0]) $("#button_new_user")[0].innerHTML = textes.button_new_user;
    $("#login")[0].innerHTML = textes.login;
    $("#aventurier").attr("placeholder", textes.champ_aventurier);
    $("#mdp").attr("placeholder", textes.champ_mdp);
    $("#sub").attr("value", textes.validation);
}