var ici;
var image = new Image();

image.src = "medias/images/motif-carte-01.png";

class Carte {
    constructor(_filtre) {
        this.regionSelectionnee = -1;
        this.ingredientsExistants = [];
        this.zoneInitiale = {
            width: 400,
            height: 200
        };

        this.canvas = document.getElementById('voronoiCanvas');
        this.isDragging = false;
        this.canMouseX = 0;
        this.canMouseY = 0;
        this.offsetX = 0;
        this.offsetY = 0;
        this.scale = 1;

        this.container = document.getElementById("container");

        this.filtreRegions = _filtre //Carte.Filtres.Saturation;


        this.regneSelectionne = -1;
    }

    init() {
        this.voronoi = Carte.getLaCarte();
        if (this.voronoi == null || this.voronoi.sites == null || this.voronoi.sites.length == 0)
            this.voronoi = Carte.nouvelleCarteAvecZoneCentrale();

        this.render();

        this.getLesIngredients();
    }

    render() {
        this.voronoi.render(this);

        this.dessinerOutlineAccessibilite();

        // this.colorisation(this.voronoi);
    }

    /*
        Verifie si toutes les régions visibles sont saturées.
    */
    saturationVisible(type) {
        let voronoiCarte = this.voronoi;
        for (var site of voronoiCarte.sites.filter(function(c) { return c.visible; })) {
            var endemiqueType = this.ingredientEndemiqueTypeDeLaRegion(site.voronoiId, type);
            if (endemiqueType.length == 0) return false;
        }
        return true;
    }

    colorisation(voronoiCarte) {
        //Coloriser les sites selon le filtre actif
        var ctx = voronoiCarte.canvas.getContext('2d');
        ctx.scale(this.scale, this.scale);

        switch (this.filtreRegions) {
            case Carte.Filtres.Aucun:
                for (var cell of voronoiCarte.diagram.cells) {
                    if(cell.site.visible)
                        this.dessineRegion(cell, voronoiCarte, 0);
                }
                break;

            case Carte.Filtres.Saturation:
                for (var cell of voronoiCarte.diagram.cells) {
                    var endemiqueType = this.ingredientEndemiqueTypeDeLaRegion(cell.site.voronoiId, this.regneSelectionne);

                    if (cell && cell.site.visible && this.regneSelectionne == -1)
                        this.dessineRegion(cell, voronoiCarte, 1);
                    else if (cell && cell.site.visible &&
                        endemiqueType.length > 0
                    ) {
                        this.dessineRegion(cell, voronoiCarte, 1)
                    } else if (cell && cell.site.visible) {
                        this.dessineRegion(cell, voronoiCarte, 0);
                    }
                }
                break;

            case Carte.Filtres.CacheVide:
                for (var cell of voronoiCarte.diagram.cells) {
                    var endemiqueType = this.ingredientEndemiqueTypeDeLaRegion(cell.site.voronoiId, this.regneSelectionne);

                    if (cell && cell.site.visible &&
                        endemiqueType.length > 0) {
                        this.dessineRegion(cell, voronoiCarte, 0)
                    } else if (cell && cell.site.visible) {
                        this.dessineRegion(cell, voronoiCarte, 2);
                    }
                }
                break;
        }

        ctx.setTransform(1, 0, 0, 1, 0, 0);
    }

    dessineRegionInterdite(cell, ctx) {
        ctx.fillStyle = '#3a3a3a40'; //régions vides
        ctx.fill();
        ctx.strokeStyle = '#fff';
        ctx.stroke();
        ctx.rect(cell.site.x - 2 / 3, cell.site.y - 2 / 3, 2, 2);
        ctx.fillStyle = '#ffffff40'; // couleur texte
        ctx.font = "700 1rem Raleway";
        //ctx.fillText("Ø", cell.site.x - 2 / 3, cell.site.y - 2 / 3);
    }

    dessineRegionAccessible(cell, ctx) {
        ctx.fillStyle = '#3a3a3a'; //régions pleines
        ctx.fill();
        ctx.strokeStyle = '#fff';
        ctx.stroke();
    }

    dessineRegionTransparente(cell, ctx) {
        ctx.fillStyle = '#3a3a3a00'; //régions vides
        ctx.fill();
        ctx.strokeStyle = '#ffffff00';
        ctx.stroke();
        ctx.rect(cell.site.x - 2 / 3, cell.site.y - 2 / 3, 2, 2);
        ctx.fillStyle = '#ffffff40'; // couleur texte
        ctx.font = "700 1rem Raleway";
        // ctx.fillText("Ø", cell.site.x - 2 / 3, cell.site.y - 2 / 3);
    }

    dessineRegionSelectionnee(cell, ctx) {
        ctx.fillStyle = '#222222'; // région sélectionnée
        ctx.fillStyle = ctx.createPattern(image, "repeat");
        ctx.fill();
        ctx.strokeStyle = '#ffffff';
        ctx.stroke();

        // C'est quoi ??
        ctx.rect(cell.site.x - 2 / 3, cell.site.y - 2 / 3, 2, 2);

        // ctx.fillStyle = '#a8a8a8'; // couleur texte
        // ctx.font = "700  1rem Raleway";
        // ctx.fillText(ici, cell.site.x - 2 / 3, cell.site.y - 2 / 3);
    }

    dessineRegion(cell, voronoiCarte, type) {
        var ctx = voronoiCarte.canvas.getContext('2d');
        var halfedges = cell.halfedges,
            nHalfedges = halfedges.length;


        //Rendre la carte "carrée" comme celle d'un appartement
        halfedges = [];
        for (const he of cell.halfedges) {
            if (he.angle == 0) {
                halfedges.push(he)
                continue;
            }

            var va = he.getStartpoint();
            var end = he.getEndpoint();
            let newEdge, newHalf;
            let edge = he.edge;
            let middlePoint;

            //Edge horizontale ou verticale (selon l'angle) à partir de la edge existante
            newEdge = new voronoiCarte.voronoi.Edge(edge.lSite, edge.rSite);
            newEdge.va = va
            if (he.angle > 0)
                newEdge.vb = new voronoiCarte.voronoi.Vertex(va.x, end.y)
            else
                newEdge.vb = new voronoiCarte.voronoi.Vertex(end.x, va.y)

            newHalf = new voronoiCarte.voronoi.Halfedge(newEdge, he.site);
            newHalf.angle = 0;
            halfedges.push(newHalf);
            middlePoint = newEdge.vb;

            //Nouvelle edge pour la portion restante
            newEdge = new voronoiCarte.voronoi.Edge(edge.lSite, edge.rSite);
            newEdge.va = middlePoint;
            newEdge.vb = end;
            newHalf = new voronoiCarte.voronoi.Halfedge(newEdge, he.site);
            newHalf.angle = 0
            halfedges.push(newHalf);
        }

        cell.halfedges = halfedges;

        nHalfedges = halfedges.length;

        if (nHalfedges > 2) {

            var v = halfedges[0].getStartpoint();
            ctx.beginPath();
            ctx.moveTo(v.x, v.y);

            for (var iHalfedge = 0; iHalfedge < nHalfedges; iHalfedge++) {
                let end = halfedges[iHalfedge].edge.vb

                /*//Pour une carte carrée
                if (halfedges[iHalfedge].angle > 0)
                    ctx.lineTo(v.x, end.y);
                else
                    ctx.lineTo(end.x, v.y);
*/
                ctx.lineTo(end.x, end.y);
                v = end;
            }

            if (type == 0)
                this.dessineRegionAccessible(cell, ctx);
            else if (type == 1)
                this.dessineRegionInterdite(cell, ctx);
            else if (type == 2)
                this.dessineRegionTransparente(cell, ctx)
            else if (type == 3)
                this.dessineRegionSelectionnee(cell, ctx)
        }
    }

    static getLaCarte() {
        var response = $.ajax({
            data: "",
            type: "GET",
            url: "gettersBDD/getLaCarte.php",
            async: false
        });

        var data = response.responseText;
        var dataCarte = JSON.parse(data);
        var carteBDD = Carte.creerCarte(dataCarte, this.filtreRegions);

        return carteBDD;
    }

    getLesIngredients() {
        var regionsVisibles = this.voronoi.sites.filter(x => x.visible);
        let idRegionsVisibles = regionsVisibles.map(x => x.voronoiId);
        this.ingredientsExistants = Ingredient.getIngredientsByRegions(idRegionsVisibles);
    }

    getRegionsAdjacentes(regionId) {
        //Récupérer toutes les edges possédant uniquement 1 site visible adjacent
        console.log(this.voronoi);
        var edgesCandidates = this.voronoi.diagram.edges.filter(function(edge) {
            return ((edge.rSite != null && edge.rSite.voronoiId == regionId) || (edge.lSite != null && edge.lSite.voronoiId == regionId));
        });

        //Récupérer les sites non-visibles correspondants
        var regionsCandidates = edgesCandidates.map(function(edge) {
            if (edge.lSite.voronoiId == regionId) return edge.rSite;
            else return edge.lSite;
        })

        regionsCandidates = regionsCandidates.filter(function(region) {
            return region.visible;
        });

        regionsCandidates = regionsCandidates.map(function(region) {
            return region.voronoiId;
        });

        return regionsCandidates;
    }


    static nouvelleCarteAvecZoneCentrale() {
        var carte = new Carte(Carte.Filtres.Aucun);
        carte.voronoi = Carte.creerCarte(null, this.filtreRegions);
        console.log(carte)


        carte.revelerZoneCentrale(true)

        return carte;
    }

    masquerToutesRegions() {
        for (var site of this.voronoi.sites) {
            site.visible = false;
        }

        this.render()
    }

    revelerToutesRegions() {
        console.log("revelerToutesRegions")
        for (var site of this.voronoi.sites) {
            site.visible = true;
        }

        this.render()
    }


    revelerZoneCentrale(masquerLeReste = false) {
        let canvas = document.getElementById('voronoiCanvas');
        let zoneInitiale = {
            width: 400,
            height: 200
        };
        this.revelerZone(
            canvas.width / 2 - zoneInitiale.width / 2,
            canvas.height / 2 - zoneInitiale.height / 2,
            zoneInitiale.width,
            zoneInitiale.height,
            true
        );
    }

    revelerZone(x, y, width, height, masquerLeReste = false) {
        for (var site of this.voronoi.sites) {
            if (site.x > x && site.x < x + width && site.y > y && site.y < y + height) {
                site.visible = true;
            } else if(masquerLeReste) {
                site.visible = false;
            }
        }
        this.voronoi.render(this);
    }

    ingredientEndemiqueTypeDeLaRegion(idRegion, regne) {
        var endemiques = this.ingredientsExistants.filter(x => x.region == idRegion);
        if (regne == -1 || endemiques.length == 0)
            return endemiques;

        var endemiqueType = endemiques.filter(x => x.iRegne == regne);

        return endemiqueType;
    }

    regionEstVide(idRegion) {


        var endemiques = this.ingredientsExistants.filter(x => x.region == idRegion);
        if (endemiques.length == 0) {
            if (this.filtreRegions == 1)
                return false;
            if (this.filtreRegions == 2)
                return true;
        } else {
            if (this.filtreRegions == 1)
                return true;
            if (this.filtreRegions == 2)
                return false;
        }
    }

    sauvegarde() {
        $.ajax({
            url: 'settersBDD/setLaCarte.php',
            type: 'POST',
            data: {
                sites: JSON.stringify(this.voronoi.sites)
            },
            dataType: 'html', // On désire recevoir du HTML
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
                $("body").append(code_html);
            },
            complete: function() {}
        });
    }



    initInterface() {
        var me = this;

        var context = this.canvas.getContext("2d");
        context.textAlign = 'center';


        // var boutonCentrer = document.getElementById("centrer");
        $('#centrer').on("click", this.centrer);
        // $('#zoom').on("click", this.zoom);



        // var boutonReveler = document.getElementById("reveler");
        // if(boutonReveler != null) boutonReveler.onclick = this.revelerRegion;





        //Zoom
        /*
        this.canvas.addEventListener('wheel', function(event){
            me.scale += event.deltaY / 100;
            var ctx = this.getContext('2d');        
            ctx.scale(me.scale, me.scale);  
            console.log(me.scale);
            me.render();
            return false; 
        }, false);*/

        this.centrer();


        var canvas = $('#voronoiCanvas');
        canvas.click(function(e) { me.handleCanvasClick(e); });
        canvas.mousedown(function(e) { me.handleMouseDown(e); });
        canvas.mousemove(function(e) { me.handleMouseMove(e); });
        canvas.mouseup(function(e) { me.handleMouseUp(e); });
        canvas.mouseout(function(e) { me.handleMouseOut(e); });

        me.render();
    }

    handleCanvasClick(e) {
        var mouse = this.voronoi.normalizeEventCoords(this.voronoi.canvas, e);
        // this.addSite(mouse.x,mouse.y);

        var cell = this.voronoi.trouverCell(mouse.x, mouse.y);
        console.log(cell.site.voronoiId)

        if (this.filtreRegions == Carte.Filtres.CacheVide) {
            if (cell.site.visible && !this.regionEstVide(cell.site.voronoiId)) {
                this.regionSelectionnee = cell.site.voronoiId;
                this.render();
                this.voronoi.selectionnerSite(cell, this);
            }
        } else if (this.filtreRegions == Carte.Filtres.Saturation) {
            if (cell.site.visible) {
                if (this.ingredientEndemiqueTypeDeLaRegion(cell.site.voronoiId, this.regneSelectionne).length == 0) {
                    this.regionSelectionnee = cell.site.voronoiId;
                    this.render();
                    this.voronoi.selectionnerSite(cell, this);
                }
            }
        } else {
            this.regionSelectionnee = cell.site.voronoiId;
            this.render();
            this.voronoi.selectionnerSite(cell, this);
        }
    }

    handleMouseDown(e) {
        this.startX = parseInt(e.clientX - this.offsetX);
        this.startY = parseInt(e.clientY - this.offsetY);
        // set the drag flag
        this.isDragging = true;

        document.body.style.cursor = 'move';
    }

    handleMouseUp(e) {
        this.startX = parseInt(e.clientX - this.offsetX);
        this.startY = parseInt(e.clientY - this.offsetY);
        // clear the drag flag
        this.isDragging = false;

        document.body.style.cursor = 'default';
    }

    handleMouseOut(e) {
        this.handleMouseUp(e);

        document.body.style.cursor = 'default';
    }

    handleMouseMove(e) {
        if (this.isDragging) {
            this.canMouseX = parseInt(e.clientX - this.offsetX);
            this.canMouseY = parseInt(e.clientY - this.offsetY);

            var dx = this.canMouseX - this.startX;
            var dy = this.canMouseY - this.startY;

            this.container.scrollLeft -= dx;
            this.container.scrollTop -= dy;

            this.startX = this.canMouseX;
            this.startY = this.canMouseY;
        }
    }


    centrer() {
        this.canvas = document.getElementById('voronoiCanvas');
        this.container = document.getElementById("container");
        this.container.scrollLeft = this.canvas.width / 2 - 400;
        this.container.scrollTop = this.canvas.height / 2 - 150;
    }

    static creerCarte(sitesFournis, filtreRegions) {
        var VoronoiDemo = {
            voronoi: new Voronoi(),
            sites: [],
            diagram: null,
            margin: 100,
            canvas: null,
            bbox: { xl: 0, xr: 8000, yt: 0, yb: 3000 },

            normalizeEventCoords: function(target, e) {
                // http://www.quirksmode.org/js/events_properties.html#position
                // =====
                if (!e) { e = self.event; }
                var x = 0;
                var y = 0;
                
                /*if (e.pageX || e.pageY) {
                    console.log("èè");
                    x = e.pageX;
                    y = e.pageY;
                }
                else*/

                if (e.clientX || e.clientY) {
                    let bb = this.canvas.getBoundingClientRect();

                    x = e.clientX - bb.x;
                    y = e.clientY - bb.y;
                }

                // return { x: x - target.offsetLeft, y: y - target.offsetTop };
                return { x: x, y: y };
            },

            init: function(sitesFournis) {
                var me = this;
                this.canvas = document.getElementById('voronoiCanvas');
                // this.canvas.onmousemove = function(e) {
                //  if (!me.sites.length) {return;}
                //  var site = me.sites[0];
                //  var mouse = me.normalizeEventCoords(me.canvas,e);
                //  site.x = mouse.x;
                //  site.y = mouse.y;
                //  me.diagram = me.voronoi.compute(me.sites,me.bbox);
                //  me.render();
                // };


                // console.log(sitesFournis);   
                if (sitesFournis == null || sitesFournis.length == 0)
                    this.randomSites(3000, true);
                else {
                    this.sites = sitesFournis;
                    this.diagram = this.voronoi.compute(this.sites, this.bbox);
                }
                this.render();
            },

            clearSites: function() {
                // we want at least one site, the one tracking the mouse
                this.sites = [{ x: 0, y: 0 }];
                this.diagram = this.voronoi.compute(this.sites, this.bbox);
            },

            randomSites: function(n, clear) {
                console.log("Carte aléatoire");
                if (clear) { this.sites = []; }
                var xo = this.margin;
                var dx = this.canvas.width - this.margin * 2;
                var yo = this.margin;
                var dy = this.canvas.height - this.margin * 2;
                for (var i = 0; i < n; i++) {
                    this.sites.push({
                        x: self.Math.round(xo + self.Math.random() * dx),
                        y: self.Math.round(yo + self.Math.random() * dy),
                        visible: false
                    });
                }
                this.diagram = this.voronoi.compute(this.sites, this.bbox);
            },

            addSite: function(x, y) {
                this.sites.push({ x: x, y: y });
                this.diagram = this.voronoi.compute(this.sites, this.bbox);
            },

            trouverCell: function(x, y) {

                //Trouver la halfedge telle que distance entre (x, y) et halfedge.va est minimale, et distance entre (x, y) et halfedge.vb et minimale
                /*let closestH = this.diagram.cells[0].halfedges[0];
                let closestHDistA = 9000, closestHDistB = 9000;
                for(let cell of this.diagram.cells) {
                    for(let halfedge of cell.halfedges) {
                        let distA = Math.sqrt((x - halfedge.edge.va.x)**2 + (y - halfedge.edge.va.y)**2)
                        let distB = Math.sqrt((x - halfedge.edge.vb.x)**2 + (y - halfedge.edge.vb.y)**2)
                            // console.log(distA, closestHDistA)
                        if(distA < closestHDistA && distB < closestHDistB) {
                            closestHDistA = distA;
                            closestHDistB = distB;
                            closestH = halfedge;
                        }
                    }
                }
                let cellReturn;
                let candidats = this.diagram.cells.filter(x => x.site.voronoiId == closestH.edge.rSite.voronoiId || x.site.voronoiId == closestH.edge.lSite.voronoiId)
                console.log(candidats)
                if(candidats.length == 1)
                    return candidats[0] 
                else if(candidats.length > 1) {
                    if(closestH.angle > 0)
                        return candidats.find(x => x.site.voronoiId = closestH.edge.rSite.voronoiId)
                    else
                        return candidats.find(x => x.site.voronoiId = closestH.edge.rSite.voronoiId)
                }*/

                /*
                                //Avec formule de distance d'un point à une droite (--> 2 candidats)
                                // Pas bon algo, il est faux
                                let minDist = -1, closestEdge;
                                console .log(this.diagram)
                                for(let edge of this.diagram.edges) {
                                    let x1 = edge.va.x,
                                        y1 = edge.va.y,
                                        x2 = edge.vb.x,
                                        y2 = edge.vb.y;

                                    let distance = Math.abs((y2-y1)*x - (x2-x1)*y + x2*y1 - y2*x1)/Math.sqrt(Math.pow(y2-y1, 2) + Math.pow(x2-x1, 2));
                                    if(minDist < 0) 
                                        minDist = distance;
                                    else if(distance < minDist) {
                                        console.log(distance)
                                        minDist = distance
                                        closestEdge = edge
                                    }
                                }
                                console.log(closestEdge)

                */

                //Algo avec raycast
                function pnpoly(nvert, vertx, verty, testx, testy) {
                    let i, j, c;
                    c = false;
                    for (i = 0, j = nvert - 1; i < nvert; j = i++) {
                        if (((verty[i] > testy) != (verty[j] > testy)) &&
                            (testx < (vertx[j] - vertx[i]) * (testy - verty[i]) / (verty[j] - verty[i]) + vertx[i]))
                            c = !c;
                    }
                    return c;
                }

                for (let cell of this.diagram.cells) {
                    let crossings = pnpoly(
                        cell.halfedges.length,
                        cell.halfedges.map(x => x.edge.va.x),
                        cell.halfedges.map(x => x.edge.va.y),
                        x,
                        y)
                    if (crossings == true) { // Ca veut dire qu'on a traversé un nombre impair de côté du polygone ==> on est dedans
                        return cell;
                    }
                }



                var cell;
                for (let i = 0; i < this.diagram.cells.length; i++) {
                    if (this.diagram.cells[i].pointIntersection(x, y) === 1) {
                        console.log(this.diagram.cells[i])
                        return this.diagram.cells[i];
                    }
                }
                return null;
            },

            selectionnerSite: function(cell, carte) { // return false ==> region desactivee
                var ctx = this.canvas.getContext('2d');
                if (cell) {
                    carte.mettreAJourLien(cell.site.voronoiId, carte.getMagnitude(cell.site.voronoiId));

                    carte.dessineRegion(cell, this, 3)
                }
            },

            render: function(carte) {
                if (this.canvas == null) return;
                var ctx = this.canvas.getContext('2d');
                //if(carte != null) ctx.scale(carte.scale, carte.scale);

                ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

                // background
                ctx.globalAlpha = 1;
                ctx.beginPath();

                ctx.rect(0, 0, this.canvas.width, this.canvas.height); // arrière plan
                ctx.fillStyle = '#ffffff80';
                ctx.fill();
                ctx.strokeStyle = '#fff';
                ctx.stroke();

                // voronoi
                if (!this.diagram) { return; }

                // how many sites do we have?
                var sites = this.sites,
                    nSites = sites.length;
                if (!nSites) { return; }



                if (carte != null) carte.colorisation(this);


            }
        };


        VoronoiDemo.init(sitesFournis);
        // console.log(VoronoiDemo.sites);
        return VoronoiDemo;
    }

    revelerRegion() {
        //Mélanger le tableau de sites
        //Prendre le premier site qui a un gauche visible OU (exclusif) un droit visible.
        //Le rendre visible
        //Récupérer toutes les edges possédant uniquement 1 site visible adjacent
        var edgesCandidates = this.voronoi.diagram.edges.filter(function(edge) {
            return (edge.rSite != null && (!edge.lSite.visible ? edge.rSite.visible : !edge.rSite.visible));
        });

        //Récupérer les sites non-visibles correspondants
        var regionsCandidates = edgesCandidates.map(function(edge) {
            if (!edge.lSite.visible) return edge.lSite;
            else if (edge.rSite != null && !edge.rSite.visible) return edge.rSite;
        });

        var counts = {};

        //On repère les régions les plus récurrentes dans la liste : ce sont celles qui ont le plus de régions adjacentes visibles
        //Count est un dictionaire de forme <voronoiID, nombreDeRegionsAdjacentes>
        for (var i = 0; i < regionsCandidates.length; i++) {
            var num = regionsCandidates[i].voronoiId;
            counts[num] = counts[num] ? counts[num] + 1 : 1;
        }

        //items est un dictionnaire copié temporaire servant à trier la liste par ordre décroissant
        var items = Object.keys(counts).map(function(key) {
            return [key, counts[key]];
        });

        items.sort(function(first, second) {
            return second[1] - first[1];
        });

        // console.log(items.slice(0, 5));

        var nouvelleRegion = regionsCandidates.find(function(r) { return parseInt(r.voronoiId) === parseInt(items[0][0]); });
        console.log(nouvelleRegion)
        nouvelleRegion.visible = true;


        this.render();
    }


    mettreAJourLien(id, distance) {
        /*  $(document).ready(function() {
                    $("a#exploration").attr("href", 'exploration.php?voronoiId='+id);
        });*/

        $.ajax({
            url: 'modifierSession.php',
            type: 'GET',
            async: false,
            data: {
                idVoronoiActuel: id,
                distanceVoronoiActuel: distance
            },
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
                /* $( "body" ).append( code_html ); */
            },
        });

        //activer bouton vers exploration
        //Si au moins 1 ingrédient présent dans région sélectionnée
        if (this.ingredientsExistants.find(x => x.region == id) == null)
            $('#exploration').addClass("desactive");
        else {
            $('#exploration').removeClass("desactive");
            $('#exploration').val("Explorer la suite n° " + id);
        }

    }

    dessinerOutlineAccessibilite() {

    }

    getMagnitude(voronoiId) {
        var site = this.getSite(voronoiId);
        var origine = this.getOrigine();
        var magnitude = Math.sqrt(Math.pow(site.x - origine.x, 2) + Math.pow(site.y - origine.y, 2));
        return magnitude;
    }

    getSite(voronoiId) {
        var site = this.voronoi.sites.filter(function(site) {
            return site.voronoiId == voronoiId;
        })[0];
        return site;
    }

    getOrigine() {
        return this.voronoi.trouverCell(
            this.canvas.width / 2,
            this.canvas.height / 2
        ).site;
    }
}


$(document).ready(function() {
    getTextes("map", parseTextesCarte);
});

function parseTextesCarte() {
    $('#centrer').attr("value", textes.center);
    ici = textes.here;
}



/*var dropdownFiltre = $("#filtre");
dropdownFiltre.onchange = function() {
    console.log(dropdown);
    var myindex  = dropdown.selectedIndex
    filtreRegions = Filtres[myindex][0];
    console.log(filtreRegions);
    carte.render();
}

console.log(dropdownFiltre);
*/

Carte.Filtres = {
    Aucun: 0,
    Saturation: 1,
    CacheVide: 2
};