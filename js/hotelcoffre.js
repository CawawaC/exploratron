class Hotelcoffre {

	static getHotelcoffre() {
		var response = $.ajax({
			data : "", 
			type : "GET", 
			url  : "gettersBDD/getHotelcoffre.php",
			async: false
		});
		
		var data = response.responseText;
		var dataStock = JSON.parse(data);

		dataStock.forEach(x => x.id = parseInt(x.id));

		return dataStock;
	}


	/*	
		argument : tableau d'intgrédients
	*/
	static stockerIngredients(ingredients) {
		if(ingredients.length==0)
			return;
		$.ajax({
			url : 'settersBDD/stockerIngredients.php',
			type : 'POST',
			data: {
				ids: ingredients.map(x => x.id)
			},
	        dataType : 'html', // On désire recevoir du HTML
	        success : function(code_html, statut){ // code_html contient le HTML renvoyé
	        	$( "body" ).append( code_html );                                 
	        },
	        complete: function() {
	        	ingredients = [];
	        }
	    });
	}

	static retirer(id, nombre) {
		let ids = [];
		for(let i = 0 ; i < nombre ; i++)
			ids.push(id);

		$.ajax({
            url : 'settersBDD/destockerIngredients.php',
            type : 'POST',
            data: {
                ids: ids
            },
            async: true,
            dataType : 'html', // On désire recevoir du HTML
            success : function(code_html, statut){ // code_html contient le HTML renvoyé
                // $( "body" ).append( code_html );                                  
            },
            complete: function() {
            }
        }); 
	}

	static ajouter(id, nombre) {
		let ids = [];
		for(let i = 0 ; i < nombre ; i++)
			ids.push(id);

		$.ajax({
            url : 'settersBDD/stockerIngredients.php',
            type : 'POST',
            data: {
                ids: ids
            },
            dataType : 'html', // On désire recevoir du HTML
            success : function(code_html, statut){ // code_html contient le HTML renvoyé
                $( "body" ).append( code_html );                                  
            },
            complete: function() {
            }
        }); 
	}

	/*
		argument : array de noms d'ingredients à destocker (1 entrée => destockage d'1 unité)
	*/
	static destockerIngredients(ingredients) {
		console.log(ingredients);
		$.ajax({
            url : 'settersBDD/destockerIngredients.php',
            type : 'POST',
            data: {
                ids: ingredients.map(x => x.id)
            },
            dataType : 'html', // On désire recevoir du HTML
            success : function(code_html, statut){ // code_html contient le HTML renvoyé
                // $( "body" ).append( code_html );                                  
            },
            complete: function() {
            }
        }); 
	}

	/*
		DEBUG : Réapprovisionne l'hotelcoffre 
	*/
	static RICHESSE() {
		$.ajax({
	        url : 'settersBDD/stockerPleinIngredients.php',
	        type : 'GET',
	        data: { 
	            
	        },
	        dataType : 'html', // On désire recevoir du HTML
	        success : function(code_html, statut) { // code_html contient le HTML renvoyé
	            $("body").append(code_html);                                  
	        },
	    });
	}

	static PAUVRETE() {
		$.ajax({
	        url : 'settersBDD/viderCoffre.php',
	        type : 'GET',
	        data: { 
	            
	        },
	        dataType : 'html', // On désire recevoir du HTML
	        success : function(code_html, statut) { // code_html contient le HTML renvoyé
	            $("body").append(code_html);                                  
	        },
	    });
	}
}