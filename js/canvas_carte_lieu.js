function Carte() {
    var c = document.getElementById("carte_lieu");
    var ctx = c.getContext("2d");
    var width = 0, height = 0, xMin=10000000, yMin=10000000;
    var planConnu=[];
    ctx.strokeStyle = "#3a3a3a";
    ctx.fillStyle = "#3a3a3a";
    ctx.lineWidth = 2;

    this.ajoutePiece = function(x, y, derniereDirection) {
        if(x*50>width) width = x*50;
        if(y*50>height) height = y*50;
        ctx.beginPath();
        ctx.arc( x * 50,  y * 50, 10, 0, 2 * Math.PI);
        ctx.fill();

        if (derniereDirection == "sud") {
            ctx.moveTo( x * 50,  y * 50 - 10);
            ctx.lineTo( x * 50,  y * 50 - 40);
            ctx.stroke();
        }

        if (derniereDirection == "est") {
            ctx.moveTo( x * 50 - 10,  y * 50);
            ctx.lineTo( x * 50 - 40,  y * 50);
            ctx.stroke();
        }

        if (derniereDirection == "ouest") {
            ctx.moveTo( x * 50 + 10,  y * 50);
            ctx.lineTo( x * 50 + 40,  y * 50);
            ctx.stroke();
        }

        if (derniereDirection == "nord") {
            ctx.moveTo( x * 50,  y * 50 + 10);
            ctx.lineTo( x * 50,  y * 50 + 40);
            ctx.stroke();
        }


        ctx.stroke();
        this.centre();
    }

    this.majPlan = function(plan){


        if(plan.x=== undefined){
            for (let i in plan) {
                for (let j in plan[i]) {
                    let salle = plan[i][j]
                    if (salle != null) {
                        var _salles  = planConnu.filter(s => s.x == i);
                        _salles =_salles.filter(s => s.y == j); 
                         if(_salles.length==0)
                                planConnu.push({x : i, y :j,accessibiliteVoisines : salle.accessibiliteVoisines });
                        if(i*50>width) width = i*50;
                         if(j*50>height) height = j*50;
                         if(i*50<xMin) xMin = i*50;
                         if(j*50<yMin) yMin = j*50;                        
                    }
                }
            }
        }
        else{
             var _salles  = planConnu.filter(s => s.x == plan.x);
            _salles =_salles.filter(s => s.y == plan.y); 
            if(_salles.length==0 || planConnu.length==0)
                 planConnu.push({x : plan.x, y :plan.y ,accessibiliteVoisines : plan.accessibiliteVoisines });

                if(plan.x*50>width) width = plan.x*50;
                 if(plan.y*50>height) height = plan.y*50;
                 if(plan.x*50<xMin) xMin = plan.x*50;
                 if(plan.y*50<yMin) yMin = plan.y*50;
        }

        this.dessineDonjon();
    }


    this.dessineDonjon = function() {
        ctx.clearRect(0, 0, c.width, c.height);
        ctx.beginPath();
        ctx.fillStyle = "#9c9c9c";
       ctx.strokeStyle = "#9c9c9c";
        ctx.translate((c.width-width)/2 - xMin/2, (c.height-height)/2 - yMin/2);
        for (index = 0; index < planConnu.length; index++) {
            s = planConnu[index];
            this.dessinePiece(s.x, s.y)
                    ctx.stroke();
                    let x = Number(s.x),
                        y = Number(s.y);
                    // salle.accessibiliteVoisines : [nord, sud, est, ouest]
                    //Nord
                    if (s.accessibiliteVoisines[0])
                        this.dessineSegment(x, y, x, y-1)
                    //Sud
                    if (s.accessibiliteVoisines[1]){
                        this.dessineSegment(x, y, x, y+1)
                    }

                    //Ouest
                    if (s.accessibiliteVoisines[3])
                        this.dessineSegment(x, y, x-1, y)
                    //Est
                    if (s.accessibiliteVoisines[2])
                        this.dessineSegment(x, y, x+1, y)
        }
         ctx.translate(-((c.width-width)/2 - xMin/2), -((c.height-height)/2 - yMin/2));
         ctx.closePath();
    }

    this.dessinePiece = function(x, y) {
        ctx.moveTo( x * 50,  y * 50);
        ctx.arc( x * 50,  y * 50, 10, 0, 2 * Math.PI);
        ctx.fill();        
    }

    this.dessineSegment = function(x1, y1, x2, y2) {
        ctx.moveTo( x1 * 50,  y1 * 50);
        ctx.lineTo( x2 * 50,  y2 * 50);
        ctx.stroke();
    }

    this.dessinePieceActive = function(x, y) {
        ctx.beginPath();
        ctx.translate((c.width-width)/2 - xMin/2, (c.height-height)/2 - yMin/2);
        ctx.moveTo( x * 50,  y * 50);
        ctx.arc( x * 50,  y * 50, 15, 0, 2 * Math.PI);
        ctx.fillStyle = "#3a3a3a";
        ctx.strokeStyle = "#3a3a3a";
        ctx.fill();
        ctx.stroke();
        ctx.translate(-((c.width-width)/2 - xMin/2), -((c.height-height)/2 - yMin/2));
        ctx.closePath();
    }

    this.centre = function(){
        ctx.translate(-width/2, -height/2);
    }
}