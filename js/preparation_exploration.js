var aventurier = Aventurier.getLaSession();

let sitesAccessibles;
var couleurContourAccessible = '#fa2b3b'

var carte = new Carte(Carte.Filtres.CacheVide);
carte.init();
carte.initInterface();

$(document).ready(function() {
    getTextes("preparation_exploration", parseTextes);
    initSitesAccessibles();
    activitesPossibles();


    $("#stats-joueur .contenu").prepend("<h2>" + aventurier.nom + "</h2>");

    $("#bouton_equipement").click(function(event) {
        event.preventDefault();
        $(".panneau-lateral#equipement").addClass('ouvert');
        $(".colonneCentrale").css("filter", "blur(2.5px)");
        $(".ombre").addClass("montree");
        $(".ombre").removeClass("cachee");
        $("#stats-joueur").removeClass("transparent");
    });

    $(".panneau-lateral .fermeture").click(function(event) {
        event.preventDefault();
        $(".panneau-lateral#equipement").removeClass("ouvert");
        $(".colonneCentrale").css("filter", "none");
        $(".ombre").removeClass("montree");
        $(".ombre").addClass("cachee");
        $("#stats-joueur").addClass("transparent");
    });

    carte.dessinerOutlineAccessibilite = dessinerOutlineAccessibilite;
    carte.dessinerOutlineAccessibilite();

    $("#exploration").prop("disabled", false);

    //$("#pouvoirs").html(afficherPouvoirsEtStats(aventurier.getLesStatsTotales(), aventurier.pouvoirs));

    $("#exploration").on("click", function() {
        let boutonExplo = $("#exploration");

        if (boutonExplo.prop("disabled"))
            return;

        let id = carte.regionSelectionnee;
        if (id < 0) {
            let div = jQuery("<div>")
            div.html(textes.no_zone).append("<br>");
            jQuery("<input>", {
                type: "button",
                id: "fermer_popup",
                value: "D'accord",
                click: fermerPopUp
            }).appendTo(div);
            ouvrirPopUp(div);
        } else if (sitesAccessibles.findIndex(x => x.voronoiId == id) < 0) {
            let div = jQuery("<div>");
            div.html(textes.forbidden_zone).append("<br>");
            jQuery("<input>", {
                type: "button",
                id: "fermer_popup",
                value: "D'accord",
                click: fermerPopUp
            }).appendTo(div);
            ouvrirPopUp(div);
        } else {
            $("#exploration").prop("title", textes.allowed_zone);

            //Déterminer les régions adjacentes
            var regionsAdjacentes = carte.getRegionsAdjacentes(carte.regionSelectionnee);
            console.log(regionsAdjacentes);

            //On enregistre les régions adjacentes dans une variable de session
            $.ajax({
                url: 'settersBDD/setRegions.php',
                type: 'POST',
                async: false,
                data: {
                    regions: JSON.stringify(regionsAdjacentes)
                },
                success: function(code_html, statut) { // code_html contient le HTML renvoyé
                    $("body").append(code_html);
                },
                complete: function() {}
            });

            window.location = "exploration.php";
        }
    });
});

function activitesPossibles() {
    if (Constants.debug) return;

    let quetes = aventurier.quetes;
    // debugger;
    /*if (quetes.crafter != QueteFlag.DONE || quetes.ramener3Miurges != QueteFlag.DONE)
        disable("bouton_equipement");*/

    console.log($("#bouton_equipement"));
}

function disable(id) {
    $("#" + id).prop('disabled', true);
}


function initSitesAccessibles() {
    let sitesVisibles = carte.voronoi.sites.filter(x => x.visible != 0);
    sitesVisibles = sitesVisibles.filter(x => carte.ingredientEndemiqueTypeDeLaRegion(x.voronoiId, -1).length > 0);

    let zir = aventurier.getLesStatsTotales().volonte;

    sitesVisibles.forEach(function(x, i) {
        x.magnitude = carte.getMagnitude(x.voronoiId);
        x.zirMinimal = Constants.volonteEntreeDonjon(x.magnitude);
    });
    sitesAccessibles = sitesVisibles.filter(x => zir >= x.zirMinimal);
}

/*
    Délimite les sites où l'on peut se rendre. Ne contient que des sites qui combinent les conditions suivantes :
    - zir d'accessibilité < zit du joueur
    - visible
    - pourvu d'au moins 1 ingrédient endémique
*/
function dessinerOutlineAccessibilite() {
    if (sitesAccessibles == null) return;

    var edgesCandidates = carte.voronoi.diagram.edges.filter(function(edge) {
        let onlyRightAccessible =
            edge.rSite != null &&
            sitesAccessibles.findIndex(x => x.voronoiId == edge.rSite.voronoiId) > -1 &&
            sitesAccessibles.findIndex(x => x.voronoiId == edge.lSite.voronoiId) < 0;
        let onlyLeftAccessible =
            edge.lSite != null &&
            sitesAccessibles.findIndex(x => x.voronoiId == edge.lSite.voronoiId) > -1 &&
            sitesAccessibles.findIndex(x => x.voronoiId == edge.rSite.voronoiId) < 0;

        return onlyRightAccessible || onlyLeftAccessible;
    });

    //Nécessaire pour un outline carré
    edgesCandidates.forEach(function(x) {
        if (x.rSite) {
            x.angle = Math.atan2(x.rSite.y - x.lSite.y, x.rSite.x - x.lSite.x);
        } else {
            var va = x.va,
                vb = x.vb;
            // rhill 2011-05-31: used to call getStartpoint()/getEndpoint(),
            // but for performance purpose, these are expanded in place here.
            x.angle = x.lSite === x.lSite ?
                Math.atan2(vb.x - va.x, va.y - vb.y) :
                Math.atan2(va.x - vb.x, vb.y - va.y);
        }
    });

    let ctx = carte.canvas.getContext('2d');
    ctx.beginPath();

    ctx.strokeStyle = '#dd4b4bBF';

    ctx.lineWidth = 3;
    for (let edge of edgesCandidates) {
        let v = edge.va;
        let end = edge.vb;
        ctx.moveTo(v.x, v.y);

        //Pour un outline carré
        if (edge.angle > 0)
            ctx.lineTo(v.x, end.y);
        else
            ctx.lineTo(end.x, v.y);

        v = edge.vb;
        ctx.lineTo(v.x, v.y);
    }
    ctx.stroke();
    ctx.lineWidth = 1;
}

function parseTextes() {
    $("#title")[0].innerHTML = textes.title;
    $("#question_01").html(textes.order_01);
    $("#bouton_equipement").attr("value", textes.button_equipment);
    //$("#exploration").attr("value", textes.button_adventure);

    $("#exploration").prop("title", textes.no_zone);
    $('#explication').html(textes.explanation)
}