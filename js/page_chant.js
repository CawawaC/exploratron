var aventurier = Aventurier.getLaSession();
var chant;
getChant();

if (Constants.debug) {
    let divDebug = $('.DEBUG');

    jQuery("<input/>", {
        type: "button",
        value: 'Apprendre 1 vers random',
        click: function() { aventurier.apprendVers(random(chant.vers.length));
            aventurier.sauvegarde() }
    }).appendTo(divDebug);
}

function getChant() {
    let response = $.ajax({
        url: 'medias/chant.json',
        type: 'get',
        dataType: 'json',
        error: function(data) {

        },
        success: function(data) {
            chant = data;
            afficherChant();
        }
    });
}

function afficherChant(destructif = true) {
    let versAffiche;
    for (let i = 0; i < chant.vers.length; i++) {
        if(aventurier.poeme.indexOf(i) < 0) {
            versAffiche = " ________"
        } else {
        	let vers = chant.vers[i];

        	vers = vers.split("==>").join("<b>");
        	vers = vers.split("==").join("</b>");

        	versAffiche = vers;
            if(destructif)
            	chant.vers[i] = vers;
        }

        $("<p>").appendTo($("#chant")).html(versAffiche);

    }
}