import { Aventurier } from './aventurier'
// import { Hotelcoffre } from './hotelcoffre'
import { Ingredient } from './ingredient'
import { Objet } from './objets'

var aventurier = Aventurier.getLaSession();
var chenil = aventurier.chenil;

inscrireEquipementFamilier();
inscrireChenil();

$(document).on('click', '#stocker', stocker);
$(document).on('click', '#retirer', retirer);

function inscrireEquipementFamilier() {
	//Nettoyage
	var select = document.getElementById("equipement-familier");
	while(select.options.length > 0) {
		select.options.remove(0);
	}

	for(var e of aventurier.familiers) {
		var x = document.createElement("OPTION");
		x.setAttribute("value", e.id);
		var t = document.createTextNode(e.nom);
		x.appendChild(t);
		select.appendChild(x);
	}
}

function inscrireChenil() {
	//Nettoyage
	var select = document.getElementById("chenil-chambre");
	while(select.options.length > 0) {
		select.options.remove(0);
	}

	for(var e of chenil) {
		var x = document.createElement("OPTION");
		x.setAttribute("value", e.id);
		var t = document.createTextNode(e.nom);
		x.appendChild(t);
		select.appendChild(x);
	}
}

function stocker() {
	var select = document.getElementById("equipement-familier");
	var i = select.selectedIndex;

	if(i >= 0) {
		var option = select.options[i];
		let idFamilierStocke = option.value;
		var familier = aventurier.familiers.filter(x => x.id == idFamilierStocke);
		var iFamilier = aventurier.familiers.indexOf(familier);

		familier = aventurier.familiers.splice(iFamilier, 1)[0];
		aventurier.chenil.push(familier);

		inscrireEquipementFamilier();
		inscrireChenil();
	}

	aventurier.sauvegarde();
}

function retirer() {
	console.log(aventurier.familierCheck())
	if(!aventurier.familierCheck()) {
		$("body").append("<p>Tu as déjà assez de familiers avec toi</p>"); 
		return;
	}

	var select = document.getElementById("chenil-chambre");
	var i = document.getElementById("chenil-chambre").selectedIndex;

	if(i >= 0) {
		for(let opt of select.options) {
			if(opt.selected) console.log(opt)
		}

		var option = select.options[i];
		let idFamilierRetire = option.value;

		var fam = aventurier.chenil.splice(i, 1)[0];
		aventurier.familiers.push(fam);
		aventurier.sauvegarde();

		//Retrait 1 expemplaire ingrédient depuis BDD
		aventurier.chenil.splice(idFamilierRetire, 1);

		inscrireEquipementFamilier();
		inscrireChenil();
	}

	aventurier.sauvegarde();
}