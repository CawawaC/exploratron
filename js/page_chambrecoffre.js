var aventurier = Aventurier.getLaSession();
let coffre = aventurier.coffre;


$(document).ready(function() {
    getTextes("room", parseTextes);


    $("#coffre").change(function(evt) {
        $("#stocker-coffre").attr("disabled", true);
        $("#retirer-coffre").attr("disabled", false);
        $('#inventaire').find(":checkbox:checked").prop('checked', false);
    });
    $("#inventaire").change(function() {
        $("#stocker-coffre").attr("disabled", false);
        $("#retirer-coffre").attr("disabled", true);
        $('#coffre').find(":checkbox:checked").prop('checked', false);
    });

    inscrireInventaire();
    inscrireCoffre();
    actualiserRemplissageSac();

    $(document).on('click', '#stocker-coffre', stockerCoffre);
    $(document).on('click', '#retirer-coffre', retirerCoffre);
});

function clicSupprimer(evt) {
    // console.log(evt.currentTarget.parentNode)
    // console.log(evt.currentTarget.parentNode.htmlFor)
    // let bound = $("#"+evt.currentTarget.parentNode.htmlFor)
    // console.log(bound)
    // bound.remove()

    $($(this).parent()[0].control).addClass('supprimer')
    supprimerCoffre();
}

function parseTextes() {
    $("#consigne_penderie").html(textes.order_01);
}

function inscrireInventaire() {
    //Nettoyage
    $("#inventaire").empty();
    var i = 0;
    for (var e of aventurier.inventaire) {

        $('<input type="checkbox" name="inventaire"  data-id="' + e.id + '" id=i' + i + ' class="itemInventaire"></input> <label for=i' + i + ' ><img src="medias/images/picto-' + Ingredient.Regnes[e.iRegne].nom + '.png">' + e.nom + '  </label>').appendTo('#inventaire');
        i++;
    }
}

function inscrireCoffre() {
    //Nettoyage
    $("#coffre").empty();

    coffre.sort(function(a, b) {
        var textA = a.nom.toUpperCase();
        var textB = b.nom.toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });

    var i = 0;
    for (let e of coffre) {
        $('<input type="checkbox" name="coffre"  data-id="' + 
            e.id + '" id=c' + i + 
            ' class="itemCoffre"></input> <label for=c' + i + '><img src="medias/images/picto-' + Ingredient.Regnes[e.iRegne].nom + '.png">' + 
            e.nom + 
            '<img src="medias/images/picto-poubelle.png" alt="supprimer" class="pictoPoubelle"></label>')
        .appendTo('#coffre');

        i++;
    }
    $(".pictoPoubelle").on("click", clicSupprimer);
}

function stockerCoffre() {
    let value;
    $('input:checkbox.itemInventaire').each(function() {
        if (!$(this).prop('checked'))
            return true;

        let value = $(this).data("id");

        let iIngredient = aventurier.inventaire.findIndex(x => x.id == value);

        ingredient = aventurier.inventaire.splice(iIngredient, 1)[0];
        coffre.push(ingredient);

        $("#stocker-coffre").attr("disabled", true);
        $("#retirer-coffre").attr("disabled", true);

        aventurier.sauvegarde();

        inscrireInventaire();
        inscrireCoffre();
        actualiserRemplissageSac();
    });

}

function retirerCoffre() {

    let id;
    $('input:checkbox.itemCoffre').each(function() {

        if (!$(this).prop('checked')) {
            return true;
        } else if (!aventurier.placeDansLeSac()) {
            $("body").append("<p>Mon inventaire est plein, ju ne peux pas retirer plus de cartes de visite</p>");
            return true;
        }

        id = $(this).data("id");

        let ingredient = coffre.filter(x => x.id == id)[0];
        let iIngredient = coffre.indexOf(ingredient);
        let ing = coffre.splice(iIngredient, 1)[0];
        aventurier.inventaire.push(ing);


        aventurier.sauvegarde();

        inscrireInventaire();
        inscrireCoffre();
        actualiserRemplissageSac();

    });
}

function supprimerCoffre() {
    let id;
    $('input:checkbox.itemCoffre').each(function() {

        if (!$(this).prop('checked') || !$(this).hasClass('supprimer')) {
            return true;
        }

        id = $(this).data("id");

        let ingredient = coffre.filter(x => x.id == id)[0];
        let iIngredient = coffre.indexOf(ingredient);
        let ing = coffre.splice(iIngredient, 1)[0];

        $("#stocker-coffre").attr("disabled", true);
        $("#retirer-coffre").attr("disabled", true);

        aventurier.sauvegarde();

        inscrireCoffre();

    });
}

function actualiserRemplissageSac() {
    var divRemplissage = $("#remplissage-sac");
    divRemplissage.empty();
    divRemplissage.append(aventurier.inventaire.length + "/" + aventurier.getLesStatsTotales().sac);
}