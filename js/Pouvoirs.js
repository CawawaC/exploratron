/*
    Utilisation :
    if((pouvoir.drapeaux & DRAPEAUX.fuiteDonjon) === DRAPEAUX.fuiteDonjon)
        On peut fuire

    Simplifié :
    if(pouvoir.fuiteDonjon)
        On peut fuire

    if(pouvoir.equipementType) {
        for(let e of equipementType.indexType)
            console.log(Objet.Categories[e])
    }
*/
class Pouvoirs {
    constructor(magnitudeMoyenne) {
        /*
            https://gist.github.com/corymartin/2393268
        */
        this.drapeaux = Constants.objetPouvoirs(magnitudeMoyenne);

        // this.bonusConsommation = {indexRegne: 0, intensite: 0};
        // this.equipementType = {types: [0]};
        // this.tailleDuSac =   0;
        // this.apprivoiser = {regne: 0};
        // this.chant = 0;
        // this.chancesApparitionIng = {regne: 0};
        // this.reductionCoutExploration = 0;
        // this.equipementGenerique {nombre: 0};

        this.initValeursVariables(magnitudeMoyenne);

        // console.log(magnitudeMoyenne, this);
    }

    somme(pvr) {
        let sum = new Pouvoirs();
        sum.drapeaux = this.drapeaux | pvr.drapeaux;

        sum.bonusConsommation = this.bonusConsommation;
        if (this.bonusConsommation == null) {
            sum.bonusConsommation = pvr.bonusConsommation;
        } else if (pvr.bonusConsommation != null) {
            if (sum.bonusConsommation[0].indexRegne == pvr.bonusConsommation[0].indexRegne)
                sum.bonusConsommation[0].intensite += pvr.bonusConsommation[0].intensite;
            else
                sum.bonusConsommation = sum.bonusConsommation.concat(pvr.bonusConsommation);
        }

        sum.equipementType = this.equipementType;
        if (this.equipementType == null) {
            sum.equipementType = pvr.equipementType;
        } else if (pvr.equipementType != null) {
            sum.equipementType = sum.equipementType.concat(pvr.equipementType.indexType);
        }

        sum.tailleDuSac = this.tailleDuSac;
        if (this.tailleDuSac == null) {
            sum.tailleDuSac = pvr.tailleDuSac;
        } else if (pvr.tailleDuSac != null) {
            sum.tailleDuSac += pvr.tailleDuSac;
        }

        sum.rechargeTempsAInteraction = this.rechargeTempsAInteraction;
        if (this.rechargeTempsAInteraction == null) {
            sum.rechargeTempsAInteraction = pvr.rechargeTempsAInteraction;
        } else if (pvr.rechargeTempsAInteraction != null) {
            sum.rechargeTempsAInteraction += pvr.rechargeTempsAInteraction;
        }

        sum.reductionCoutExploration = this.reductionCoutExploration;
        if (this.reductionCoutExploration == null) {
            sum.reductionCoutExploration = pvr.reductionCoutExploration;
        } else if (pvr.reductionCoutExploration != null) {
            sum.reductionCoutExploration += pvr.reductionCoutExploration;
        }

        sum.apprivoiser = this.apprivoiser;
        if (this.apprivoiser == null) {
            sum.apprivoiser = pvr.apprivoiser;
        } else if (pvr.apprivoiser != null) {
            sum.apprivoiser = sum.apprivoiser.concat(pvr.apprivoiser);
        }
        /*
                sum.enchainerDonjons = this.enchainerDonjons;
                if (this.enchainerDonjons == null) {
                    sum.enchainerDonjons = pvr.enchainerDonjons;
                }*/

        sum.chant = this.chant;
        if (this.chant == null) {
            sum.chant = pvr.chant;
        } else if (pvr.chant != null) {
            sum.chant += pvr.chant;
        }

        sum.agressiviteCreatures = this.agressiviteCreatures;
        if (this.agressiviteCreatures == null) {
            sum.agressiviteCreatures = pvr.agressiviteCreatures;
        }

        sum.fuiteDonjon = this.fuiteDonjon;
        if (this.fuiteDonjon == null) {
            sum.fuiteDonjon = pvr.fuiteDonjon;
        }

        sum.positionJoueur = this.positionJoueur;
        if (this.positionJoueur == null) {
            sum.positionJoueur = pvr.positionJoueur;
        }

        sum.positionSalleSecrete = this.positionSalleSecrete;
        if (this.positionSalleSecrete == null) {
            sum.positionSalleSecrete = pvr.positionSalleSecrete;
        }

        sum.fuiteCombat = this.fuiteCombat;
        if (this.fuiteCombat == null) {
            sum.fuiteCombat = pvr.fuiteCombat;
        }

        sum.chancesApparitionIng = this.chancesApparitionIng;
        if (this.chancesApparitionIng == null) {
            sum.chancesApparitionIng = pvr.chancesApparitionIng;
        } else if (pvr.chancesApparitionIng != null) {
            if (sum.chancesApparitionIng[0].indexRegne == pvr.chancesApparitionIng[0].indexRegne)
                sum.chancesApparitionIng[0].intensite += pvr.chancesApparitionIng[0].intensite;
            else
                sum.chancesApparitionIng = sum.chancesApparitionIng.concat(pvr.chancesApparitionIng);
        }


        sum.equipementGenerique = this.equipementGenerique;
        if (this.equipementGenerique == null) {
            sum.equipementGenerique = pvr.equipementGenerique;
        } else if (pvr.equipementGenerique != null) {
            sum.equipementGenerique += pvr.equipementGenerique;
        }

        sum.carteDonjon = this.carteDonjon;
        if (this.carteDonjon == null || Constants.debug) {
            sum.carteDonjon = pvr.carteDonjon;
        }

        

        // console.log("somme : ", sum)

        return sum;
    }

    initValeursVariables(magnitudeMoyenne) {
        // console.log(magnitudeMoyenne);
        magnitudeMoyenne = parseInt(magnitudeMoyenne);

        if ((this.drapeaux & DRAPEAUX.bonusConsommation) === DRAPEAUX.bonusConsommation) {
            // console.log("bonus conso")

            let i = Math.floor(Math.random() * Ingredient.Regnes.length);
            this.bonusConsommation = [{
                indexRegne: i,
                intensite: parseInt((magnitudeMoyenne / Constants.bonusConsommation.intensiteReduction).toFixed(1))
            }];
        }
        if ((this.drapeaux & DRAPEAUX.equipementType) === DRAPEAUX.equipementType) {
            // console.log("donne slots equipement")

            this.equipementType = [];
            let nbSlots = Math.floor(magnitudeMoyenne / Constants.equipementType.nombreSlotsReduction) + 1;
            for (let i = 0; i < nbSlots; i++) {
                let slot = Math.floor(Math.random() * Objet.Categories.length);
                this.equipementType.push(slot);
            }
        }
        if ((this.drapeaux & DRAPEAUX.tailleDuSac) === DRAPEAUX.tailleDuSac) {
            this.tailleDuSac = Math.ceil(magnitudeMoyenne / Constants.tailleDuSacReduction);
        }

        if ((this.drapeaux & DRAPEAUX.rechargeTempsAInteraction) === DRAPEAUX.rechargeTempsAInteraction) {
            this.rechargeTempsAInteraction = Math.ceil(magnitudeMoyenne / Constants.rechargeTempsAInteractionReduction);
        }

        if ((this.drapeaux & DRAPEAUX.reductionCoutExploration) === DRAPEAUX.reductionCoutExploration) {
            this.reductionCoutExploration = Math.ceil(magnitudeMoyenne / Constants.reductionCoutExplorationReduction);
        }

        if ((this.drapeaux & DRAPEAUX.apprivoiser) === DRAPEAUX.apprivoiser) {
            let i = Math.floor(Math.random() * Ingredient.Regnes.length);
            this.apprivoiser = [i];
        }
        /*if ((this.drapeaux & DRAPEAUX.enchainerDonjons) === DRAPEAUX.enchainerDonjons) {
            this.enchainerDonjons = true;
        }*/
        if ((this.drapeaux & DRAPEAUX.chant) === DRAPEAUX.chant) {
            this.chant = 1;
        }
        if ((this.drapeaux & DRAPEAUX.agressiviteCreatures) === DRAPEAUX.agressiviteCreatures) {
            this.agressiviteCreatures = false;
        }
        if ((this.drapeaux & DRAPEAUX.fuiteDonjon) === DRAPEAUX.fuiteDonjon) {
            this.fuiteDonjon = true;
        }
        if ((this.drapeaux & DRAPEAUX.positionJoueur) === DRAPEAUX.positionJoueur) {
            this.positionJoueur = true;
        }
        if ((this.drapeaux & DRAPEAUX.positionSalleSecrete) === DRAPEAUX.positionSalleSecrete) {
            this.positionSalleSecrete = true;
        }


        if ((this.drapeaux & DRAPEAUX.fuiteCombat) === DRAPEAUX.fuiteCombat) {
            this.fuiteCombat = true;
        }
        if ((this.drapeaux & DRAPEAUX.chancesApparitionIng) === DRAPEAUX.chancesApparitionIng) {
            let i = Math.floor(Math.random() * Ingredient.Regnes.length);
            this.chancesApparitionIng = [{ indexRegne: i, intensite: 1 }];
        }

        if ((this.drapeaux & DRAPEAUX.equipementGenerique) === DRAPEAUX.equipementGenerique) {
            let nbSlots = Math.floor(magnitudeMoyenne / Constants.equipementGenerique.nombreSlotsReduction);
            this.equipementGenerique = nbSlots;
        }
        if ((this.drapeaux & DRAPEAUX.carteDonjon) === DRAPEAUX.carteDonjon) {
            this.carteDonjon = true;
        }
        if ((this.drapeaux & DRAPEAUX.grosseRecolte) === DRAPEAUX.grosseRecolte) {
            this.grosseRecolte = Constants.grosseRecolte(magnitudeMoyenne);
        }
    }

    /*
        Renvoie un tableau de strings = aux noms des drapeaux présents dans les pouvoirs
    */
    getDrapeauxSelectionnes() {
        if (this.drapeaux === DRAPEAUX.aucun)
            return [];

        var selected = [];

        for (var flag in DRAPEAUX) {
            if (flag === 'aucun')
                continue;

            var val = DRAPEAUX[flag];

            if ((this.drapeaux & val) === val)
                selected.push(flag);
        }

        return selected;
    }

    static fromBDD(obj) {
        // console.log(obj)
        var pvr = new Pouvoirs(0);
        pvr.drapeaux = parseInt(obj.drapeaux);

        if (obj.bonusConsommation) {
            for (let e of obj.bonusConsommation) {
                e.indexRegne = parseInt(e.indexRegne);
                e.intensite = Number(e.intensite);
            }
            pvr.bonusConsommation = obj.bonusConsommation;
        }
        if (obj.equipementType) {
            pvr.equipementType = obj.equipementType;
            pvr.equipementType = pvr.equipementType.map(e => parseInt(e))
        }
        if (obj.tailleDuSac) pvr.tailleDuSac = parseInt(obj.tailleDuSac);
        if (obj.rechargeTempsAInteraction) pvr.rechargeTempsAInteraction = parseInt(obj.rechargeTempsAInteraction);
        if (obj.reductionCoutExploration) pvr.reductionCoutExploration = parseInt(obj.reductionCoutExploration);

        if (obj.positionJoueur) pvr.positionJoueur = obj.positionJoueur == "true";
        if (obj.apprivoiser) pvr.apprivoiser = obj.apprivoiser;
        // if (obj.enchainerDonjons) pvr.enchainerDonjons = obj.enchainerDonjons;
        if (obj.chant) pvr.chant = obj.chant;
        if (obj.agressiviteCreatures) pvr.agressiviteCreatures = obj.agressiviteCreatures;
        if (obj.fuiteDonjon) pvr.fuiteDonjon = obj.fuiteDonjon == "true";
        if (obj.fuiteCombat) pvr.fuiteCombat = obj.fuiteCombat;
        if (obj.chancesApparitionIng) pvr.chancesApparitionIng = obj.chancesApparitionIng;
        if (obj.equipementGenerique) pvr.equipementGenerique = obj.equipementGenerique;
        if (obj.carteDonjon) pvr.carteDonjon = obj.carteDonjon == "true";
        if (obj.positionSalleSecrete) pvr.positionSalleSecrete = obj.positionSalleSecrete == "true";
        if (obj.grosseRecolte) pvr.grosseRecolte = obj.grosseRecolte;

        // console.log(obj, pvr);
        return pvr;
    }

    static aucun() {
        var pvr = new Pouvoirs(0);
        pvr.tailleDuSac = 0;
        pvr.drapeaux = DRAPEAUX.aucun;
        return pvr;
    }

    toString(afficherSac = true) {
        let s = []

        if(this.bonusConsommation) {
            for (let b of this.bonusConsommation) {
                if (b.intensite == 0) continue;
                s.push("Bonus de consommation des " + Ingredient.Regnes[b.indexRegne].pluriel + " : " + b.intensite);
            }
        }

        if(this.positionJoueur) {
            s.push("Coordonnées visibles sur la carte");
        }

       /*if(this.tailleDuSac && afficherSac) {
            s.push("Bonus de taille du sac : " + this.tailleDuSac);
        }*/

        if(this.chancesApparitionIng) {
            for (let b of this.chancesApparitionIng) {
                if (b.intensite == 0) continue;
                s.push("Chance d'apparition des " + Ingredient.Regnes[b.indexRegne].pluriel + " : " + b.intensite);
            }
        }

        if(this.chant) {
            s.push("Aide au chant : " + this.chant);
        }

        if(this.reductionCoutExploration) {
            s.push("Réduction du coût de l'exploration : " + this.reductionCoutExploration);
        }

        if(this.agressiviteCreatures) {
            s.push("Miurges aggressifs");
        }

        if(this.grosseRecolte) {
            s.push("Bonus de récolte : " + this.grosseRecolte);
        }

        if(this.carteDonjon) {
            s.push("Connaissance des plans des suites")
        }

        if(this.positionSalleSecrete) {
            s.push("Connaissance de la position de la salle secrète d'une suite")
        }

        if(this.rechargeTempsAInteraction) {
            s.push("Interactions réussies avec miurges offrent du temps : " + this.rechargeTempsAInteraction)
        }

        if(this.apprivoiser) {
            for (let b of this.apprivoiser) {
                s.push("Apprivoisement des " + Ingredient.Regnes[b].pluriel);
            }
        }

        return s;
    }

    /*afficher(grosDiv) {
        if (this.bonusConsommation && this.bonusConsommation.filter(x => x.intensite != 0).length > 0) {
            let div = jQuery("<div>Bonus de consommation :</div>").appendTo(grosDiv).append("<br>");
            for (let b of this.bonusConsommation) {
                if (b.intensite == 0) continue;
                div.append(Ingredient.Regnes[b.indexRegne].nom)
                    .append(" : ")
                    .append(b.intensite)
                    .append("<br>")
            }
        }
        if(this.tailleDuSac) {
            let div = jQuery("<div>Taille du sac :</div>").append(this.tailleDuSac + Constants.tailleDuSac).appendTo(grosDiv).append("<br>");
        }
        if (this.apprivoiser) {
            let div = jQuery("<div>Apprivoisement :</div>").appendTo(grosDiv).append("<br>");
            div.appendTo(grosDiv)
            for (let a of this.apprivoiser) {
                jQuery(Ingredient.Regnes[a].nom).appendTo(div)
            }
        }
        if (this.fuiteDonjon) {
            grosDiv.append("Fuite donjon").append("<br>")
        }

        if (this.carteDonjon)
            grosDiv.append("Carte du donjon révélée").append("<br>")

        if(this.grosseRecolte) {
            grosDiv.append("Recolte améliorée : ", this.grosseRecolte).append("<br>");
        }

        if(this.chant) {
            grosDiv.append("Aide au chant : ", this.chant).append("<br>");
        }
    }*/
}
/*
var drapeauxModele = [
    "bonusConsommation",
    "tailleDuSac",
    "positionJoueur",
    "apprivoiser",
    "chant",
    "agressiviteCreatures",
    "fuiteDonjon",
    "fuiteCombat",
    "chancesApparitionIng",
    "reductionCoutExploration",
    "carteDonjon",
];
*/


/*get DRAPEAUX() {
    let retour = {}
    let i = 0
    for (let d of drapeauxModele) {
        retour[d] = 1 << i;
        i++;
    }
}*/
var DRAPEAUX = {
    aucun: 0,
    bonusConsommation:          1 << 0, //Variable
    positionJoueur:             1 << 1, //Variable
    tailleDuSac:                1 << 2, //Variable
    chancesApparitionIng:       1 << 3, //Variable
    chant:                      1 << 4, //Variable
    reductionCoutExploration:   1 << 5, //Variable
    agressiviteCreatures:       1 << 6, //Variable
    grosseRecolte:              1 << 7, //Variable
    carteDonjon:                1 << 8, //Variable
    positionSalleSecrete:       1 << 9, //Variable
    rechargeTempsAInteraction:  1 << 10, //Variable
    apprivoiser:                1 << 11 //Variable
};