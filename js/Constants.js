

/*
	Equilibrage
	
	- Scaling stats ingrédients avec la distance
	- Répartition des stats d'un ingrédient à une distance donnée
	- Scaling prix d'entrée donjon


	J'entre dans un donjon de magnitude 100 (région adjacente ou presque). 
	Je dépense xx.
	Je rencontre des ingrédients aux stats xx.
	Avec ces ingrédients, je fabrique un objet aux stats xx.


	Power Curve
	Fermi Solution
	Quick pointing
	Dark magic
	Triple tapping
	Safeguards

*/


class Constants {
	static ingredientConsommation(ingredientStats) {
        return new Stats(
            Math.round((Math.random() * 2 - 1) * ingredientStats.moral),
            0,
            Math.round((Math.random() * 2 - 1) * ingredientStats.volonte)
        );
	}

	/*
		Points de tempts offerts par la consommation d'un inngrédient 
	*/
	static consommation(ingredient) {
		let conso = Math.round(Math.sqrt(ingredient.magnitude));
		console.log(conso)
		if(conso == 0) conso = 1;
		return conso;
	}

	static ingredientVolonte(magnitude) {
        return Math.round(Math.pow(magnitude / 100 + 3, 2));
	}

	/*
		Array de int en parametre
	*/
	static objetVolonte(volontesIngredients) {
		let sum = 0;
		for(let v of volontesIngredients)
			sum += v;
		let average = sum/volontesIngredients.length;
		let volonte = Math.round(average / 6);

		let volonteGaussienne = Math.round(Constants.gaussienne(volonte/3, volonte));
		if(volonteGaussienne < 0) volonteGaussienne = 0;

		return volonteGaussienne;
	}

	/*
		Array de int en parametre
	*/
	static objetMoral(volontesIngredients) {
		let sum = 0;
		for(let v of volontesIngredients)
			sum += v;
		let average = sum/volontesIngredients.length;
		let moral = Math.round(average / 3);

		let moralGaussien = Math.round(Constants.gaussienne(moral/3, moral));
		if(moralGaussien < 0) moralGaussien = 0;

		return moralGaussien;
	}

	static objetElegance(volontesIngredients) {
		let sum = 0;
		for(let v of volontesIngredients)
			sum += v;
        return Math.round(Constants.gaussienne(sum, sum));
	}

	// Standard Normal variate using Box-Muller transform.
	//(0, 1)
	static gaussienne(sigma, mu) {
	    var u = 0, v = 0;
	    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
	    while(v === 0) v = Math.random();
	    return Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v ) * sigma + mu;
	}

	/*
		Tirage 2 randoms, donnent une coordonnée. 
		Si le point est au-dessus de la droite décroissante qui symbolise la décroissance de la probabilité de haute valeur d'un pouvoir, 
		On fait un nouveau tirage.
		Sinon, la coordonnée x donne la valeur de la variable binaire pouvoir.

		L'affine vaut 1 à l'origine, et 0 à x = [magnitude moyenne des ingrédients]

		Tirage 2 nombres, combinaison binaire possible pour enlever des pouvoirs

		Autre algo : 
		Tirage binaire, digit par digit, nombre de digit déterminé par magnitude
	*/
	static pouvoirSubLineaire(magnitudeMoyenne) {
		let x, y, fx;
		let a = magnitudeMoyenne;
		do {
			x = Math.random() * magnitudeMoyenne;
			y = Math.random();
			// fx = -1/magnitudeMoyenne * x + 1;	//Affine décroissante
			fx = Math.pow((a - x), 2)/Math.pow(a, 2);
		} while (y > fx);

		return Math.floor(x);
	}

	static objetPouvoirs(magnitudeMoyenne) {
		return this.pouvoirSubLineaire(magnitudeMoyenne)// & this.pouvoirSubLineaire(magnitudeMoyenne);
	}

	/*
		Coût d'un déplacement dans le donjon
	*/
	static coutDeplacement(magnitude) {
		// return (magnitude/80)**2;
		return (magnitude/160)**2;
	}

	/*
		Nombre de lettres suggérées dans un mot lors de l'application du pouvoir relatif au chant
	*/
	static pouvoirChantSuggestion(intensite) {
		return Math.sqrt(intensite);
		// return Math.ln(intensite) + 1;
	}

	/*
		Zir nécessaire pour entrer dans une région / donjon
	*/
	static volonteEntreeDonjon(magnitude) {
		return (magnitude/100) ** 2 + 10;
	}

	/*
		Pouvoir multiplicateur de récolte
	*/
	static grosseRecolte(magnitude) {
		return magnitude / 150 + 1;
	}

	/*
		Obtention d'un ingrédient : quantité aléatoire définie par répartition gaussienne et scalée par un multiplicateur (issu du pouvoir grosseRecolte)
	*/
	static cueillette(multiplicateur) {
		var g = 0;
		while(g <= 0) 
			g = Constants.gaussienne(multiplicateur, multiplicateur)
		
		return Math.round(g);
	}

	/*
		Taille du donjon en fonction de la magnitude de sa région. 
		Plus précisément, la longueur du côté du carré qui contient toutes les pièces.
		Le même nombre est utilisé pour le nombre de pièces.
	*/
	static tailleDonjon(magnitude) {
		// Math.floor(map_range(distanceRegionActuelle, 0, 500, 5, Constants.scaleTailleDonjon) / 2);
		return Math.round(0.6 * Math.sqrt(magnitude) + 4)
	}
}

Constants.debug = true;
Constants.agressivite = false;
Constants.ramassagePhrases = false;

Constants.tailleDuSac = 6;
Constants.offsetVolonte = 10;
Constants.gainTempsCreaObjetConnu = 1;
Constants.gainTempsCreaObjetNouveau = 2;
Constants.zirMinimalCreationIngredient = 15;
Constants.scaleTailleDonjon = 50 //25

Constants.magnitudeMaximale = 1000;


Constants.bonusConsommation = {};
Constants.bonusConsommation.intensiteReduction = 100;
Constants.equipementType = {}
Constants.equipementType.nombreSlotsReduction = 250;
Constants.tailleDuSacReduction = 200;
Constants.rechargeTempsAInteractionReduction = 20;
Constants.reductionCoutExplorationReduction = 100;
Constants.equipementGenerique = {}
Constants.equipementGenerique.nombreSlotsReduction = 500;