/*import { Ingredient } from "./ingredient"
import { Carte } from "./carte"
import { Aventurier } from './Aventurier';*/

var nouvelIngredient;
var regne;
var genre;
var description;

var longueurDescriptionMinimale = 25;
var longueurDescriptionMaximale = 280;

var aventurier = Aventurier.getLaSession();
var carte = new Carte(Carte.Filtres.Saturation);
carte.init();
carte.initInterface();


var checked = true;
var radio = document.createElement("div");

for (let i in Ingredient.Regnes) {
    var regne = Ingredient.Regnes[i];
    var listeRegnes = document.getElementById("regne");
    var s = "<input type=\"radio\" id=\"" + regne.nom + "\" value=\"" + regne.nom + "\" name=\"regne\"";

    /*if(checked) {
        s += " checked";
        checked = false;
    }*/

    var description = Ingredient.Regnes[i].description;

    s += "> <label for=\"" + regne.nom + "\" title=\"" + description + "\"><img src='medias/images/picto-"+ regne.nom +".png'>" + regne.nom + "</label>";

    radio.innerHTML += s;
    listeRegnes.appendChild(radio);

    radio.addEventListener('input', function(evt) {
        var nomRegne = evt.target.defaultValue;
        carte.regneSelectionne = Ingredient.Regnes.findIndex(e => e.nom == nomRegne);
        carte.render();
        var label = ($("label[for='"+$(evt.target).attr("id")+"']"))
        $('#bloc-genre').css('margin-top',label.offset().top - label.parent().offset().top);
        $('#bloc-genre').removeClass('bloc-cache');
    });
}

$(document).ready(function() {
    getTextes("ingredient_creation", parseTextes);

    if(aventurier.quetes.creerIngredient == QueteFlag.STARTED)
        aventurier.quetes.creerIngredient = QueteFlag.IN_PROGRESS;


    //Reveler des régions tant que toutes les régions visibles sont intégralement saturées
    if (carte.saturationVisible(-1)) {
        console.log("Reveler region");
        while (carte.saturationVisible(-1))
            carte.revelerRegion();
        carte.sauvegarde();
    }

    if (!Constants.debug && aventurier.getLesStatsTotales().volonte < Constants.zirMinimalCreationIngredient) {
        let div = jQuery("<div>", {
            id: "zir_low"
        });
        jQuery("<p>", {
            id: "error_zir"
        }).appendTo(div)

        jQuery("<p>", {
            id: "error_zir_explain",
            class: "tip"
        }).appendTo(div)

        jQuery("<input>", {
            type: "button",
            id: "retour_hall",
            click: function() { window.location = 'hub.php' }
        }).appendTo(div);

        ouvrirPopUp(div)
    }

    //Click sur regne implique verification validité region selectionnée
    $('#regne').on("click", function(event) {
        carte.regionSelectionnee = -1;    
    });

    $('#genre').on("click", function(event) {
      $('#bloc-description').removeClass('bloc-cache');
    });

    $('#bloc-carte').on("click", function(event) {
        event.stopPropagation();
        event.preventDefault();
        if(carte.regionSelectionnee>0)
            $('#bloc-validation').removeClass('bloc-cache');
        else
            $('#bloc-validation').addClass('bloc-cache');
    });

    $('.condition').click(actualiserActivationSoumission);
    $('#description').on("keypress", actualiserActivationSoumission);

    //Taille description max
    var tailleVerif = function() {
        if (this.value.length > longueurDescriptionMaximale) {
            alert('Maximum length exceeded: ' + longueurDescriptionMaximale);
            this.value = this.value.substr(0, longueurDescriptionMaximale);
            return false;
        }
    }
    var textArea = $('#description')[0];
    textArea.onkeyup = tailleVerif;
    textArea.onblur = tailleVerif;

    $("#voronoiCanvas").on("click", actualiserActivationSoumission);

    $('#sub')[0].onclick = soumettreCreation;

    /*$(document).on('change', '#description', function() { 
        console.log(document.getElementById("description").value.length); 
    });*/

    $('#description').bind('input propertychange', function() {
        $("#nombreCaracteres").text(document.getElementById("description").value.length + "/" + longueurDescriptionMinimale);
        actualiserActivationSoumission();

        if (document.getElementById("description").value.length>=longueurDescriptionMinimale){
            $('#bloc-carte').removeClass('bloc-cache');
            if(carte.regionSelectionnee>0)
                $('#bloc-validation').removeClass('bloc-cache');
            else
                $('#bloc-validation').addClass('bloc-cache');
        }
        else{
             $('#bloc-carte').addClass('bloc-cache');
             $('#bloc-validation').addClass('bloc-cache');
        }

    });
});

function soumettreCreation() {
    if (!ajoutIngredient())
        return;

    //Reset la page de création
    $("#formulaireIngredient")[0].reset();
    Ingredient.ajouteIngredient(nouvelIngredient, carte.regionSelectionnee, aventurier.nom);

    //Tweeter la création
    //nouvelIngredient.tweet(aventurier.nom);

    carte.regneSelectionne = -1;
    carte.getLesIngredients();
    carte.voronoi.selectionnerSite(null);
    carte.render();

    //Si toutes les cases sont saturées par le type d'ingrédient sélectionné, révélation d'un nouvelle case
    if (carte.saturationVisible(nouvelIngredient.iRegne)) {
        console.log("Reveler region");
        carte.revelerRegion();
        carte.sauvegarde();
    }
}
/*
function creerObjet() {
    let ingredients = Ingredient.getIngredientsNommesAleatoires(3);
    console.log(ingredients)

    let categorie = Objet.Categories[Math.floor(Math.random() * Objet.Categories.length)];

    //Requete mysql : voir si un objet avec ces ingrédients a déjà été créé, et le récupérer si c'est le cas
    let objet = Objet.getObjetByRecette(ingredients.map(x => x.id));

    //Sinon new Objet
    if (objet == null) {
        objet = new Objet(ingredients, categorie);
        Objet.ajouteObjet(objet, aventurier);
    }
    console.log(objet);
    return objet;
}
*/
function actualiserActivationSoumission() {

    $("#sub").prop("disabled", false);
    return;

    var radioRegne = getBoutonRadioActif("regne");
    var radioGenre = getBoutonRadioActif("genre");
    var descriptionElement = $('#description')[0];
    if (radioRegne == null) {
        $("#sub").prop("disabled", true);
    } else if (radioGenre == null) {
        $("#sub").prop("disabled", true);
    } else if (descriptionElement.value != undefined && (descriptionElement.value.length < longueurDescriptionMinimale || descriptionElement.value.length > longueurDescriptionMaximale)) {
        $("#sub").prop("disabled", true);
    } else if (carte.regionSelectionnee < 0 || carte.ingredientEndemiqueTypeDeLaRegion(carte.regionSelectionnee, radioRegne.value).length > 0) {
        $("#sub").prop("disabled", true);
    } else {
        $("#sub").prop("disabled", false);
    }
}

function getBoutonRadioActif(nomDuGroupe) {
    var radios = document.getElementsByName(nomDuGroupe);
    for (var r of radios)
        if (r.checked) return r;
    return null;
}

function ajoutIngredient() {
    console.log(textes)
    var radioRegne = getBoutonRadioActif("regne");
    var radioGenre = getBoutonRadioActif("genre");
    let div = jQuery("<div>");
    let iRegne = -1;

    let errorButton = jQuery("<input>", {
        type: "button",
        class: "interaction",
        id: "fermerPopup",
        value: textes.error_buttons,
        click: fermerPopUp
    });

    //Si aucun règne sélectionné, erreur
    if (radioRegne == null) {
        div.html(textes.error_category).append(errorButton)
        ouvrirPopUp(div);
        return false;
    } else {
        iRegne = Ingredient.Regnes.findIndex(e => e.nom == radioRegne.value);
        regne = Ingredient.Regnes[iRegne];
    }

    //Si aucun genre sélectionné, erreur
    if (radioGenre == null) {
        div.html(textes.error_gender).append(errorButton)
        ouvrirPopUp(div);
        return false;
    } else if (radioGenre.value == "rand") {
        genre = Math.random() >= 0.5 ? "feminin" : "masculin";
    } else
        genre = radioGenre.value;

    let genreBinaire = genre == "masculin" ? 1 : 0;


    description = document.getElementById("description").value;

    //Si description trop courte, erreur
    if (description.length < longueurDescriptionMinimale) {
        div.html(textes.error_description).append(errorButton)
        ouvrirPopUp(div);
        return false;
    }

    //Si aucune région sélectionnée, erreur
    if (carte.regionSelectionnee == -1) {
        div.html(accorderTexte(textes.error_place_no_selection, genreBinaire)).append(errorButton)
        ouvrirPopUp(div);
        return false;
    }

    //Si dejà ingrédient endémique présent dans région, ajoutable = false
    if (regne != null && carte.regionSelectionnee > -1) {
        if (carte.ingredientEndemiqueTypeDeLaRegion(carte.regionSelectionnee, regne).length > 0) {
            div.html(accorderTexte(textes.error_place, genreBinaire)).append(errorButton)
            ouvrirPopUp(div);
            return false;
        }
    }

    //Tout est bon, on crée le miurge
    console.log(textes);
    let create_01 = accorderTexte(textes.create_01, genreBinaire);

    let genreAccorde = genre;
    if (genreBinaire == 0)
        genreAccorde = "féminine";

    let create_02 = accorderTexte(textes.create_02, genreBinaire);
    let create_03 = accorderTexte(textes.create_03, genreBinaire);

    div.html(create_01 + regne.nom + " " + genreAccorde + textes.create_01_2 +
        create_02 + description + "." +
        create_03 + carte.regionSelectionnee + ".<br><br>")
/*

    //Récompenser l'aventurier avec un objet
    if (Ingredient.nombreIngredientsNommes() >= 3) {
        let cadeau = creerObjet();
        let cadeauP = jQuery("<p>")
        cadeauP.append("Vous avez été récompensés avec un objet : ").append(cadeau.nom).append(". <br>")
        if (!aventurier.equipementCheck(cadeau.categorie)) {
            aventurier.vestiaire.push(cadeau)
            cadeauP.append("Vous êtes déjà équipé d'un objet de ce type, il a donc été rangé dans votre penderie.");
        } else {
            aventurier.equipement.push(cadeau)
        }
        div.append("<br><br>")
        div.append(cadeauP)
        aventurier.sauvegarde();
    }
*/

    jQuery("<input>", {
        type: "button",
        class: "interaction",
        id: "init_page",
        value: textes.create_button,
        click: function() { window.location = 'creation_ingredient.php' }
    }).appendTo(div);
    jQuery("<input>", {
        type: "button",
        class: "interaction",
        id: "retour_hall",
        value: textes.back_to_hall_button,
        click: function() { window.location = 'hub.php' }
    }).appendTo(div);
    ouvrirPopUp(div)

    let magnitude = carte.getMagnitude(carte.regionSelectionnee);
    var agressif = 0;
    if (regne.combat)
        agressif = Math.floor(Math.random() * 2);

    nouvelIngredient = new Ingredient(0, iRegne, genre, description, carte.regionSelectionnee, magnitude, agressif);

    return true;
}

function parseTextes() {
    $("#title").html(textes.title);
    $("#order").html(textes.order);
    $("#order_01").html(textes.order_01);
    $("#order_02").html(textes.order_02);
    $("#description").attr("placeholder", textes.order_03);
    $("#order_04").html(textes.order_04);
    $("#centrer").attr("value", textes.center_map);
    $("#sub").attr("value", textes.button_create);


    $('#label_01')[0].innerHTML = textes.gender_01;
    $('#label_02')[0].innerHTML = textes.gender_02;
    $('#label_03')[0].innerHTML = textes.gender_03;


    $('#rce').click(function() { window.location = "registre.php" });
    $("#error_zir").html(textes.error_zir);
    $("#error_zir_explain").html(textes.error_zir_explain + Constants.zirMinimalCreationIngredient)
    $("#retour_hall").prop("value", textes.back_to_hall_button)
}