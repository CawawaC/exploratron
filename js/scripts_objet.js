var nouvelObjet;
var ingredientsExistants = [];
var ingredientsDisponibles = [];
var textesDescription;

var longueurDescriptionMaximale = 50;

var aventurier = Aventurier.getLaSession();
aventurier.recettes = aventurier.getRecettes();

function onlyUnique(value, index, self) {
    if (value == null) return false;
    return self.findIndex(x => x.id == value.id) === index;
}

let recettes = Objet.getObjetsByIds(aventurier.recettes);
recettes = recettes.filter(x => x != null);
recettes = recettes.filter(onlyUnique);

let ingredientsInsuffisants = false

if (Constants.debug) {
    let divDebug = $('.DEBUG');
    let tousLesIngredients = Ingredient.getIngredientsTous();

    jQuery("<input/>", {
        type: "button",
        value: 'objet random',
        click: Objet.testCrea
    }).appendTo(divDebug);

    /*jQuery("<input/>", {
        type: "button",
        value: 'clean recettes',
        click: Objet.cleanRecettes
    }).appendTo(divDebug);*/

    divDebug.append("<br>")

    jQuery("<input/>", {
        type: "button",
        value: '!!! Générer recettes pour tous les ingrédients existants !!!',
        click: Objet.retroCreationRecettes
    }).appendTo(divDebug);

    $(document).on('click', '#cadeau', cadeau);

    function cadeau() {
        // Hotelcoffre.RICHESSE();
    }

    $(document).on('click', '#poubelle', poubelle);

    function poubelle() {
        // Hotelcoffre.PAUVRETE();
    }
}

function descriptionValide() {
    let longueur = document.getElementById("description").value.length;
    if (longueur > longueurDescriptionMaximale) return false;
    else return true;
}

$(document).ready(function() {

    getTextesDescription();

    $('#description').bind('input propertychange', function() {
        let longueur = document.getElementById("description").value.length
        $("#nombreCaracteres").text(longueur + "/" + longueurDescriptionMaximale);
    });

    getTextes("object_creation", function() {
        // populerCategories();
        parseTextes();

        // Adapter le contenu de la page si on n'a pas assez d'ingrédients
        if (!Constants.debug && ingredientsDisponibles.length < 3) {
            ingredientsInsuffisants = true;

            var div = jQuery("<div>");
            jQuery("<p>").html(textes.missing_ingredients).appendTo(div)
            jQuery("<p>", { class: "tip" })
                .append(textes.missing_ingredients_explain_b)
                .append(aventurier.inventaire.filter(onlyUnique).length)
                .append(". ")
                .append(textes.missing_ingredients_explain_a)
                .append("3. ")
                .appendTo(div);

            jQuery("<input>", {
                type: "button",
                value: "Retourner dans le Grand Hall",
                click: function() { window.location = "hub.php" }
            }).appendTo(div)
            jQuery("<input>", {
                type: "button",
                value: "Voir mon coffre",
                click: function() { window.location = "chambre-coffre.php" }
            }).appendTo(div)

            // + "<br/><br/><a href='hub.php'>" + textes.hall_button + "</a>";
            ouvrirPopUp(div);

        } else {
            remplirDropdowns();
            defautDropdowns()
            populerRecettes();
            connecterInterface();
        }
    });

    getStock();
    actualiserActivationSoumission();

    console.log(ingredientsDisponibles.length);
});

function connecterInterface() {
    $("#bouton_guestlist").click(function(event) {
        event.preventDefault();
        $(".panneau-lateral#equipement").addClass('ouvert');
        $(".colonneCentrale").css("filter", "blur(2.5px)");
        $(".ombre").addClass("montree");
        $(".ombre").removeClass("cachee");
        $("#stats-joueur").removeClass("transparent");
    });

    $(".panneau-lateral .fermeture").click(function(event) {
        event.preventDefault();
        fermerPanneau();
    });

    $('.recette').click(function() {
        appliquerRecette(this);
    })


    $('.bouton_invitation').click(function(event) {
        if ($(this).hasClass("desactive")) return;
        appliquerRecette(event.target);
        fermerPanneau();
    })

    $('.condition').click(actualiserActivationSoumission);
    $('.condition').click(actualiseDropdownsIngredients);
    $('#stock').click(function() { window.location = "hotel-coffre.php" });

    $("form").on("click", "#sub", soumettreCreation);
}

function fermerPanneau() {
    $(".panneau-lateral#equipement").removeClass("ouvert");
    $(".colonneCentrale").css("filter", "none");
    $(".ombre").removeClass("montree");
    $(".ombre").addClass("cachee");
    $("#stats-joueur").addClass("transparent");
}

function appliquerRecette(option) {
    let ingredientsIds = $(option).next(".ingredients_id").text().split(" ").map(x => parseInt(x));
    let ingredients = Ingredient.getIngredientsByIds(ingredientsIds);

    for (let i = 1; i <= 3; i++) {
        var selectobject = document.getElementById("ingredient" + i);
        for (var j = 0; j < selectobject.length; j++) {
            if (selectobject.options[j].value == ingredients[i - 1].nom) {
                selectobject.options[j].selected = true;
            } else
                selectobject.options[j].selected = false;
        }

        /*pour les select custom*/
        var k = 0;
        $('.custom-select').each(function() {

            $(this).children('.select-items').children('.condition').each(function() {
                if (ingredients[k].nom.includes($(this).find(":first").html()))
                    itemSelected($(this)[0]);
            });

            k++;
        });

    }
    actualiserActivationSoumission();
}

function populerRecettes() {
    //Get recettes connues par le joueur
    //ajouter une ligne pour chacune
    let dataRecettes = []
    var ings;
    var estPossible;
    var ingredientTrouve;
    var ingredientsEnStock;

    for (let r of recettes) {
        if (r == null) continue;

        ings = Ingredient.getIngredientsByIds(r.ingredients);
        estPossible = true;
        ingredientsEnStock = ["en-stock", "en-stock", "en-stock"];
        for (var i = 0; i < ings.length; i++) {
            ingredientTrouve = false;
            for (var j = 0; j < ingredientsDisponibles.length; j++) {
                if (ingredientsDisponibles[j].id == ings[i].id) ingredientTrouve = true;
            }

            if (!ingredientTrouve) {
                estPossible = false;
                ingredientsEnStock[i] = "en-rupture";

            }
        }

        var active
        if (estPossible) active = "active";
        else active = "desactive";
        $("ul.list").append(
            '<li><span class="' +
            ingredientsEnStock[0] +
            '">' +
            ings[0].nom +
            '</span><span>, </span><span class="' +
            ingredientsEnStock[1] +
            '">' +
            ings[1].nom +
            '</span><span> et </span><span class="' +
            ingredientsEnStock[2] +
            '">' +
            ings[2].nom +
            '</span><span> sont amies</span><br><span>elles confectionnent : ' +
            r.nom +
            '<br></span><span class="objet"></span><input type="button" class="bouton_invitation ' +
            active + '" value="INVITER"/><div class="ingredients_id"  style="display:none;">' +
            ings[0].id.toString() +
            ' ' +
            ings[1].id.toString() +
            ' ' +
            ings[2].id.toString() +
            '</div></li>');
        /*pour options de filtrage avec List.js
         dataRecettes.push({
            objet:r.nom, 
            ingredient01:ings[0].nom,
            ingredient02:ings[1].nom,
            ingredient03:ings[2].nom,
            ingredient01id:ings[0].id.toString(),
            ingredient02id:ings[1].id.toString(),
            ingredient03id:ings[2].id.toString()
             })*/
    }

    /* pour options de filtrage avec List.js
    var options = {
           valueNames: [ 'ingredient01', 'ingredient02','ingredient03','ingredient01id', 'ingredient02id','ingredient03id', 'objet' ], 
           item: '<li><span class="ingredient01 "></span><span>, </span><span class="ingredient02 "></span><span> et </span><span class="ingredient03"></span><span> sont amies</span><br><span>elles confectionnent : </span><span class="objet"></span><input type="button" class="bouton_invitation" value="INVITER"/><div class="ingredients_id"  style="display:none;"><span class="ingredient01id"></span><span class="ingredient02id"></span><span class="ingredient03id"></span></div></li>'
         };

         var ingredientsListe = new List('recettes', options,dataRecettes); */

    if (!ingredientsInsuffisants)
        makeCustomSelect();
    actualiseDropdownsIngredients();
}

function ingredientsRecettePossedes(ingIds) {
    let oui = true;
    for (let id of ingIds) {
        if (!ingredientsDisponibles.find(x => x.id == id)) oui = false;
    }

    return oui;
}

function getStock() {
    // ingredientsExistants = Hotelcoffre.getHotelcoffre();

    // if(ingredientsExistants.length == 0) return;
    // ingredientsExistants = Ingredient.getIngredientsByIds(ingredientsExistants.map(x => x.id));
    // ingredientsDisponibles = ingredientsExistants.filter(x => x.nom != "");

    //Ingredients pour craft proviennent de l'inventaire du joueur

    if (Constants.debug) {
        let ings = Ingredient.getIngredientsNommesAleatoires(50);
        aventurier.coffre = aventurier.coffre.concat(ings);
        aventurier.sauvegarde()
    }

    aventurier.inventaire.forEach(x => x.parent = aventurier.inventaire)
    aventurier.coffre.forEach(x => x.parent = aventurier.coffre)
    ingredientsDisponibles = aventurier.inventaire.concat(aventurier.coffre);
    ingredientsDisponibles.sort(function(a, b) {
        let textA = a.nom.toUpperCase();
        let textB = b.nom.toUpperCase();
        return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });

    function onlyUnique(value, index, self) {
        return self.indexOf(value) === index;
    }

    actualiserActivationSoumission();
}

function actualiseDropdownsIngredients() {
    // debugger;
    let regnesSelectionnes = [];
    let ingredientsSelectionnes = [];

    // Noter les ingrédients sélectionnés
    for (var i = 1; i <= 3; i++) {
        var selectobject = document.getElementById("ingredient" + i);
        let optionSelectionnee = [].slice.call(selectobject.options).find(x => x.selected);
        ingredientsSelectionnes.push(optionSelectionnee.value);
    }

    /*for (let i = 1; i <= 3; i++) {
        $('.select-items> div').each(function() {
            $(this).removeClass("desactive");
            for (let j = 0; j < selectobject.options.length; j++) {
                console.log(ingredientsSelectionnes.includes($(this).find(':first').html()))
                if (ingredientsSelectionnes.includes($(this).find(':first').html()))
                    $(this).addClass("desactive");
            }
        });
    }
    */
}

function actualiserActivationSoumission() {
    let categorie = Objet.categorie(getIngredientsSelectionnes().map(x => x.iRegne));
    /* $('#produit').html(categorie);*/

    return;
}


function soumettreCreation() {
    var ingredientsSelectionnes = getNomsIngredientsSelectionnes();
    let erreur = false;
    let div = jQuery("<div>");
    // let categorie = getBoutonRadioActif("categorie");

    let errorButton = jQuery("<input>", {
        type: "button",
        class: "interaction",
        id: "fermerPopup",
        value: textes.error_buttons,
        click: fermerPopUp
    });

    closeAllSelect();

    ingredientsSelectionnesFull = getIngredientsSelectionnes();

    if (!uniciteIngredients(ingredientsSelectionnesFull)) {
        div.html(textes.error_ingredient).append(errorButton); //'<div>Vous ne pouvez pas sélectionner le même ingrédient plusieurs fois !</div>'
        ouvrirPopUp(div)
        return false;
    }

    if (!uniciteRegnes(ingredientsSelectionnesFull)) {
        div.html(textes.error_ingredients_regnes).append(errorButton); //'<div>Vous ne pouvez pas sélectionner le même ingrédient plusieurs fois !</div>'
        ouvrirPopUp(div)
        return false;
    }

    if (!descriptionValide()) {
        div.html(textes.error_description).append(errorButton); //'<div>Vous ne pouvez pas sélectionner le même ingrédient plusieurs fois !</div>'
        ouvrirPopUp(div)
        return false;
    }

    /*if (getBoutonRadioActif("categorie") == null) {
        div.html(textes.error_type).append(errorButton); //'<div>Vous ne pouvez pas sélectionner le même ingrédient plusieurs fois !</div>'
        ouvrirPopUp(div)
        return false;
    }*/
    /*
        if (!aventurier.equipementCheck(null)) {
            div.html(textes.inventory_full).append(errorButton); //'<div>Vous ne pouvez pas sélectionner le même ingrédient plusieurs fois !</div>'
            ouvrirPopUp(div)
            return false;
        }
    */

    /* if (!aventurier.equipementCheck(Objet.categorie(ingredientsSelectionnesFull.map(x => x.iRegne)))) {
         div.html(textes.inventory_taken).append(errorButton); //'<div>Vous ne pouvez pas sélectionner le même ingrédient plusieurs fois !</div>'
         ouvrirPopUp(div)
         return false;
     }*/

    let jDiv = creerObjet(ingredientsSelectionnesFull /*, getDescription(nouvelObjet.categorie)*/ );


    //Suppression dans la réserve de l'hotel des ingrédients utilisés
    // Hotelcoffre.destockerIngredients(ingredientsDisponibles.filter(x => ingredientsSelectionnes.includes(x.nom)));

    //Suppression depuis l'inventaire du joueur
    // if (Constants.debug == false) {
    for (let ing of ingredientsSelectionnes) {
        var index = aventurier.inventaire.findIndex(x => x.nom == ing);
        if (index < 0) {
            aventurier.coffre.findIndex(x => x.nom == ing);
            aventurier.coffre.splice(index, 1);
        } else {
            aventurier.inventaire.splice(index, 1);
        }
    }
    // }

    //Ajoute de l'objet créé à l'équipemetn du joueur         
    if (nouvelObjet != null) {
        if (!aventurier.equipementCheck(nouvelObjet.categorie)) {
            aventurier.vestiaire.push(nouvelObjet);
            jDiv.append("<br>").append(textes.new_object_sent_to_dressing);
        } else {
            aventurier.equipement.push(nouvelObjet);
        }
    }

    ouvrirPopUp(jDiv);


    aventurier.sauvegarde();
}

function getDescription(_categorie, _matiere) {
    // console.log($("#description").val())
    let champ = $("#description").val();
    if (champ)
        return champ
    let def = creerDescription(_categorie, _matiere);
    return def;
}

function creerDescription(_categorie, _matiere) {
    let genre, singulier, accord, phrase, qualificatifs, rand;

    singulier = true;
    genre = 0;
    if (_categorie == "Lunettes" || _categorie == "Chaussettes" || _categorie == "Boucle d’oreille" || _categorie == "Gants")
        singulier = false;

    if (_categorie == "Bracelet" || _categorie == "Collier" || _categorie == "Foulard" || _categorie == "Gants" || _categorie == "Chapeau")
        genre = 1;



    if (singulier && genre == 1) accord = 0;
    if (!singulier && genre == 1) accord = 1;
    if (singulier && genre == 0) accord = 2;
    if (!singulier && genre == 0) accord = 3;

    phrase = "";
    phrase += textesDescription.demonstratifs[accord];
    phrase += " " + textesDescription.qualificatifs_un[[Math.floor(Math.random() * textesDescription.qualificatifs_un.length)]][accord];
    phrase += " " + _categorie.toLowerCase();
    _matiere = _matiere.substring(_matiere.lastIndexOf(" "), _matiere.length);
    phrase += " " + textesDescription.connecteur_un + _matiere;

    let max = Math.floor(Math.random() * 4);
    qualificatifs = textesDescription.qualificatifs_aspect.slice(0);

    for (var i = 0; i < max; i++) {
        if (max > 1 && i > 0 && i < max - 1)
            phrase += textesDescription.connecteur_deux;
        else if (max > 1 && i > 0 && i == max - 1)
            phrase += textesDescription.connecteur_trois;
        else
            phrase += " ";
        rand = Math.floor(Math.random() * qualificatifs.length)
        phrase += qualificatifs[[rand]][accord];
        qualificatifs.splice(rand, 1);
    }

    //bloc motifs
    max = Math.floor(Math.random() * 3);
    if (max > 1) {
        phrase += textesDescription.connecteur_deux;
        phrase += textesDescription.connecteur_motifs;
        phrase += " " + textesDescription.qualificatifs_motif[Math.floor(Math.random() * textesDescription.qualificatifs_motif.length)];

    }

    //bloc comparaison
    max = Math.floor(Math.random() * 3);
    if (max > 0) {
        phrase += textesDescription.connecteur_deux;
        phrase += textesDescription.qualificatifs_emotion[[Math.floor(Math.random() * textesDescription.qualificatifs_emotion.length)]][accord];
        phrase += " " + textesDescription.connecteur_quatre;
        phrase += " " + textesDescription.qualificatifs_texture_deux[Math.floor(Math.random() * textesDescription.qualificatifs_texture_deux.length)];

    }

    phrase += "."

    return phrase;


}


function populerCategories() {
    //var checked = true;

    for (var categorie of Objet.Categories) {
        var listeCategories = document.getElementById("categorie");
        var s = "<input type=\"radio\" name=\"categorie\" value=\"" + categorie + "\" id=\"" + categorie + "\"";
        /*if(checked) {
          s += " checked";
          checked = false;
        }*/
        s += "> <label for=\"" + categorie + "\">" + categorie + "</label>";
        var radio = document.createElement("div");
        radio.innerHTML = s;
        listeCategories.appendChild(radio);
    }
}

function getNomsIngredientsSelectionnes() {
    var ingSel = [];
    for (var i = 1; i <= 3; i++) {
        ingSel.push(document.getElementById("ingredient" + i).value);
    }

    return ingSel;
}

function getIngredientsSelectionnes() {
    var ingSel = getNomsIngredientsSelectionnes();

    return Ingredient.getIngredientsByNames(ingSel);
}

function getBoutonRadioActif(nomDuGroupe) {
    var radios = document.getElementsByName(nomDuGroupe);
    for (var r of radios)
        if (r.checked) return r.value;
    return null;
}

function uniciteIngredients(ingredients) {
    var interdits = [];
    for (var ing of ingredients) {
        if (interdits.includes(ing.id)) return false;
        else interdits.push(ing.id);
    }
    return true;
}

function uniciteRegnes(ingredients) {
    var interdits = [];
    for (var ing of ingredients) {
        if (interdits.includes(ing.iRegne)) return false;
        else interdits.push(ing.iRegne);
    }
    return true;
}

function remplirDropdowns() {

    function onlyUnique(ing, index, self) {
        return self.findIndex(x => x.id == ing.id) === index;
    }

    let listUniques = ingredientsDisponibles.filter(onlyUnique);

    for (var i = 1; i <= 3; i++) {
        for (var ing of listUniques) {
            let option = jQuery("<option/>", {
                disabled: ing.quantite <= 0,
                text: ing.nom,
                value: ing.nom,
                class: Ingredient.Regnes[ing.iRegne].nom,
            });
            $("#ingredient" + i).append(option);
        }
    }
}

//selectionner ingrédient par défaut
function defautDropdowns() {
    if (ingredientsDisponibles.length < 3) return;
    var j = 0;
    for (var i = 1; i <= 3; i++) {
        let childrenOfDropdown = document.getElementById("ingredient" + i).children;
        while (j < childrenOfDropdown.length - 1 && childrenOfDropdown[j].disabled) {
            j += 1;
        }
        childrenOfDropdown[j].selected = "true";
        if (j < childrenOfDropdown.length - 1) j += 1;
    }
}


function creerObjet(ingredients) {
    //Requete mysql : voir si un objet avec ces ingrédients a déjà été créé, et le récupérer si c'est le cas
    //Sinon new Objet
    let div = "";
    let gainTemps = 1;
    let jDiv;



    var objet = getObjet(ingredients.map(x => x.id));

    if (objet == null) {
        nouvelObjet = new Objet(ingredients, "");
        nouvelObjet.description = getDescription(nouvelObjet.categorie, nouvelObjet.nom);
        nouvelObjet.pouvoirs = Pouvoirs.aucun();
        nouvelObjet.stats = Stats.nul();
        // Objet.ajouteObjet(nouvelObjet, aventurier);


        // gainTemps = Constants.gainTempsCreaObjetNouveau;

        if (aventurier.quetes.crafter == QueteFlag.STARTED)
            aventurier.quetes.crafter = QueteFlag.IN_PROGRESS;

        div += textes.create_new_useless + "</br></br>" + nouvelObjet.description + "</br></br></p>";
        /* div += "cette recette ne mène à rien" + "</p>";*/

        jDiv = jQuery("<div>").html(div);
    } else {
        nouvelObjet = objet;
        nouvelObjet.description = getDescription(nouvelObjet.categorie, nouvelObjet.nom);

        if (aventurier.quetes.crafter == QueteFlag.STARTED || aventurier.quetes.crafter == QueteFlag.IN_PROGRESS)
            aventurier.quetes.crafter = QueteFlag.FINISHED;

        let txt = "";

        if (!aventurier.recetteConnue(nouvelObjet.id)) {
            aventurier.apprendRecette(nouvelObjet.id);
            //update recettes
            populerRecettes();

            txt = textes.create_existing;
        } else {
            txt = textes.create_new;
        }

        div += txt + nouvelObjet.description + "</p>";

        gainTemps = Constants.gainTempsCreaObjetConnu;
        jDiv = jQuery("<div>").html(div).append("Propriétés : ").append(afficherPouvoirsEtStats(nouvelObjet.stats, nouvelObjet.pouvoirs, false))
    }

    aventurier.sauvegarde();

    let button = jQuery("<input>", {
        type: "button",
        value: "Créer un autre objet",
        click: reset
    }).appendTo(jDiv);
    jDiv.append("<br/><br/><a href='hub.php'>Retourner dans le grand hall</a>")



    return jDiv;
}

function reset() {
    fermerPopUp();
    nouvelObjet = null;
}

function getObjet(recette) {
    var objet = Objet.getObjetByRecette(recette);
    return objet;
}

function getTextesDescription() {
    let response = $.ajax({
        url: 'medias/objets.json',
        type: 'get',
        dataType: 'json',
        error: function(data) {
        },
        success: function(data) {
            textesDescription = data;
        },
        async: true
    });
/*
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == 4 && (xmlhttp.status == 200 || xmlhttp.status == 0)) {
            var obj = JSON.parse(this.responseText);
            textesDescription = obj;
            // console.log(textesDescription)
        }
    };
    xmlhttp.open("GET", "medias/objets.json", false);
    xmlhttp.send(null);*/
}

function parseTextes() {
    $("#title").html(textes.title);
    $("#order").html(textes.order);
    $("#la_question").html(textes.order03);
    // $("#la_question_2").html(textes.order02);
    $("#bouton_guestlist").val(textes.order02);
}