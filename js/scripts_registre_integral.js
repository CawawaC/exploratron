
let ingredientsNommes = Ingredient.getIngredientsTous();
var ingredientsListe
$(document).ready(function() {
    getTextes("registre", parseTextesRegistre);

    let data = []

    for(let ing of ingredientsNommes) {
        

    	
        data.push({
            nom:ing.nom, 
            regne:Ingredient.Regnes[ing.iRegne].nom,
            description:ing.description,
            createur:ing.joueurCreateur,
            nommeur:ing.joueurNommeur,
            region:ing.region
             })
    }

  console.log(data)
 /* var options = {
  valueNames: [ { data: ['timestamp'] }, { data: ['status'] }, 'jSortNumber', 'jSortName', 'jSortTotal' ],
  page: 6,
  pagination: {
    innerWindow: 1,
    left: 0,
    right: 0,
    paginationClass: "pagination",
    }
};*/
var options = {
  valueNames: [ 'nom', 'regne', 'description','createur', 'nommeur','region' ], item: '<li><span class="nom"></span><span> - </span><span class="regne"></span> : </span><span class="description"></span><br/><span>Décrite par </span><span class="createur"></span><span>, nommée par </span><span class="nommeur"></span><span> dans la suite </span><span class="region"></span></li>'
};

ingredientsListe = new List('tableIngredients', options,data); 
});

$('#filtreTous').on('click',function(){filtre();});
$('#filtreOnirique').on('click',function(){filtre('onirique');});
$('#filtreUtile').on('click',function(){filtre('utile');});
$('#filtreEgaree').on('click',function(){filtre('égarée');});
$('#filtreBotanique').on('click',function(){filtre('botanique');});
$('#filtrePrécieux').on('click',function(){filtre('précieux');});

function filtre(nomFiltre){
   
    if(nomFiltre!=null){
        ingredientsListe.filter(function(item) {
        if (item.values().regne ==nomFiltre) {
           return true;
        } else {
           return false;
        }
        });
    }

    else
         ingredientsListe.filter();
}

function parseTextesRegistre() {

}