class Objet {
    constructor(ingredients, description, generer = true) {
        this.description = description
        this.ingredients = ingredients;

        if(generer) {
            this.categorie = Objet.categorie(ingredients.map(x => x.iRegne));

            // console.log(ingredients.map(x => x.stats))
            this.stats = Stats.average(ingredients.map(x => x.stats));
            this.stats.volonte = Constants.objetVolonte(ingredients.map(x => x.stats.volonte));
            this.stats.moral = Constants.objetVolonte(ingredients.map(x => x.stats.moral));

            this.nom = this.generationNomInitialesClassementId(ingredients, this.categorie);
            this.id = -1;
            this.elegance = Constants.objetElegance(ingredients.map(x => x.stats.volonte));

            function getSum(total, num) {
                return total + num;
            }

            if (ingredients.length > 0) {
                let magnitudeMoyenne = ingredients.map(e => parseInt(e.magnitude)).reduce(getSum) / ingredients.length;
                this.pouvoirs = new Pouvoirs(magnitudeMoyenne);
            } else this.pouvoirs = new Pouvoirs(0);
        }
    }
    /*

        generationNom(ingredients, categorie) {
            let nom = categorie + " en ";
            let nomsIngredients = ingredients.map(x => x.nom.toLowerCase());
            nomsIngredients = nomsIngredients.sort((a, b) => a.localeCompare(b));
            for(let i = 0 ; i < nomsIngredients.length ; i++) {
                nom += nomsIngredients[i].slice(i, i+3);
            }
            return nom;
        }

        generationNomInitiales(ingredients, categorie) {
            let nom = categorie + " en ";
            let nomsIngredients = ingredients.map(x => x.nom.toLowerCase());
            nomsIngredients = nomsIngredients.sort((a, b) => a.localeCompare(b));
            for(let i = 0 ; i < nomsIngredients.length ; i++) {
                nom += nomsIngredients[i].slice(0, 3);
            }
            return nom;
        }
    */

    generationNomInitialesClassementId(ingredients, categorie) {
        let nom = categorie + " en ";
        let idSort = ingredients.sort((a, b) => parseInt(a.id) < parseInt(b.id) ? -1 : 1);
        let nomsIngredients = idSort.map(x => x.nom.toLowerCase());
        for (let i = 0; i < nomsIngredients.length; i++) {
            nom += nomsIngredients[i].split(" ").join().slice(0, 3).replace(/\s/g,'');
        }
        return nom;
    }

    static getObjetsByIds(ids, detailsIngredients) {
        if (ids.length === 0)
            return [];

        ids = ids.map(x => parseInt(x));
        var response = $.ajax({
            url: 'gettersBDD/getObjetsByIds.php',
            type: 'GET',
            async: false,
            data: {
                ids: ids
            },
            dataType: 'html'
        }).responseText;

        var datasObjets = JSON.parse(response); //Aller chercher les bons objets dans la bdd objets 


        var objets = [];
        for (var dataO of datasObjets) {
            var objet = Objet.objetDepuisLigneBDD(dataO, detailsIngredients);
            objets.push(objet);
        }

        return objets;
    }

    static ajouteObjet(nouvelObjet, aventurier) {
        var idsIngredients = nouvelObjet.ingredients.map(e => e.id).sort((a, b) => a < b ? -1 : 1).map(x => parseInt(x));

        console.log("Ajoute objet BDD", idsIngredients);
        var response = $.ajax({
            url: 'settersBDD/ajouteObjet.php',
            type: 'GET',
            data: {
                ingredients: idsIngredients,
                categorie: nouvelObjet.categorie,
                description: nouvelObjet.description,
                stats: nouvelObjet.stats,
                nom: nouvelObjet.nom,
                id: nouvelObjet.id,
                pouvoirs: nouvelObjet.pouvoirs
            },
            async: true,
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
                nouvelObjet.id = parseInt(code_html);
                if (statut === 'success')
                    console.log("Votre objet a bien été crée");
                else
                    console.log("Echec de la création de l'objet");
                if (aventurier != null)
                    aventurier.sauvegarde();
            },
            dataType: 'html'
        }).responseText;
    }

    /*
        Recette : array d'ids des ingrédients
    */
    static getObjetByRecette(recette, categorie) {
        recette = recette.map(x => parseInt(x)).sort((a, b) => a < b ? -1 : 1);

        var response = $.ajax({
            url: 'gettersBDD/getObjetByRecette.php',
            type: 'GET',
            async: false,
            data: {
                recette: recette
            },
            dataType: 'html'
        }).responseText;

        var dataObjet = JSON.parse(response); //Aller chercher les bons objets dans la bdd objets 
        if (dataObjet === false) return null;

        var objet = Objet.objetDepuisLigneBDD(dataObjet, true);

        return objet;
    }

    static objetDepuisLigneBDD(dataO, detailsIngredients) {
        if (dataO == false) return null;
        // = bel objet Ingredient bien formé
   /*     $.ajax({
            url: 'gettersBDD/getIngredientById',
            type: 'GET',
            async: false,
            data: {
                ids: dataO.ingredients
            }
        });*/


        if(dataO.ingredients == null) dataO.ingredients = [];
        if(dataO.ingredient1 != null) {
            dataO.ingredients.push(dataO.ingredient1);
            dataO.ingredients.push(dataO.ingredient2);
            dataO.ingredients.push(dataO.ingredient3);     
        }
        dataO.ingredients = dataO.ingredients.map(x => parseInt(x));

        var ingredients = [];
        if (detailsIngredients)
            ingredients = Ingredient.getIngredientsByIds(dataO.ingredients);

        if(dataO.description == null) dataO.description = ""

        var objet = new Objet(
            ingredients,
            dataO.description, false);

        objet.categorie = dataO.categorie;

        if (!detailsIngredients)
            objet.ingredients = dataO.ingredients;

        if(dataO.stats == "")
            objet.stats = Stats.nul();
        else if(typeof dataO.stats == "string")
            objet.stats = Stats.depuisLigneBDD(JSON.parse(dataO.stats));
        else
            objet.stats = Stats.depuisLigneBDD(dataO.stats)

        objet.nom = dataO.nom;
        objet.id = parseInt(dataO.id);

        if (dataO.pouvoirs === "")
            objet.pouvoirs = Pouvoirs.aucun();
        else if(typeof dataO.pouvoirs == "string")
            objet.pouvoirs = Pouvoirs.fromBDD(JSON.parse(dataO.pouvoirs));
        else
            objet.pouvoirs = Pouvoirs.fromBDD(dataO.stats)
            // objet.pouvoirs = Pouvoirs.fromBDD(JSON.parse(dataO.pouvoirs));

        return objet;
    }

    /*
        Détermine la catégorie d'objet produite par cette combinaison de regnes
        regnes: array d'int
    */
    static categorie(regnes) {
        let categorie = "";

        if (regnes.includes(0) && regnes.includes(1) && regnes.includes(2)) //botanique égarée onirique : foulard
            categorie = "Foulard";
        if (regnes.includes(0) && regnes.includes(1) && regnes.includes(3)) //botanique égarée précieux : gants
            categorie = "Gants";
        if (regnes.includes(0) && regnes.includes(1) && regnes.includes(4)) //botanique égarée utile : lunettes
            categorie = "Lunettes";
        if (regnes.includes(0) && regnes.includes(2) && regnes.includes(3)) //botanique onirique précieux : chaussettes
            categorie = "Chaussettes";
        if (regnes.includes(0) && regnes.includes(2) && regnes.includes(4)) //botanique onirique utile : chapeau
            categorie = "Chapeau";
        if (regnes.includes(0) && regnes.includes(3) && regnes.includes(4)) //botanique précieux utile : bracelet
            categorie = "Bracelet";
        if (regnes.includes(1) && regnes.includes(2) && regnes.includes(3)) //égarée onirique préceixu : boucle orielle
            categorie = "Boucle d’oreille";
        if (regnes.includes(1) && regnes.includes(2) && regnes.includes(4)) //égarée onirique utile : cape
            categorie = "Cape";
        if (regnes.includes(1) && regnes.includes(3) && regnes.includes(4)) //égarée préceixu utile : bague
            categorie = "Bague";
        if (regnes.includes(2) && regnes.includes(3) && regnes.includes(4)) //onirique préceixu utile : collier
            categorie = "Collier";

        return categorie
    }

    static testCrea() {
        let n = 1;
        let obj = Objet.exNihilo(n);
        console.log(obj[0].pouvoirs);
        return obj;
    }

    /*
        Créer un nouvel objet. ATTENTION : objet aléatoire, pas issu de la liste des objets existants.
    */
    static exNihilo(nombre, existant) {
        let objets = [];

        for (let i = 0; i < nombre; i++) {
            let ingredients = Ingredient.getIngredientsNommesAleatoires(3);
            let objetAleatoire = new Objet(ingredients);
            objets.push(objetAleatoire);
        }

        return objets;
    }

    /*
        Renvoie [nombre] objets choisis aléatoirement parmi les objets existants
    */
    static getObjetsRandom(nombre) {
        var response = $.ajax({
            data: {
                nombre: nombre
            },
            type: "GET",
            url: "gettersBDD/objets/getObjetsRandom.php",
            async: false
        });

        var data = response.responseText;
        var dataObjets = JSON.parse(data);

        var objets = [];
        for (var dataObj of dataObjets) {
            if (dataObj == false) continue;
            let obj = Objet.objetDepuisLigneBDD(dataObj, true);
            objets[objets.length] = obj;
        }

        console.log(objets);

        return objets;
    }

    /*
        Donne une liste d'objets dont la recette inclut au moins 1 des ingrédients fournis en paramètre
        Peut servir à déterminer les objets dans le craft desquels les ingrédients d'une région sont impliqués
    */
    static getObjetsByIngredients(ingredients, detailsIngredients) {
        var response = $.ajax({
            data: {
                ingredients: ingredients.map(x => x.id)
            },
            type: "GET",
            url: "gettersBDD/objets/getObjetsByIngredients.php",
            async: false
        });

        var data = response.responseText;
        var dataObjets = JSON.parse(data);

        var objets = [];
        for (var dataObj of dataObjets) {
            if (dataObj == false) continue;
            let obj = Objet.objetDepuisLigneBDD(dataObj, true);
            objets[objets.length] = obj;
        }

        //Filtrer pour ne garder que les objets dont les ingrédients sont tous nommés
        objets = objets.filter(x => !x.ingredients.find(y => y.nom == ""));

        return objets;
    }

    /*
        Renommer objets qui  utilisent un ingrédient spécifique
        Utile quand un ingrédient vient d'être nommé
    */
    static updateNomsObjets(ingredient) {
        //Get objets utilisant ingredient
        let objets = Objet.getObjetsByIngredients([ingredient], true);

        //Modifier leur noms
        for (let o of objets) {
            o.nom = o.generationNomInitialesClassementId(o.ingredients, o.categorie);
        }

        //Update objets
        $.ajax({
            data: {
                ids: objets.map(x => x.id),
                noms: objets.map(x => x.nom)
            },
            type: "GET",
            url: "settersBDD/objets/updateNomsObjets.php",
            async: false,
            complete: function(code_html, statut) { // code_html contient le HTML renvoyé
                $("body").append(code_html);
            }
        });
    }

    /*
        Donne les ingrédients d'un objet aléatoire et qui utilise au moins 1 des ingrédients passés en paramètre
    */
    static recetteAleatoire(ingredients, exclusions) {
        //Choisir objet au hasard mais qui utilise l'un des ingrédients
        let candidats = Objet.getObjetsByIngredients(ingredients, false);
        console.log(candidats.map(x => x.id), exclusions);
        if (candidats.length < 1) return null;


        candidats = candidats.filter(x => exclusions.indexOf(x.id) < 0);
        // console.log(candidats);
        if (candidats.length < 1) return null;

        let randomObjet = candidats[random(candidats.length)];

        return randomObjet;
    }

    /*
        Vérifier que toutes les recettes d'objets utilisent des ingrédients qui existent
    */
    /*static cleanRecettes() {

    }*/

    /*
        Create object recipes 
        !!! DEBUG or DB update thing
    */
    static retroCreationRecettes() {
        let ingredients = Ingredient.getIngredientsTous();

        for (let max = 3; max < ingredients.length; max++) {

            let arr = [];
            while (arr.length < 3) {
                var r = random(max);
                if (arr.indexOf(r) === -1) arr.push(r);
            }

            let recette = [];
            for (let i of arr) {
                recette.push(ingredients[i]);
            }

            let objet = new Objet(recette);
            console.log(objet);
            Objet.ajouteObjet(objet);
        }
    }

    static getPremiereRecette() {
        var response = $.ajax({
            type: "GET",
            url: "gettersBDD/objets/getObjetPremier.php",
            async: false
        });

        var data = response.responseText;
        var dataObjets = JSON.parse(data);

        let obj = Objet.objetDepuisLigneBDD(dataObjets, true);

        return obj;
    }
}

Objet.Categories = [
    "Bracelet",
    "Bague",
    "Collier",
    "Lunettes",
    "Chaussettes",
    "Boucle d’oreille",
    "Foulard",
    "Gants",
    "Chapeau",
    "Cape"
];

function afficherPouvoirsEtStats(stats, pouvoirs, afficherTailleDuSac = true) {
    let div = jQuery("<div>")

    tailleDuSac = aventurier.getLesStatsTotales().sac;

    div.append("Temps : ").append(stats.moral).append("<br>");
    div.append("Zir : ").append(stats.volonte).append("<br>");
    if (afficherTailleDuSac) 
        div.append("Taille du sac : ").append(tailleDuSac).append("<br>");

    for(let s of pouvoirs.toString(false)) {
        div.append(s).append("<br>");
    }

    // if (pouvoirs != null)
    //     pouvoirs.afficher(div);

    return div;
}