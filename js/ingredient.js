var nouvelIngredient;
var regne;
var genre;
var description;


class Ingredient {
    constructor(_id, regne, genre, description, region, magnitude, _agressif) {
        this.id = _id;
        this.nom = "inconnu";
        this.iRegne = regne; //Index du regne. Voir Ingredient.Regnes
        this.genre = genre; //Garçon, fille
        this.description = description;
        this.region = region; //voronoiID de la cell
        this.chant = "";
        this.agressif = _agressif;

        this.interactionConsommation = Ingredient.Regnes[this.iRegne].interactionConsommation;
        this.interactions = [Ingredient.Regnes[this.iRegne].interactionAcquerir];

        this.stats = new Stats(
            Constants.ingredientVolonte(magnitude),
            0,
            Constants.ingredientVolonte(magnitude)
        );

        this.consommation = Constants.ingredientConsommation(this.stats);

        this.stats_combat = [];
        this.magnitude = magnitude;
    }

    tweet(createur) {
        console.log("tweet");
        var contenu = '{"tweet":"' + createur + " a créé un " + Ingredient.Renges[this.iRegne].nom + " de genre " + this.genre + " : " + this.description + '"}';

        $.ajax({
            type: 'POST',
            // url: 'http://127.0.0.1:8080/',
            url: 'https://exploratron.herokuapp.com/test',
            data: JSON.parse(contenu),
            dataType: 'json',
            success: function(data) {
                console.log("success");
                console.log(data);
            },
            /* error: function (jqXHR, textStatus, errorThrown) {
                 alert('error ' + textStatus + " " + errorThrown);
             }*/
        });
    }


    /*
        defineInteractionConsommation() {
            switch(this.regne) {
                case 0:
                this.interactionConsommation = "manger";
                break;

                case 1:
                this.interactionConsommation = "";
                break;

                case 2:
                this.interactionConsommation = "";
                break;

                case 3:
                this.interactionConsommation = "manger";
                break;

                case 4:
                this.interactionConsommation = "manger";
                break;
            }
        }*/

    static ajouteIngredient(nouvelIngredient, regionSelectionnee, nomAventurier) {
        console.log(nouvelIngredient);
        nouvelIngredient.region = regionSelectionnee;

        $.ajax({
            url: 'settersBDD/ajouteIngredient.php',
            type: 'GET',
            data: {
                pos: regionSelectionnee, //identifiant unique de la pièce
                genre: nouvelIngredient.genre, // coordonnée x de la pièce
                type: nouvelIngredient.iRegne, // coordonnée y de la pièce
                description: nouvelIngredient.description,
                stats: nouvelIngredient.stats,
                consommation: nouvelIngredient.consommation,
                createur: nomAventurier,
                agressif: nouvelIngredient.agressif,
                magnitude: nouvelIngredient.magnitude
            },
            dataType: 'html', // On désire recevoir du HTML
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
                $("body").append(code_html);
                console.log(code_html)
                nouvelIngredient.id = parseInt(code_html);
            },
            complete: function() {},
            async: false
        });



        //Création d'une recette contenant cet ingrédient
        let ingredients = [];
        ingredients[0] = nouvelIngredient;
        let picks = Ingredient.getIngredientsNommesAleatoires(2, false);
        ingredients[1] = picks[0];
        ingredients[2] = picks[1];

        console.log(ingredients.filter(x => x != null));
        if (ingredients.filter(x => x != null).length == ingredients.length) {
            let nouvelObjet = new Objet(ingredients);
            Objet.ajouteObjet(nouvelObjet);
        }
    }

    verrouillerEcriture() {
        $.ajax({
            url: 'settersBDD/verrouillerEcritureIngredient.php',
            type: 'GET',
            data: {
                ids: [this.id]
            },
            dataType: 'html', // On désire recevoir du HTML
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
            },
            complete: function() {},
            async: false
        });
    }

    static nombreIngredientsNommes() {
        var response = $.ajax({
            type: "GET",
            url: "gettersBDD/getNombreIngredientsNommes.php",
            async: false
        });

        var data = response.responseText;
        console.log(data);
        return data;
    }

    /*
        
    */
    static getIngredientsNommesAleatoires(n, regnesDifferents) {
        let response;
        if (regnesDifferents) {
            response = $.ajax({
                data: {
                    nombre: n
                },
                type: "GET",
                url: "gettersBDD/getIngredientsRandomRegnesDistincts.php",
                async: false
            });
        } else {
            response = $.ajax({
                data: {
                    nombre: n
                },
                type: "GET",
                url: "gettersBDD/getIngredientsRandom.php",
                async: false
            });
        }


        var data = response.responseText;
        var dataIngredients = JSON.parse(data);

        var ingredients = [];
        for (var dataIng of dataIngredients) {
            if (dataIng == false) continue;
            var ing = Ingredient.ingredientDepuisLigneBDD(dataIng);
            ingredients[ingredients.length] = ing;
        }

        while (ingredients.length > n)
            ingredients.pop();

        return ingredients;
    }

    static getIngredientsNommes() {
        var response = $.ajax({
            data: "",
            type: "GET",
            url: "gettersBDD/getIngredients.php",
            async: false
        });

        var data = response.responseText;
        var dataIngredients = JSON.parse(data);

        var ingredients = [];
        for (var dataIng of dataIngredients) {
            if (dataIng == false) continue;
            var ing = Ingredient.ingredientDepuisLigneBDD(dataIng);
            ingredients[ingredients.length] = ing;
        }

        return ingredients;
    }

    static getIngredientsTous() {
        var response = $.ajax({
            data: "",
            type: "GET",
            url: "gettersBDD/getIngredientsTous.php",
            async: false
        });

        var data = response.responseText;
        var dataIngredients = JSON.parse(data);

        var ingredients = [];
        for (var dataIng of dataIngredients) {
            if (dataIng == false) continue;
            var ing = Ingredient.ingredientDepuisLigneBDD(dataIng);
            ingredients[ingredients.length] = ing;
        }

        return ingredients;
    }

    static getIngredientsByNames(names) {
        var data = $.ajax({
            url: "gettersBDD/getIngredientByName.php",
            type: "GET",
            data: {
                ingredients: JSON.stringify(names)
            },
            dataType: 'html',
            async: false
        }).responseText;

        var dataIngredients = JSON.parse(data);

        var ingredients = [];
        for (var dataIng of dataIngredients) {
            if (dataIng == false) continue;
            var ing = Ingredient.ingredientDepuisLigneBDD(dataIng);
            ingredients[ingredients.length] = ing;
        }

        return ingredients;
    }

    static getIngredientsByIds(ids) {
        // ids = Array.from(ids);
        ids = ids.map(x => parseInt(x));

        var data = $.ajax({
            url: "gettersBDD/getIngredientsByIds.php",
            type: "POST",
            data: {
                ids: ids
            },
            dataType: 'html',
            async: false
        }).responseText;


        var dataIngredients = JSON.parse(data);

        var ingredients = [];
        for (var dataIng of dataIngredients) {
            if (dataIng == false) continue;
            var ing = Ingredient.ingredientDepuisLigneBDD(dataIng);
            ingredients[ingredients.length] = ing;
        }

        return ingredients;
    }

    static getIngredientsByRegion(region) {
        var data = $.ajax({
            url: "gettersBDD/getIngredientsByRegion.php",
            type: "GET",
            data: {
                region: region
            },
            dataType: 'html',
            async: false
        }).responseText;

        var dataIngredients = JSON.parse(data);

        var ingredients = [];
        for (var dataIng of dataIngredients) {
            if (dataIng == false) continue;
            var ing = Ingredient.ingredientDepuisLigneBDD(dataIng);
            ingredients[ingredients.length] = ing;
        }

        return ingredients;
    }

    static getIngredientsByJoueurCreateur(nomJoueur) {
        var data = $.ajax({
            url: "gettersBDD/ingredients/getIngredientsByJoueurCreateur.php",
            type: "GET",
            data: {
                joueurCreateur: nomJoueur
            },
            dataType: 'html',
            async: false
        }).responseText;

        var dataIngredients = JSON.parse(data);

        var ingredients = [];
        for (var dataIng of dataIngredients) {
            if (dataIng == false) continue;
            var ing = Ingredient.ingredientDepuisLigneBDD(dataIng);
            ingredients[ingredients.length] = ing;
        }

        return ingredients;
    }

    static getIngredientsByRegions(regions) {
        var data = $.ajax({
            url: "gettersBDD/getIngredientsByRegions.php",
            type: "POST",
            data: {
                regions: regions
            },
            dataType: 'html',
            async: false
        }).responseText;

        var dataIngredients = JSON.parse(data);

        var ingredients = [];
        for (var dataIng of dataIngredients) {
            if (dataIng == false) continue;
            var ing = Ingredient.ingredientDepuisLigneBDD(dataIng);
            ingredients[ingredients.length] = ing;
        }

        return ingredients;
    }

    static ingredientDepuisLigneBDD(dataBDD) {
        var ing = new Ingredient(
            parseInt(dataBDD.id),
            parseInt(dataBDD.type),
            dataBDD.genre,
            dataBDD.def,
            parseInt(dataBDD.place),
            0,
            parseInt(dataBDD.agressif));

        ing.nom = dataBDD.nom;
        // ing.description = dataBDD.def;
        // ing.region = dataBDD.place;
        // ing.genre = dataBDD.genre;
        // ing.regne = dataBDD.type;

        if (typeof(dataBDD.stats) === 'object')
            ing.stats = dataBDD.stats;
        else
            ing.stats = Stats.depuisLigneBDD(JSON.parse(dataBDD.stats));

        ing.consommation = dataBDD.consommation;
        if (ing.consommation != "") {
            if (typeof(dataBDD.stats) === 'object')
                ing.consommation = dataBDD.consommation;
            else {
                ing.consommation = Stats.depuisLigneBDD(JSON.parse(dataBDD.consommation));

                // ing.consommation.moral = parseFloat(ing.consommation.moral);
                // ing.consommation.sac = parseFloat(ing.consommation.sac);
                // ing.consommation.volonte = parseFloat(ing.consommation.volonte);
            }
        }

        ing.joueurCreateur = dataBDD.joueurCreateur;
        ing.joueurNommeur = dataBDD.joueurNommeur;

        if (typeof(dataBDD.stats_combat) == 'object')
            ing.stats_combat = dataBDD.stats_combat;
        else
            ing.stats_combat = JSON.parse(dataBDD.stats_combat);

        ing.interactionConsommation = Ingredient.Regnes[ing.iRegne].interactionConsommation;
        ing.interactions = [Ingredient.Regnes[ing.iRegne].interactionAcquerir];

        ing.magnitude = parseInt(dataBDD.magnitude);

        return ing;
    }

    static updateIngredientNom(nom, id, nommeur, ingredient) {
        $.ajax({
            url: 'settersBDD/setIngredient.php',
            type: 'POST',
            data: {
                nom: nom,
                id: id,
                nommeur: nommeur
            },
            dataType: 'html', // On désire recevoir du HTML
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
                // $( "body" ).append( code_html ); 
            },
            complete: function() {
                //TODO : update noms objets utilisant l'ingrédient
                Objet.updateNomsObjets(ingredient);
            }
        });

        //update les recettes utilisant cet ingrédient, pour que les objets correspondants soient renommés
    }

    /*
        retourne un tableau des 3 premierss miurges du monde. Utile pour la quete tuto.
    */
    static getPremiersIngredients(nombre) {
        var data = $.ajax({
            url: "gettersBDD/ingredients/getPremiersIngredients.php",
            type: "GET",
            data: {
                n: nombre
            },
            dataType: 'html',
            async: false
        }).responseText;

        var dataIngredients = JSON.parse(data);

        var ingredients = [];
        for (var dataIng of dataIngredients) {
            if (dataIng == false) continue;
            var ing = Ingredient.ingredientDepuisLigneBDD(dataIng);
            ingredients[ingredients.length] = ing;
        }

        return ingredients;
    }
}


Ingredient.Genres = [
    "garçon",
    "fille"
];

Ingredient.Regnes = [{
        nom: "botanique",
        nom_genre_0: "botanique",
        nom_genre_1: "botanique",
        pluriel: "botaniques",
        refString: "botanique",
        description: "Créature végétale",
        interactionConsommation: "Je me plante devant toi et je te cueille ! Beau tanique, belle végétale créature !",
        interactionAcquerir: "Je l’effleure et l’emporte avec moi",
        frequence: 0.3,
        combat: false,
        genre: 1,
        zirRequis: 10
    },
    {
        nom: "égarée",
        nom_genre_0: "égarée",
        nom_genre_1: "égaré",
        pluriel: "égarées",
        refString: "egaree",
        description: "Humain ou animal échappé d’un souvenir",
        interactionConsommation: "Je ==la--le== regarde et je m'imprègne de sa présence",
        interactionAcquerir: "chanter",
        frequence: 0.2,
        combat: true,
        genre: 1,
        zirRequis: 30,
        indicePieceSecrete: true
    },
    {
        nom: "onirique",
        nom_genre_0: "onirique",
        nom_genre_1: "onirique",
        pluriel: "oniriques",
        refString: "onirique",
        description: "Créature fantastique et imaginaire",
        interactionConsommation: "Je lui demande de me raconter ses aventures et je gobe tout, même les récits les plus extravagants.",
        interactionAcquerir: "chanter",
        frequence: 0.2,
        combat: true,
        indiceRecette: true,
        genre: 0,
        zirRequis: 50
    },
    {
        nom: "précieux",
        nom_genre_0: "précieuse",
        nom_genre_1: "précieux",
        pluriel: "précieux",
        refString: "precieux",
        description: "Objet inutile mais rare et très attachant",
        interactionConsommation: "je secoue ==la--le== _ingredient_ énergiquement",
        interactionAcquerir: "Une force irrésistible m’attire vers ==cette--ce== _ingredient_. Je ==la--le== ramasse et ==la--le== cache.",
        frequence: 0.12,
        combat: false,
        genre: 1,
        zirRequis: 40
    },
    {
        nom: "utile",
        nom_genre_0: "utile",
        nom_genre_1: "utile",
        pluriel: "utiles",
        refString: "utile",
        description: "Meubles, outils et matériels d’entretien",
        interactionConsommation: "J’utilise l’utile",
        interactionAcquerir: "Je ==la--le== ramasse et lui assure qu’==elle--il== servira",
        frequence: 0.22,
        combat: false,
        genre: 1,
        zirRequis: 20
    },
]