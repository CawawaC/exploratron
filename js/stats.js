class Stats {

    constructor(moral, sac, volonte) {
        this.moral = moral;
        this.sac = sac;
        this.volonte = volonte;
    }

    ajoute(stats) {
        return new Stats(
            this.moral + stats.moral,
            this.sac + stats.sac,
            this.volonte + stats.volonte
        );
    }

    scale(multiplier) {
        return new Stats(
            this.moral * multiplier,
            this.sac * multiplier,
            this.volonte * multiplier
        );
    }

    toStrings() {
        return [
            "Temps : " + this.moral,
            "Inventaire : " + this.sac,
            "Zir : " + this.volonte
        ];
    }

    toString() {
        let s = this.moral + "/" +
            this.sac + "/" +
            this.volonte
        return s;
    }

    static nul() {
        return new Stats(0, 0, 0);
    }

    static average(array) {
        var moral = 0;
        var sac = 0;
        var volonte = 0;

        for (var a of array) {
            moral = moral + parseInt(a.moral);
            sac = sac + parseInt(a.sac);
            volonte = volonte + parseInt(a.volonte);
        }

        // console.log(moral);
        // console.log(volonte);

        moral /= array.length;
        sac /= array.length;
        volonte /= array.length;

        moral = Math.round(moral);
        sac = Math.round(sac);
        volonte = Math.round(volonte);


        return new Stats(moral, sac, volonte);
    }

    /*
    Fournir un truc parsé par JSON.parse()
    */
    static depuisLigneBDD(ligne) {
        var stat = new Stats(
            parseInt(ligne.moral),
            parseInt(ligne.sac),
            parseInt(ligne.volonte)
        );

        return stat;
    }

    static depuisTableau(array) {
        var stat = new Stats(
            array[0],
            array[1],
            array[2]
        );
        return stat;
    }
}