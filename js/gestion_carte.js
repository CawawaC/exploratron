var carte = new Carte(Carte.Filtres.Aucun);
carte.init();
carte.initInterface();


$(document).ready(function() {
	console.log(carte)
	if(Constants.debug)
	    initInterfaceGestion();
});

function initInterfaceGestion() {
    $("#ouvrir").on("click", function() { getLaCarte(); });

    $("#stock").on("click", function() {
        carte.sauvegarde();
    });

    $("#creer").on("click", function() { 
    	carte = Carte.nouvelleCarteAvecZoneCentrale();
        carte.render();
    });

    $("#reveler").on("click", function() {
        carte.revelerRegion();
    });

    $("#masquer").on("click", function() {
        carte.masquerToutesRegions();
    });

    $("#reveler_tout").on("click", function() {
        carte.revelerToutesRegions();
    });

    $("#reveler_centre").on("click", function() {
        carte.revelerZoneCentrale();
    });
}