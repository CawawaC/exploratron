aventurier = Aventurier.getLaSession();

let divQueteTuto;
let tutoAutoPopUp = true;
getTutoAutoPopup();

$(document).ready(function() {


    getTextes("hall", parseTextes);

    $("#questLog").on("click", ouvrirPopUpQuete);
    $(document).on('click', '.etapeSuivanteTuto', function(event) { etapeTutoSuivante(event.target) });
    $(document).on('click', '.popupSuivant', function(event) { popupSuivant() });


    if (Constants.debug) {
        jQuery('<input>', {
            type: "button",
            value: "reset quetes",
            click: resetQuetes
        }).appendTo($("#debug"));

        jQuery('<input>', {
            type: "button",
            value: "finir quetes",
            click: finirQuetes
        }).appendTo($("#debug"));
    }

});

function activitesPossibles() {
    if (Constants.debug) return;
    let quetes = aventurier.quetes;
    // if (quetes.explore != QueteFlag.DONE)
    //     disable("objet");

    if (quetes.ramener3Miurges != QueteFlag.DONE) {
        disable("objet");
        disable("chambre-coffre");
    }

    if (quetes.crafter != QueteFlag.DONE)
        disable("chambre");

    if (quetes.upZir != QueteFlag.DONE)
        disable("ingredient");

    if (quetes.creerIngredient != QueteFlag.DONE)
        disable("RCE");

    disable("salon");
    disable("debarras");
    disable("chenil");
}

function disable(id) {
    $("#" + id).addClass('disabled');
}

function resetQuetes() {
    aventurier.initQuetes();
    aventurier.sauvegarde();
    location.reload();

    tutoAutoPopUp = true;
    modifierTutoAutoPopup(tutoAutoPopUp);
}

function finirQuetes() {
    aventurier.finirQuetes();
    aventurier.sauvegarde();
}

function parseTextes() {
    $("#title").html(textes.title);
    $("#order").html(textes.order + "<br/><br/>");
    $("#ingredient").html(textes.button_creature);
    $("#objet").html(textes.button_object);
    $("#vestibule").html(textes.button_adventure);
    $("#RCE").html(textes.button_RCE);
    $("#chant").html(textes.button_song);
    $("#chambre").html(textes.button_room);
    $("#chambre-coffre").html(textes.button_room_chest);
    $("#questLog").val(textes.button_tuto);
    $("#stock").html(textes.button_stock);
    $("#debarras").html(textes.button_closet);
    $("#chenil").html(textes.button_kennel);
    $("#chant").html(textes.button_song);

    getTextes("quests", gererQuetes);

    new Tooltip($('#ingredient'), {
        placement: 'top',
        html: true,
        title: textes.button_creature_tooltip
    });
    new Tooltip($('#objet'), {
        placement: 'top',
        html: true,
        title: textes.button_object_tooltip
    });
    new Tooltip($('#vestibule'), {
        placement: 'top',
        html: true,
        title: textes.button_adventure_tooltip
    });
    new Tooltip($('#RCE'), {
        placement: 'top',
        html: true,
        title: textes.button_RCE_tooltip
    });
    new Tooltip($('#chant'), {
        placement: 'top',
        html: true,
        title: textes.button_song_tooltip
    });
    new Tooltip($('#chambre'), {
        placement: 'top',
        html: true,
        title: textes.button_room_tooltip
    });

    new Tooltip($('#chambre-coffre'), {
        placement: 'top',
        html: true,
        title: textes.button_room_chest_tooltip
    });

    new Tooltip($('#questLog'), {
        placement: 'top',
        html: true,
        title: textes.button_tuto_tooltip
    });

}

function gererQuetes() {


    textes.upZir.STARTED[2].text = textes.upZir.STARTED[2].text.replace("_value_", Constants.zirMinimalCreationIngredient)

    setTextePopupQuete(textes.nothing);

    let quetes = aventurier.quetes;

    if (quetes.explore >= 0 && quetes.explore != QueteFlag.DONE) {

    } else if (quetes.ramener3Miurges >= 0 && quetes.ramener3Miurges != QueteFlag.DONE) {
        //Si inventaire contient au moins 1 de chaque miurge désirée, c'est réussi
        let protoRecette = Objet.getPremiereRecette();
        let premiersMiurges = protoRecette.ingredients;
        let reussi = true;
        for (let pm of premiersMiurges) {
            if (aventurier.inventaire.find(x => x.id == pm.id) == null)
                reussi = false;
        }

        if (reussi)
            quetes.ramener3Miurges = QueteFlag.FINISHED;
    }
    if (quetes.upZir >= 0 &&
        quetes.upZir != QueteFlag.DONE &&
        aventurier.getLesStatsTotales().volonte >= Constants.zirMinimalCreationIngredient) {
        quetes.upZir = QueteFlag.FINISHED;
    }
    if (quetes.creerIngredient >= 0 && quetes.creerIngredient != QueteFlag.DONE) {
        let creations = Ingredient.getIngredientsByJoueurCreateur(aventurier.nom);
        if (creations.length > 0)
            quetes.creerIngredient = QueteFlag.FINISHED;
    }

    let queteActuelle = null;
    for (let q in quetes) {
        if (quetes[q] == QueteFlag.DONE)
            continue;
        else {
            if (quetes[q] == QueteFlag.UNKNOWN)
                quetes[q] += 1;

            let progress = Object.keys(QueteFlag).find(k => QueteFlag[k] == quetes[q]);
            queteActuelle = { name: q, progress: progress };

            let txt = "";

            if (textes[q] != null)
                txt = textes[q][progress];
            else
                txt = "TEXTE INCONNU: " + q;

            if (queteActuelle.name == "ramener3Miurges" && quetes[q] == QueteFlag.STARTED) {
                let protoRecette = Objet.getPremiereRecette();
                aventurier.apprendRecette(protoRecette.id);
                aventurier.sauvegarde();
                let premiersMiurges = protoRecette.ingredients;

                txt[2].text = txt[2].text.replace("_ingredient1_", premiersMiurges[0].nom);
                txt[2].text = txt[2].text.replace("_ingredient2_", premiersMiurges[1].nom);
                txt[2].text = txt[2].text.replace("_ingredient3_", premiersMiurges[2].nom);
            }


            let enchainer = false;
            if (quetes[q] == QueteFlag.FINISHED) {
                quetes[q] = QueteFlag.DONE;

                //Bouton suivant pour emmener directement sur la quête suivante
                //divQueteTuto.append(boutonSuivant());
                enchainer = true;

                tutoAutoPopUp = true;
                modifierTutoAutoPopup(modifierTutoAutoPopup)


                // $("#fermerPopUp").on('click', function() { location.reload(); });
            } else {}

            setTextePopupQuete(txt, enchainer);

            if (tutoAutoPopUp) ouvrirPopUpQuete();

            break;
        }
    }

    aventurier.sauvegarde();


    activitesPossibles();
}

function getTutoAutoPopup() {
    var response = $.ajax({
        data: "",
        type: "POST",
        url: "exposeSession.php",
        async: true,
        success: function(data) {
            var objSession = JSON.parse(data);
            tutoAutoPopUp = objSession.tutoAutoPopUp == "true";

            if (tutoAutoPopUp) {
                if (divQueteTuto) ouvrirPopUpQuete();
            }
        }
    });
}


function modifierTutoAutoPopup(val) {
    $.ajax({
        url: 'modifierSession.php',
        type: 'GET',
        async: true,
        data: { tutoAutoPopUp: val }
    });
}

/*function boutonSuivant() {
    let boutonSuivant = jQuery("<input>", {
        type: "button",
        id: "popupSuivant",
        value: "Et que faire maintenant ?",
        click: popupSuivant
    });

    return boutonSuivant;
}*/

function popupSuivant() {
    tutoAutoPopUp = true;
    modifierTutoAutoPopup(tutoAutoPopUp)
    gererQuetes()
}

function setTextePopupQuete(message, enchainer) {
    let _contenu = "";
    divQueteTuto = jQuery("<div>");
    /* divQueteTuto.append(message).append("<br>");*/

    for (var i = 0; i < message.length; i++) {
        const display = i > 0 ? "none" : "auto";
        _contenu += '<div class="etape-tuto " data-id="' + i + '" style="display: ' + display + '">' + message[i].text;
        if (i < message.length - 1)
            _contenu += '<br><br><input type="button" class="etapeSuivanteTuto" value="' + message[i].button + '">';
        else if (!enchainer)
            _contenu += '<br><br><input type="button" class="fermerPopUp" value="' + message[i].button + '">';
        else
            _contenu += '<br><br><input type="button" class="popupSuivant" value="' + message[i].button + '">';

        _contenu += '</div>';
    }
    divQueteTuto = _contenu;
}

function etapeTutoSuivante(target) {
    let _nextId = $(target).parent().data("id") + 1;
    $("#popup .contenu").children().hide();
    $("#popup .contenu").find("[data-id=" + _nextId + "]").show();

}

function ouvrirPopUpQuete() {
    ouvrirPopUp(divQueteTuto);

    tutoAutoPopUp = false;
    modifierTutoAutoPopup(tutoAutoPopUp);
}