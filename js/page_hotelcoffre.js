import { Aventurier } from './aventurier'
import { Hotelcoffre } from './hotelcoffre'
import { Ingredient } from './ingredient'

var aventurier = Aventurier.getLaSession();
var coffre = getCoffre();

inscrireInventaire();
inscrireCoffre();

$(document).on('click', '#stocker', stocker);
$(document).on('click', '#retirer', retirer);

function getCoffre() {
	let stock = [];
	stock = Hotelcoffre.getHotelcoffre();
	var coffreIngredients = Ingredient.getIngredientsByIds(stock.map(x => x.id));
	// for(var c of coffreIngredients) {
	// 	c.quantite = stock.find(x => x.id == parseInt(c.id)).quantite;
	// }
	return coffreIngredients;
}

function inscrireInventaire() {
	//Nettoyage
	var select = document.getElementById("inventaire");
	while(select.options.length > 0) {
		select.options.remove(0);
	}

	console.log(aventurier)
	console.log(aventurier.inventaire)

	for(var e of aventurier.inventaire) {
		var x = document.createElement("OPTION");
		x.setAttribute("value", e.id);
		var t = document.createTextNode(e.nom);
		x.appendChild(t);
		document.getElementById("inventaire").appendChild(x);
	}
}

function inscrireCoffre() {
	//Nettoyage
	var select = document.getElementById("coffre-hotel");
	while(select.options.length > 0) {
		select.options.remove(0);
	}

	for(var e of coffre) {
		var x = document.createElement("OPTION");
		x.setAttribute("value", e.id);
		var t = document.createTextNode(e.nom);
		x.appendChild(t);
		document.getElementById("coffre-hotel").appendChild(x);
	}
}

function stocker() {
	var select = document.getElementById("inventaire");
	var i = document.getElementById("inventaire").selectedIndex;

	if(i >= 0) {
		for(let option of select.options) {
			let idIngredientStocke = option.value;
			let ingredient = aventurier.inventaire.filter(x => x.id == idIngredientStocke);
			let iIngredient = aventurier.inventaire.indexOf(ingredient);

			ingredient = aventurier.inventaire.splice(iIngredient, 1)[0];
			Hotelcoffre.ajouter(idIngredientStocke, 1);
		}

		aventurier.sauvegarde();

		inscrireInventaire();
		inscrireCoffre();
	}
}

function retirer() {
	if(!aventurier.placeDansLeSac()) {
		$("body").append("<p>Ton inventaire est plein, tu ne peux pas retirer plus d'ingrédients</p>"); 
		return;
	}

	var select = document.getElementById("coffre-hotel");
	var i = document.getElementById("coffre-hotel").selectedIndex;

	if(i >= 0) {
		for(let opt of select.options) {
			if(!aventurier.placeDansLeSac()) {
				$("body").append("<p>Ton inventaire est plein, tu ne peux pas retirer plus d'ingrédients</p>"); 
				break;
			}

			if(opt.selected) {
				let idIngredientRetire = opt.value;
				var ing = coffre.splice(i, 1)[0];
				aventurier.inventaire.push(ing);

				//Retrait 1 expemplaire ingrédient depuis BDD
				Hotelcoffre.retirer(idIngredientRetire, 1);
			}
		}

		aventurier.sauvegarde();		
		coffre = getCoffre();

		inscrireInventaire();
		inscrireCoffre();
	}
}