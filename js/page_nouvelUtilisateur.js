var nom;
var mdp;
var description;

$(document).ready(function() {
    getTextes("login", function() {
        setupNommer();
        parseTextes();
    });

    $('#input_text').keypress(function(event) {
        if (event.keyCode == 13 || event.which == 13) {
            event.preventDefault();
            console.log("entrée");

            $('#sub').click();
        }
    });

    
});


function setupNommer(texte) {
    $("#input_text")[0].value = "";
    $("#input_text")[0].type = "text";
    $("#input_text")[0].focus();

    if (texte == null)
        $("#input_question")[0].innerHTML = textes.welcome_pseudo;
    else
        $("#input_question")[0].innerHTML = texte;

    $("#sub")[0].value = textes.validate_pseudo;
    $("#sub")[0].onclick = nommer;
}

function nommer() {
    nom = $("#input_text")[0].value;
    console.log(nom)

    if (nom == "")
        setupNommer(textes.welcome_pseudo_empty);
    else {
        //Check BDD
        //Si bon, inscrire dans BDD et continuer
        //sinon demander un autre pseudo

        $.ajax({
            url: 'settersBDD/joueurs_ajouterPseudo.php',
            type: 'GET',
            data: {
                nom: nom
            },
            dataType: 'html', // On désire recevoir du HTML
            complete: function() {},
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
                if (code_html == "false") {
                    setupNommer("C'est gênant, ce nom est déjà pris par un résident. Vous en auriez pas un autre ?")
                } else {
                    setupMdp();
                }
            }
        });

    }

}

function setupMdp(texte) {
    $("#input_text")[0].value = "";
    $("#input_text")[0].type = "password";
    $("#input_text")[0].focus();

    if (texte == null)
        $("#input_question")[0].innerHTML = "Bonjour " + nom + " ! " + textes.welcome_password;
    else
        $("#input_question")[0].innerHTML = texte;

    $("#sub")[0].value = textes.validate_mdp;
    $("#sub")[0].onclick = setMdp;
}

function setMdp() {
    mdp = $("#input_text")[0].value;

    if (mdp == "")
        setupMdp(textes.welcome_password_empty);
    else {
        $.ajax({
            url: 'settersBDD/joueurs_ajouterMDP.php',
            type: 'GET',
            data: {
                nom: nom,
                mdp: mdp
            },
            dataType: 'html', // On désire recevoir du HTML
            complete: function(data) {},
            success: function(code_html, statut) { // code_html contient le HTML renvoyé
                console.log(code_html);

                if (code_html == "") {
                    setupDescription();
                } else {
                    console.log("Problème de création de mot de passe")
                }
            }
        });
    }
}

function setupDescription() {
    $("#input_text")[0].value = "";
    $("#input_text")[0].type = "text";
    $("#input_text")[0].focus();

    $("#input_question")[0].innerHTML = "Très bien ! " + textes.welcome_description;
    $("#sub")[0].value = textes.validate_description;
    $("#sub")[0].onclick = setDescription;
}

function setDescription() {
    description = $("#input_text")[0].value;
    //Inscrire description dans BDD
    $.ajax({
        url: 'settersBDD/joueurs_ajouterDescription.php',
        type: 'GET',
        data: {
            nom: nom,
            description: description
        },
        dataType: 'html', // On désire recevoir du HTML
        complete: function(data) {},
        success: function(code_html, statut) { // code_html contient le HTML renvoyé
            console.log(code_html);

            if (code_html == "") {
                setupFin();
            } else {
                console.log("Pb d'ajout de description")
            }
        }
    });
}

function setupFin() {
    console.log("clean")

    $("#welcome")[0].remove();
    $("#input_question")[0].innerHTML = textes.welcome_end;
    $("#input_text")[0].remove();

    $("#sub")[0].value = textes.welcome_enter;
    $("#sub")[0].onclick = allerAuHub;
}

function allerAuHub() {
    location = "hub.php";
}

function parseTextes() {
    $("#title")[0].innerHTML = textes.title;
    let subtitle = textes.subtitle[random(textes.subtitle.length)];
    $("#subtitle").html(subtitle);
    $("#welcome")[0].innerHTML = textes.welcome;
}

function random(max) {
    let x = Math.floor(Math.random() * max);
    return x;
}
