<?php 
	include("sessionCheck.php"); // décommenter pour interdire l'accès aux utilisateurs non connectés 
	include("header.php");
	$_SESSION['idVoronoiActuel'] = -1;
	$_SESSION['distanceVoronoiActuel'] = -1;
?>

<script type="text/javascript" src="js/stats.js"></script>
<script type="text/javascript" src="js/objets.js"></script>
<script type="text/javascript" src="js/ingredient.js"></script>
<script type="text/javascript" src="js/Pouvoirs.js"></script>
<script type="text/javascript" src="js/Constants.js"></script>
<script type="text/javascript" src="js/Aventurier.js"></script>
<script src="js/scripts_hub.js"></script>

<?php include("headerQuitter.php");?>


<div class="colonneCentrale">

	 <div id="lignes">
	   <img src="medias/images/lignes.png">
	 </div>

	<h2 id="title"></h2>
	<hr>
	<div id="order"></div>
	<p id="explorationConteneur"><a href="preparation_exploration.php"  id="vestibule"></a></p>
	<p id="objetConteneur"><a href="creation_objet.php"  id="objet"></a></p>
	<p class="" id="RCEConteneur"><a href="registre.php"  id="RCE"></a></p>
	<p id="ingredientConteneur"><a href="creation_ingredient.php" id="ingredient"></a></p>
	<p id="chantConteneur"><a href="chant.php" id="chant"></a></p>
	<p class=""id="chambre-coffreConteneur"><a href="chambre-coffre.php"  id="chambre-coffre"></a></p>
	<p class=""id="chambreConteneur"><a href="chambre-vestiaire.php"  id="chambre"></a></p>
	<p class=""id="chantConteneur"><a href="chant.php"  id="chant"></a></p>
	<p class="cache"><a href="salon.php"  id="salon">Aménager mon salon</a></p>
	<p class="cache"><a href="hotel-vestiaire.php"  id="debarras"></a></p>
	<p class="cache"><a href="chenil.php"  id="chenil"></a></p>
	<p id="questLogConteneur"><input type="button" value="Que dois-je faire ?" id="questLog"></p>
	<!-- <p><a href="gestion_carte.php">Gestion de la carte (DEV)</a></p> -->
</div>

	<div id="debug"/>



	
<?php include("footer.php"); ?>