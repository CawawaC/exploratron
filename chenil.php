<?php 
	include("sessionCheck.php"); // décommenter pour interdire l'accès aux utilisateurs non connectés 
	include("header.php"); 
	$_SESSION['idVoronoiActuel'] = -1;
	$_SESSION['distanceVoronoiActuel'] = -1;
	include("headerNavigation.php"); 
?>

<script type="module" src="js/page_chenil.js"></script>

<section id="stock">
	<p id="consigne_chenil"></p>

	<select id="equipement-familier" size="1">
	</select>

	<input type="button" id="stocker" value="-->"/>
	<input type="button" id="retirer" value="<--"/>

	<select id="chenil-chambre" size="10">
	</select>
</section>


<?php include("footer.php"); ?>