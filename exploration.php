<?php 

	include("sessionCheck.php"); // décommenter pour interdire l'accès aux utilisateurs non connectés 
	include("header.php"); 

	if($_SESSION['idVoronoiActuel'] == -1)
		echo "<script type='text/javascript'>document.location.replace('hub.php');</script>";

	$place;

	if (isset($_GET['place']))
		$place = $_GET['place'];
	else
		$place = -1;
?>


<script src="js/canvas_carte_lieu.js"></script>
<script type="text/javascript" src="js/Constants.js"></script>
<script type="text/javascript" src="js/Pouvoirs.js"></script>
<script type="text/javascript" src="js/objets.js"></script>
<script type="text/javascript" src="js/Aventurier.js"></script>
<script type="text/javascript" src="js/ingredient.js"></script>
<script type="text/javascript" src="js/stats.js"></script>
<script defer type="text/javascript" src="js/generateur_donjon.js"></script>
<script defer type="text/javascript" src="js/scripts_exploration.js"></script>


	
<nav id="menu-exploration">
	<!--a href="quitter">menu</a-->

	<a href="personnage" id="nav_bouton_personnage">
		<img 
		src="medias/images/picto-joueur.svg" 
		alt="quitter la maison perchée"
		width="38px">
	</a>
	<a href="carte" id="nav_bouton_carte">
		<img 
		src="medias/images/picto-carte.svg" 
		alt="quitter la maison perchée"
		width="53px" /></a>


		<!--input type="button" id="sortir" value="Sortir (temp)"/-->
	</nav>
	<?php include("headerQuitter.php"); ?>

	<section id='bandeau-central-exploration'>
		<h1 id="titre-suite"></h1>
		<hr>
		<div id="conteneur-jauge-temps">
			<h2 id="titre-temps"></h2>
			<div id="myProgress">
				<div id="myBar"></div>
			</div>


		</div>
	</section>

	<section id='pieces'>
	</section>



	<div class="panneau-lateral " id="personnage">
		<div class="contenu">
			<div class="conteneur-flex">
			<div id="stats-joueur" class="transparent">
					<div class="contenu">
						<h2></h2>
					<p class="tip">Voici les pouvoirs conférés par votre équipement</p>
					<div id="pouvoirs"></div>
				</div>
			</div>

			<div class = "conteneur-liste">
				<p id="inventaire-perso"> sur moi</p><p id="remplissage-sac"></p>
				<div style="clear: both;"></div>
				<form id="inventaire">
				</form>
			</div>

			<div class = "conteneur-liste">
				<p id="equipement-perso"> sur moi</p>
				<div id="equipement-chambre">
				</div>
			</div>
			</div>
			<div class="colonne" id="fiche">
			</div>
			<a class="fermeture">X</a>
		</div>
	</div>

	<div class="panneau-lateral " id="carte">
		<canvas id="carte_lieu" width="1000" height="1000"></canvas>
		<a class="fermeture">X</a>
	</div>

	<div class="panneau-lateral " id="quitter">
		<div class="colonne">
			<p>êtes vous sur de vouloir retourner au menu ?</p>
			<a href="hub.php">oui</a>
		</div>
		<a class="fermeture">X</a>		
	</div>

	<div class="ombre cachee"></div>



	<script type="text/javascript">
		var place = <?php Print($place); ?>;
	</script>


	
	

	<?php include("footer.php"); ?>

