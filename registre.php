 <?php 
    include("sessionCheck.php"); // décommenter pour interdire l'accès aux utilisateurs non connectés 
    include("header.php"); 
    $_SESSION['idVoronoiActuel'] = -1;
    $_SESSION['distanceVoronoiActuel'] = -1;
    include("headerNavigation.php");

try{

	// $bdd = new PDO('mysql:host=db4free.net;dbname=db_exploratron;charset=utf8mb4', 'adminexploratron', '28112018');
	$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
}

catch (Exception $e){
    die('Erreur : ' . $e->getMessage());
}?>



<div class="colonneCentrale">
    <h2 id="title"> Le registre des choses qui existent</h2>
    <hr>
    <div id="tableIngredients">
        <div id="filtresRecherche">
            <div class="contenu">
             <input class="search" placeholder="Rechercher" /><br><br>
             <p> trier par :</p>
                 <button type="button" class="sort" data-sort="nom">nom</button><button type="button" class="sort" data-sort="region">suite</button>
           <br> <p> fitrer par :</p>
            <button type="button" id="filtreTous">tous</button>
            <button type="button" id="filtreOnirique">onirique</button>
             <button type="button" id="filtreEgaree">égarée</button>
              <button type="button" id="filtreUtile">utile</button>
               <button type="button" id="filtrePrécieux">précieux</button>
                <button type="button" id="filtreBotanique">botanique</button>
            <br> </div>
        </div>
         <ul class="list"></ul>    
    </div>
</div>



<script type="text/javascript" src="js/Constants.js"></script>
<script type="text/javascript" src="js/stats.js"></script>
<script type="text/javascript" src="js/Pouvoirs.js"></script>
<script type="text/javascript" src="js/objets.js"></script>
<script type="text/javascript" src="js/Aventurier.js"></script>
<script type="text/javascript" src="js/ingredient.js"></script>

<!--link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script-->
<script src="//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
<script type="text/javascript" src="js/scripts_registre.js"></script>

		
<?php include("footer.php"); ?>