<?php 
	include("sessionCheck.php"); // décommenter pour interdire l'accès aux utilisateurs non connectés 
	include("header.php"); 
	$_SESSION['idVoronoiActuel'] = -1;
	$_SESSION['distanceVoronoiActuel'] = -1;
	include("headerNavigation.php");
?>

<script type="text/javascript" src="js/stats.js"></script>
<script type="text/javascript" src="js/objets.js"></script>
<!-- <script type="text/javascript" src="js/hotelcoffre.js"></script> -->
<script type="text/javascript" src="js/ingredient.js"></script>
<script type="text/javascript" src="js/Pouvoirs.js"></script>
<script type="text/javascript" src="js/Constants.js"></script>
<script type="text/javascript" src="js/Aventurier.js"></script>


<div class="colonneCentrale">
	<section id="creationIngredient">
		<h2 id="title"></h2>
		<hr>
		<div id="order" class="bloc-principal"></div>

		<form id="formulaireIngredient">

			<div class="conteneur-flex bloc-principal">
				<div id="bloc-regne" class="ingredient-flex">
					<p id="order_01"></p>				
					<div id="regne" class="condition"></div>
				</div>

			  	<div id="bloc-genre" class="ingredient-flex bloc-cache">
			  		<p id="order_02"></p>
			  		<div id="genre">
						<input type="radio" name="genre" class="condition" value="masculin" id="masculin"><label id="label_01" for="masculin">un garçon</label>
						<input type="radio" name="genre" class="condition" value="feminin" id="feminin"> <label id="label_02" for="feminin"> une fille</label>
						<input type="radio" name="genre" class="condition" value="rand" id="rand"> <label id="label_03" for="rand">Au choix</label><br>
					</div>
				</div>
		 	</div>
		 	<div class="bloc-principal bloc-cache" id="bloc-description">
			 	<p id="nombreCaracteres"> 0/25</p>
				<textarea placeholder="À quoi ressemble iel ?" class="condition" id="description" rows="4" cols="50"></textarea> 
			</div>

			<div class="bloc-principal bloc-cache" id="bloc-carte">
					<p id="order_04"></p>  
			
					<?php include("carte.php"); ?>
			</div>
			<br/><br/>
			<div id="bloc-validation" class="bloc-cache">
	  			<input type="button" id="sub" value=""/>
	  		</div>
		</form>
	</section>
</div>

<section id='popup' class="ferme" >
		<div class="ombre cacherPopup">
			<div class="conteneur">
				<div class="contenu">
					<h1>titre</h1>
					<p>conentu du pop up</p>
					<input type="button" value="fermer" class="cacherPopup"/>
				</div>
			</div>
		</div>
	</section>

	
<script type="text/javascript" src="js/libs/rhill-voronoi-core.min.js"></script>
<script type="text/javascript" src="js/carte.js"></script>
<script type="text/javascript" src="js/scripts_ingredient.js"></script>


<?php include("footer.php"); ?>