<?php
// $xs;
// $ys;
// $visibles;

$sites;

require_once('../config.php');


$sites = json_decode($_POST["sites"]);

echo "<p>Enregistrement de la carte...</p>";
// echo isset($_GET['xs']);
// echo $ys == [];
// echo $visibles == [];

// var_dump($sites);

if($sites != null) {
	
	try{
		$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
	} catch (Exception $e){
	    die('Erreur : ' . $e->getMessage());
	}

	//Effacement de la carte enregistrée
	$sql = "TRUNCATE TABLE carte";
	$req = $bdd->prepare($sql);
	if ($req->execute()) {
	    echo "<p>Ancienne carte effacée</p>";
	} else {
	    echo "Error: " . $sql . "<br>" . $bdd->error;
	}

	$sql = "INSERT INTO carte(x, y, visible, id) values ";

	foreach ($sites as $site) {
	    $x = $site->x;
        $y = $site->y;
        $visible = $site->visible;
        $id = $site->voronoiId;

		$req = $bdd->prepare('INSERT INTO carte(x, y, visible, id) VALUES(:x, :y, :visible, :id)');
		// var_dump($site);
		$req->execute(array(
			'x' => $x,
		    'y' => $y,
		    'visible' => $visible ? 1 : 0,
		    'id' => $id
	    ));
	}

	echo "<p>Carte enregistrée !</p>";
}
?>