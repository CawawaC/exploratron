<?php
session_start();
 
require_once('../config.php');

// get the q parameter from URL
$nom = strip_tags($_GET["nom"]);

try
{
	$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e)
{
    die('Erreur : ' . $e->getMessage());
}

$req = $bdd->prepare('SELECT * FROM joueurs WHERE nom = :pseudo');
$req->execute(array('pseudo' => $nom));
$resultat = $req->fetch();
$req->closeCursor();
// echo $resultat;

if($resultat == false) {
	$req = $bdd->prepare('INSERT INTO joueurs(`nom`, `description`, `vie`, `vol`, `vit`) VALUES(:pseudo, "", 5, 10, 5)');
	$req->execute(array('pseudo' => $nom));
	$resultat = $req->fetch();
    $req->closeCursor();
    $_SESSION['nom'] = $nom;
    $_SESSION['vie'] = 5;
    $_SESSION['vol'] = 10;
    $_SESSION['vit'] = 5;
    $_SESSION['objets'] = "";
    $_SESSION['objetsMax'] = 3;
    $_SESSION['ingredientsMax'] = 5;
    $_SESSION['idVoronoiActuel'] = -1;
    $_SESSION['distanceVoronoiActuel'] = -1;
    $_SESSION['vestiaire'] = "";
    $_SESSION['chenil'] = "";
    $_SESSION['poeme'] = "";
    $_SESSION['familiers'] = "";
    $_SESSION['inventaire'] = "";
    $_SESSION['tuto'] = "";
    $_SESSION['coffre'] = "";

	echo $resultat;
} else {
	echo "false";
}




?>