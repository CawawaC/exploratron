<?php
$ingredients;
$categorie;
$description;
$stats;
$nom;
$pouvoirs;

require_once('../config.php');

if (isset($_GET['ingredients']))
	$ingredients = $_GET['ingredients'];
else
	$ingredients = [];

if (isset($_GET['description']))
	$description = strip_tags($_GET['description']);
else
	$description = null;

if (isset($_GET['categorie']))
	$categorie = $_GET['categorie'];
else
	$categorie = "";

if (isset($_GET['stats']))
	$stats = $_GET['stats'];
else
	$stats = null;

if (isset($_GET['nom']))
	$nom = $_GET['nom'];
else
	$nom = "inconnu";

if (isset($_GET['pouvoirs']))
	$pouvoirs = $_GET['pouvoirs'];
else
	$pouvoirs = "";

if($categorie != "" && $stats != null && $ingredients != null && $ingredients != []) {
	
	try{
		$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
	}

	catch (Exception $e){
	    die('Erreur : ' . $e->getMessage());
	}

	$statsSerialisees = json_encode($stats);
	$pouvoirs = json_encode($pouvoirs);

    $req = $bdd->prepare('INSERT INTO objets(nom, def, ingredient1, ingredient2, ingredient3, stats, categorie, pouvoirs) VALUES(:nom, :def, :ings0, :ings1, :ings2, :stats, :cat, :pouvoirs)');
	$req->execute(array(
		'nom' => $nom,
	    'def' => $description,
	    'ings0' => $ingredients[0],
	    'ings1' => $ingredients[1],
	    'ings2' => $ingredients[2],
	    'stats' => $statsSerialisees,
	    'cat' => $categorie,
	    'pouvoirs' => $pouvoirs
	    ));

	$lastId = $bdd->lastInsertId();

	echo $lastId;
}
?>
