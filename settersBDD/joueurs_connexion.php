<?php
session_start();
 
require_once('../config.php');

// get the q parameter from URL
$nom = $_GET["nom"];
$mdp = $_GET["mdp"];

try {
	$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$req = $bdd->prepare("SELECT * FROM joueurs WHERE mdp = :mdp AND nom = :nom");
$req->execute(array(
	'mdp' => $mdp,
	'nom' => $nom
	));
$resultat = $req->fetch();

if($resultat == false) {
	$resultat['status'] = 'failure';
} else {
	$_SESSION['nom'] = $resultat['nom'];
	$_SESSION['vie'] = $resultat['vie'];
	$_SESSION['vol'] = $resultat['vol'];
	$_SESSION['vit'] = $resultat['vit'];
	$_SESSION['description'] = $resultat['description'];
	$_SESSION['objets'] = $resultat['objets'];
	$_SESSION['objetsMax'] = $resultat['objetsMax'];
	$_SESSION['ingredientsMax'] = $resultat['ingredientsMax'];
	$_SESSION['idVoronoiActuel'] = -1;
	$_SESSION['vestiaire'] = $resultat['vestiaire'];
	$_SESSION['coffre'] = $resultat['coffre'];
	$_SESSION['chenil'] = $resultat['chenil'];
	$_SESSION['poeme'] = $resultat['poeme'];
	$_SESSION['familiers'] = $resultat['familiers'];
	$_SESSION['inventaire'] = $resultat['inventaire'];
	$_SESSION['tuto'] = $resultat['tuto'];
}

header('Content-type: application/json');
echo json_encode($resultat);



// $resultat['status'] = 'success';  

// echo $resultat;

?>