<?php
session_start();
 
require_once('../config.php');

// get the q parameter from URL
$nom = $_GET["nom"];
$mdp = $_GET["mdp"];

try {
	$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$req = $bdd->prepare("UPDATE joueurs SET mdp = :mdp WHERE nom = :nom");
$req->execute(array(
	'mdp' => $mdp,
	'nom' => $nom
	));
$resultat = $req->fetch();

echo $resultat;

?>