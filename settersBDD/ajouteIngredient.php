<?php

require_once('../config.php');

$def;
$place;
$genre;
$type;
$stats;
$consommation;
$createur;
$stats_combat;
$seedCombat;
$agressif;
$magnitude;

if (isset($_GET['genre']))
	$genre = strip_tags($_GET['genre']);
else
	$genre = null;

if (isset($_GET['description']))
	$def = strip_tags($_GET['description']);
else
	$def = null;

if (isset($_GET['pos']))
	$place = strip_tags($_GET['pos']);
else
	$place = null;

if (isset($_GET['type']))
	$type = strip_tags($_GET['type']);
else
	$type = null;

if (isset($_GET['agressif']))
	$agressif = strip_tags($_GET['agressif']);
else
	$description = null;

if (isset($_GET['stats']))
	$stats = $_GET['stats'];
else
	$stats = null;

if (isset($_GET['consommation']))
	$consommation = $_GET['consommation'];
else
	$consommation = null;

if (isset($_GET['createur']))
	$createur = $_GET['createur'];
else
	$createur = "";

if (isset($_GET['magnitude']))
	$magnitude = $_GET['magnitude'];
else
	$magnitude = 0;

try{
	$bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

//créer stats combat
$seedCombat = 10;
$stats_combat = [];
$stats_combat[0] = rand(0, $seedCombat);
$stats_combat[1] = rand(0, $seedCombat - $stats_combat[0]);
$stats_combat[2] = rand(0, $seedCombat - $stats_combat[0] - $stats_combat[1]);
shuffle($stats_combat);

// crée ingrédient
if($def!=null && $place!=null && $genre!=null && $type!=null && $stats != null) {
	
	try{
		$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
		// $bdd->query("SET NAMES utf8mb4"); // pour la gestion des accents
	} catch (Exception $e){
	    die('Erreur : ' . $e->getMessage('erreur'));
	}
	
	$statsSerialisees = json_encode($stats);
	$consommationSerialisee = json_encode($consommation);

    $req = $bdd->prepare('INSERT INTO ingredients(nom, def, place, genre, type, stats, consommation, joueurCreateur, joueurNommeur, stats_combat, agressif, magnitude) VALUES(:nom, :def, :place, :genre, :type, :stats, :consommation, :joueurCreateur, :joueurNommeur, :stats_combat, :agressif, :magnitude)');

	$req->execute(array(
		'nom' => '',
	    'def' => $def,
	    'place' => $place,
	    'genre' => $genre,
	    'type' => $type,
	    'stats' => $statsSerialisees,
	    'consommation' => $consommationSerialisee,
	    'joueurCreateur'=> $createur,
	    'joueurNommeur' => '',
	    'stats_combat' => json_encode($stats_combat),
	    'agressif' => $agressif,
	    'magnitude' => $magnitude
	));

	$last_id = $bdd->lastInsertId();
	echo $last_id;
}
?>