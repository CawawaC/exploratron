<?php
session_start();
 
require_once('../config.php');

// get the q parameter from URL
$nom = $_GET["nom"];
$description = $_GET["description"];

try {
	$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$req = $bdd->prepare("UPDATE joueurs SET description = :description WHERE nom = :nom");
$req->execute(array(
	'description' => $description,
	'nom' => $nom
	));
$resultat = $req->fetch();

$_SESSION['description'] = $description;

echo $resultat;

?>