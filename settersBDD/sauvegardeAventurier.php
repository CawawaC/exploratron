<?php
	session_start();
	 
	require_once('../config.php');

	$nom;
	$vie;
	$vol;
	$vit;
	$objets;
	$vestiaire;
	$coffre;
	$chenil;
	$poeme;
	$familiers;
	$inventaire;
	$tuto;

	$nom = ($_POST['nom']);
	$vie = ($_POST['vie']);
	$vol = ($_POST['vol']);
	$vit = ($_POST['vit']);

	if (isset($_POST['objets']))
		$objets = $_POST['objets'];
	else $objets = null;

	if (isset($_POST['vestiaire']))
		$vestiaire = $_POST['vestiaire'];
	else $vestiaire = null;

	if (isset($_POST['coffre']))
		$coffre = $_POST['coffre'];
	else $coffre = null;

	if (isset($_POST['chenil']))
		$chenil = $_POST['chenil'];
	else $chenil = null;

	if (isset($_POST['poeme']))
		$poeme = $_POST['poeme'];
	else $poeme = null;

	if (isset($_POST['familiers']))
		$familiers = $_POST['familiers'];
	else $familiers = null;

	if (isset($_POST['inventaire']))
		$inventaire = $_POST['inventaire'];
	else $inventaire = null;

	if (isset($_POST['tuto']))
		$tuto = $_POST['tuto'];
	else $tuto = "";

	if (isset($_POST['phrases']))
		$phrases = $_POST['phrases'];
	else $phrases = null;

	try {
		$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
	} catch (Exception $e) {
	    die('Erreur : ' . $e->getMessage());
	}

	if($objets != null) $objets = serialize($objets);
	if($vestiaire != null) $vestiaire = serialize($vestiaire);
	if($coffre != null) $coffre = serialize($coffre);
	if($chenil != null) $chenil = serialize($chenil);
	if($poeme != null) $poeme = serialize($poeme);
	if($familiers != null) $familiers = serialize($familiers);
	if($inventaire != null) $inventaire = serialize($inventaire);
	if($tuto != null) $tuto = serialize($tuto);

	// echo $objets;

	$result = $bdd->query("UPDATE joueurs SET 
		vie='$vie', 
		vol='$vol', 
		vit='$vit', 
		objets='$objets',
		vestiaire='$vestiaire',
		coffre='$coffre',
		chenil='$chenil',
		poeme='$poeme',
		familiers='$familiers',
		inventaire='$inventaire',
		tuto='$tuto'
		WHERE nom='$nom'");

	// echo $result;

	$_SESSION['nom'] = $nom;
	$_SESSION['vie'] = $vie;
	$_SESSION['vol'] = $vol;
	$_SESSION['vit'] = $vit;
	$_SESSION['objets'] = $objets;
	$_SESSION['vestiaire'] = $vestiaire;
	$_SESSION['coffre'] = $coffre;
	$_SESSION['chenil'] = $chenil;
	$_SESSION['poeme'] = $poeme;
	$_SESSION['familiers'] = $familiers;
	$_SESSION['inventaire'] = $inventaire;
	$_SESSION['tuto'] = $tuto;

?>