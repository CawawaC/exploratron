<?php
require_once('config.php');

try {
	$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

// texte intro
$req = $bdd->prepare('SELECT texte FROM piece_intro');
$array = array('N');
$req->execute($array);
$resultat = $req->fetch();

echo $resultat;

$req->closeCursor();


// texte sol
if(rand(0,5)>2) {
	$req = $bdd->prepare('SELECT texte FROM piece_sol ORDER BY RAND() LIMIT 1');
	$array = array('N');
	$req->execute($array);
	$resultat = $req->fetch();

	echo " ".$resultat['texte'];

	$req->closeCursor();
}

// texte mur
if(rand(0,5)>2) {
	$req = $bdd->prepare('SELECT texte FROM piece_mur ORDER BY RAND() LIMIT 1');
	$array = array('N');
	$req->execute($array);
	$resultat = $req->fetch();

	echo " ".$resultat['texte'];

	$req->closeCursor();
}

// texte fenetre
if(rand(0,5)>2) {
	$req = $bdd->prepare('SELECT texte FROM piece_fenetre ORDER BY RAND() LIMIT 1');
	$array = array('N');
	$req->execute($array);
	$resultat = $req->fetch();

	echo " ".$resultat['texte'];

	$req->closeCursor();
}

// description créature 
$genreCreature = "feminin";
$descriptionCreature = '<a class="ingredient" href="creature">description créature</a>';


// texte créature
$req = $bdd->prepare('SELECT texte FROM piece_creature ORDER BY RAND() LIMIT 1');
$array = array('N');
$req->execute($array);
$resultat = $req->fetch();
$texteCreature = $resultat['texte'];
$req->closeCursor();

preg_match_all('/==(.*?)==/', $texteCreature, $match, PREG_PATTERN_ORDER);
//print_r($match[1]);
$patterns = array();

for ($i = 0; $i<sizeof($match[1]); $i++){
	$patterns[$i] = '/=='.$match[1][$i].'==/';
}

$replacements = array();
$accords;

for ( $i = 0; $i<sizeof($match[1]); $i++){
	
	if(strcmp ($match[1][$i],'créature')===0){

		$replacements[$i] = $descriptionCreature;
	}
	$accords = explode("--", $match[1][$i]);
	if (sizeof($accords)>1){		
		if($genreCreature =="masculin")
			$replacements[$i] = $accords[0];
		if($genreCreature =="feminin")
			$replacements[$i] = $accords[1];
	}
}
echo " ".preg_replace($patterns, $replacements,$texteCreature);

?>