<?php 
	include("sessionCheck.php"); // décommenter pour interdire l'accès aux utilisateurs non connectés 
	include("header.php"); 
	$_SESSION['idVoronoiActuel'] = -1;
	$_SESSION['distanceVoronoiActuel'] = -1;
	include("headerNavigation.php");
?>


<script type="text/javascript" src="js/stats.js"></script>
<script type="text/javascript" src="js/objets.js"></script>
<script type="text/javascript" src="js/ingredient.js"></script>
<script type="text/javascript" src="js/Pouvoirs.js"></script>
<script type="text/javascript" src="js/Constants.js"></script>
<script type="text/javascript" src="js/Aventurier.js"></script>
<!-- <script type="text/javascript" src="js/hotelcoffre.js"></script> -->


<script type="text/javascript" src="js/page_chambrecoffre.js"></script>



<div class="colonneCentrale coffre" >
<section id="stock">
	<h2 id="title"> coffre</h2>
		<hr>
	<p>Stocker et retirer des cartes de visites.</p>

	<!--select id="inventaire" size="10", , name="Inventaire" multiple="true">
	</select>

	<input type="button" id="stocker" value=">"/>
	<input type="button" id="retirer" value="<"/>

	<select id="coffre-chambre" size="10" name="Coffre" multiple="true">
	</select-->

	<p id="consigne_coffre"></p>
	<br/>
	<div class="conteneur-flex">
	<div class = "conteneur-liste">
	<p id="inventaire-perso"> sur moi</p><p id="remplissage-sac"></p>
	<div style="clear: both;"></div>
	<form id="inventaire">
	</form>
	</div>
	<div>
	<input type="button" id="stocker-coffre" value="&#9654;" disabled="true" />
	<input type="button" id="retirer-coffre" value="&#9664;" disabled="true"/>
</div>
	<div class = "conteneur-liste">
		<p id="inventaire-coffre">dans mon classeur</p>
		<form id="coffre">
		</form>
	</div>
</div>


</section>
</div>
