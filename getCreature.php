<?php 

require_once('config.php');

$nombre =5;

try{
	$bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
}

catch (Exception $e){
    die('Erreur : ' . $e->getMessage());
}

//$result = $bdd->query('SELECT * FROM objets ORDER BY RAND() LIMIT N');

$result = $bdd->prepare('SELECT * FROM objets ORDER BY RAND() LIMIT  :nombre');
$result->execute(array('nombre' => $nombre));

$arr = [];
$i = 0;
while($row = $result->fetch(PDO::FETCH_ASSOC)) {
    // $arr[$i] = $row["nom"];
    $arr[$i] = $row;
    $i += 1;
}

echo json_encode($arr);
/*
if ($result->num_rows > 0 && $result->num_rows != null) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        echo utf8_encode(json_encode($row));
        echo "nom: " . $row["nom"];
    }
} else {
    echo "0 results";
}*/

$result->closeCursor();
?>