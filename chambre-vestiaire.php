<?php 
	include("sessionCheck.php"); // décommenter pour interdire l'accès aux utilisateurs non connectés 
	include("header.php"); 
	$_SESSION['idVoronoiActuel'] = -1;
	$_SESSION['distanceVoronoiActuel'] = -1;
	include("headerNavigation.php");
?>


<script type="text/javascript" src="js/stats.js"></script>
<script type="text/javascript" src="js/objets.js"></script>
<script type="text/javascript" src="js/ingredient.js"></script>
<script type="text/javascript" src="js/Pouvoirs.js"></script>
<script type="text/javascript" src="js/Constants.js"></script>
<script defer type="text/javascript" src="js/Aventurier.js"></script>
<script defer type="text/javascript" src="js/page_chambrevestiaire.js"></script>


<div class="DEBUG"></div>

<div class="colonneCentrale vestiaire" >
	<section id="stock">
		<h2 id="title"> penderie</h2>
		<hr>
		<p id="consigne_penderie"></p>
		<br/>
		<div class="conteneur-flex">
					<div class = "conteneur-liste">
						<p id="equipement-perso"> sur moi</p>
						<form id="equipement-chambre">
						</form>
					</div>
					<div>
						<input type="button" id="stocker-vestiaire" value="&#9654;" disabled="true" />
						<input type="button" id="retirer-vestiaire" value="&#9664;" disabled="true"/>
					</div>
					<div class = "conteneur-liste">
						<p id="equipement-penderie">dans ma penderie</p>
						<form id="vestiaire-chambre"></form>
					</div>
				</div>
	</section>
	<div id="stats-joueur">
				<div class="contenu">
					<div id="pouvoirs"></div>
				</div>
	</div>
</div>


<?php include("footer.php"); ?>