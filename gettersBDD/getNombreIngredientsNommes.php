 <?php 

require_once('../config.php');

try{
	$bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e){
    die('Erreur : ' . $e->getMessage());
}

$result = $bdd->prepare('SELECT COUNT(*) FROM ingredients WHERE nom <> ""');
$result->execute();
$number_of_rows = $result->fetchColumn(); 

echo $number_of_rows;
?>