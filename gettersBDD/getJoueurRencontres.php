<?php 

require_once('../config.php');

$nomJoueur = $_GET["nomJoueur"];

try{
	$bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e){
    die('Erreur : ' . $e->getMessage());
}

$result = $bdd->query("SELECT rencontres FROM joueurs WHERE nom = '$nomJoueur'");
$result = $result->fetch();
$rencontres = unserialize($result[0]);

echo json_encode($rencontres);

?>