<?php 

require_once('../../config.php');

$n = $_GET["n"];

try{
	$bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e){
    die('Erreur : ' . $e->getMessage());
}

//Pas réussi à mettre $n après le LIMIT ! Aucune idée pourquoi.
//"SELECT * FROM ingredients WHERE nom <> '' ORDER BY RAND() LIMIT 3"
$result = $bdd->prepare("SELECT * FROM ingredients ORDER BY id LIMIT $n");
$result->execute();

$arr = [];
$i = 0;
while($row = $result->fetch(PDO::FETCH_ASSOC)) {
    $arr[$i] = $row;
    $i += 1;
}

echo json_encode($arr);

$result->closeCursor();
?>