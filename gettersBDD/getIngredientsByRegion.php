 <?php

require_once('../config.php');

$region;
$region = $_GET["region"];

if($region != null) {

	try {
		$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
	} catch (Exception $e) {
	    die('Erreur : ' . $e->getMessage());
	}

	$sql = "SELECT * FROM ingredients WHERE place='$region'";
	$result = $bdd->query($sql);

	$arr = [];
	$i = 0;
	while($row = $result->fetch(PDO::FETCH_ASSOC)) {
	    $arr[$i] = $row;
	    $i += 1;
	}

	echo json_encode($arr);
}


?>