 <?php 

require_once('../config.php');

$quantite = $_GET['quantite'];

try{
	$bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e){
    die('Erreur : ' . $e->getMessage());
}

$result = $bdd->query("
	SELECT *
	FROM hotelvestiaire
	WHERE quantite > 0
	ORDER BY RAND()
	LIMIT 10"
);

//Pas de doublons


$arr = [];
$i = 0;
while($row = $result->fetch(PDO::FETCH_ASSOC)) {
    $arr[$i] = $row;
    $i += 1;
}

echo json_encode($arr);

$result->closeCursor();
?>