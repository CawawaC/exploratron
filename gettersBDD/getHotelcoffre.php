<?php 

require_once('../config.php');

try{
	$bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e){
    die('Erreur : ' . $e->getMessage());
}

$result = $bdd->query('SELECT * FROM hotelcoffre');

$arr = [];
$i = 0;
while($row = $result->fetch(PDO::FETCH_ASSOC)) {
    $arr[$i] = $row;
    $i += 1;
}

echo json_encode($arr);

$result->closeCursor();
?>