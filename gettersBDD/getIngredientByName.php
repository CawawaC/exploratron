 <?php 

 // pour récupérer les ingrédients du joueur (création d'objets)

require_once('../config.php');

$ingredients;
$ingredients = json_decode($_GET["ingredients"]);

if($ingredients != null) {
	try{
		$bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
	} catch (Exception $e){
	    die('Erreur : ' . $e->getMessage());
	}

	$arr = [];
	foreach ($ingredients as $ing) {
		$sql = "SELECT * FROM ingredients WHERE nom='$ing'";
		$stmt = $bdd->query($sql); 
		$row = $stmt->fetchObject();
		$arr[] = $row;
	}

	echo json_encode($arr);
}

?>