 <?php 

require_once('../config.php');

try{
	$bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e){
    die('Erreur : ' . $e->getMessage());
}

$result = $bdd->query('SELECT * FROM carte');

$json = [];
while($row = $result->fetch(PDO::FETCH_ASSOC)) {
    $json[] = $row;
}

echo json_encode($json, JSON_NUMERIC_CHECK); 

$result->closeCursor();
?>