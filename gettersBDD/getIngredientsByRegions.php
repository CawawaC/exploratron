 <?php

require_once('../config.php');

$regions;
$regions = $_POST["regions"];

if($regions != null) {

	try {
		$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
	} catch (Exception $e) {
	    die('Erreur : ' . $e->getMessage());
	}

	$arr = [];
	foreach ($regions as $region) {
		$sql = "SELECT * FROM ingredients WHERE place='$region'";
		$stmt = $bdd->query($sql); 


		$arrLocal = [];
		$i = 0;
		while($rowLocal = $stmt->fetch(PDO::FETCH_ASSOC)) {
		    $arrLocal[$i] = $rowLocal;
		    $i += 1;
		}

		$arr = array_merge($arr, $arrLocal);
	}

	echo json_encode($arr);
}


?>