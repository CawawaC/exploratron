 <?php

require_once('../../config.php');

try {
	$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
} catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}

$result = $bdd->query("	SELECT * 
						FROM objets 
						ORDER BY id
						LIMIT 1 ");
$row = $result->fetchObject();

echo json_encode($row);


?>