 <?php 

require_once('../config.php');

try{
	$bdd = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
}

catch (Exception $e){
    die('Erreur : ' . $e->getMessage());
}

$result = $bdd->prepare('SELECT texte FROM questions ORDER BY RAND() LIMIT  5');
$result->execute();

$arr = [];
$i = 0;
while($row = $result->fetch(PDO::FETCH_ASSOC)) {
    // $arr[$i] = $row["nom"];
    $arr[$i] = $row["texte"];
    $i += 1;
   // echo $row["texte"];
}


echo json_encode($arr);


$result->closeCursor();
?>