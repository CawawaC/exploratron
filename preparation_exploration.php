<?php 
	include("sessionCheck.php"); // décommenter pour interdire l'accès aux utilisateurs non connectés 
	include("header.php"); 
	$_SESSION['idVoronoiActuel'] = -1;
	$_SESSION['distanceVoronoiActuel'] = -1;
	include("headerNavigation.php"); 
?>   

<script type="text/javascript" src="js/libs/rhill-voronoi-core.min.js"></script>
<script defer type="text/javascript" src="js/carte.js"></script>
<script defer type="text/javascript" src="js/Aventurier.js"></script>
<script defer type="text/javascript" src="js/Pouvoirs.js"></script>
<script defer type="text/javascript" src="js/objets.js"></script>

<script type="text/javascript" src="js/ingredient.js"></script>
<script type="text/javascript" src="js/stats.js"></script>
<script type="text/javascript" src="js/Constants.js"></script>
<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tooltip.js@1"></script>
<script defer type="text/javascript" src="js/page_chambrecoffre.js"></script>
<script defer type="text/javascript" src="js/page_chambrevestiaire.js"></script>
<script defer type="text/javascript" src="js/preparation_exploration.js"></script>

<div class="panneau-lateral " id="equipement">
	<a class="fermeture">X</a>

	<div class="contenu">
		<div id="stats-joueur" class="transparent">
				<div class="contenu">
				<p class="tip">Voici les pouvoirs conférés par votre équipement</p>
				<div id="pouvoirs"></div>
			</div>
		</div>

		<div class="coffre" >
			<section id="stock">
				<div class="conteneur-flex">
					<div class = "conteneur-liste">
						<p id="inventaire-perso"> sur moi</p><p id="remplissage-sac"></p>
						<div style="clear: both;"></div>
						<form id="inventaire">
						</form>
					</div>
					<div>
						<input type="button" id="stocker-coffre" value="&#9654;" disabled="true" />
						<input type="button" id="retirer-coffre" value="&#9664;" disabled="true"/>
					</div>
					<div class = "conteneur-liste">
						<p id="inventaire-coffre">dans mon classeur</p>
						<form id="coffre">
						</form>
					</div>
				</div>
			</section>
		</div>


		<div class="vestiaire" >
			<section id="stock">
				<div class="conteneur-flex">
					<div class = "conteneur-liste">
						<p id="equipement-perso"> sur moi</p>
						<form id="equipement-chambre">
						</form>
					</div>
					<div>
						<input type="button" id="stocker-vestiaire" value="&#9654;" disabled="true" />
						<input type="button" id="retirer-vestiaire" value="&#9664;" disabled="true"/>
					</div>
					<div class = "conteneur-liste">
						<p id="equipement-penderie">dans ma penderie</p>
						<form id="vestiaire-chambre"></form>
					</div>
				</div>
			</section>
		</div>

	</div>
</div>



<div class="ombre cachee"></div>

<div class="colonneCentrale">

	<h2 id="title"></h2>
	<hr>

	<p id="explication" class="tip">Vous allez partir à l'aventure</p>

	<input class="" type="button" id="bouton_equipement" value="" />
	<div id="pouvoirs"></div>

	<p id="question_01">Où veux-tu partir ?</p>

	<?php include("carte.php"); ?>

	<br/><br/>
	<input type="button" id="exploration" value=""/>

</div>

<?php include("footer.php"); ?>