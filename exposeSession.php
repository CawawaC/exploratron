<?php
session_start();
//if(isset($_SESSION['vie'])){
	$ObjSession = (object) [
		'nom' => $_SESSION['nom'],
		'description' => $_SESSION['description'],
	    'vie' => $_SESSION['vie'],
	    'vit' =>  $_SESSION['vit'],
	    'vol' =>  $_SESSION['vol'],
	    'objets'=>unserialize($_SESSION['objets']),
	    'objetsMax'=>$_SESSION['objetsMax'],
	    'ingredientsMax'=>$_SESSION['ingredientsMax'],
	    'idVoronoiActuel'=>$_SESSION['idVoronoiActuel'],
	    'vestiaire'=>unserialize($_SESSION['vestiaire']),
	    'coffre'=>unserialize($_SESSION['coffre']),
	    'chenil'=>unserialize($_SESSION['chenil']),
	    'poeme'=>unserialize($_SESSION['poeme']),
	    'inventaire'=>unserialize($_SESSION['inventaire']),
	    'familiers'=>unserialize($_SESSION['familiers']),
	    'distanceVoronoiActuel'=>$_SESSION['distanceVoronoiActuel'],
	    'tuto'=>unserialize($_SESSION['tuto']),
	    'tutoAutoPopUp'=>$_SESSION['tutoAutoPopUp']
	];

	echo json_encode($ObjSession);
/*}

else
	echo "NULL";*/
?>