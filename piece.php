<?php
require_once('config.php');

$numeroPiece;
$arraySorties;
$x;
$y;
$description;
$sortieDonjon;

if (isset($_GET['numeroPiece']))
	$numeroPiece = $_GET['numeroPiece'];
else
	$numeroPiece = -1;

if (isset($_GET['sorties']))
	$arraySorties = $_GET['sorties'];
else
	$arraySorties = [-1,-1,-1,-1];

if (isset($_GET['x']))
	$x = $_GET['x'];
else
	$x = -1;

if (isset($_GET['y']))
	$y = $_GET['y'];
else
	$y = -1;

if (isset($_GET['description']))
	$description = $_GET['description'];
else
	$description = "";

if (isset($_GET['sortieDonjon']))
	$sortieDonjon = $_GET['sortieDonjon'];
else
	$sortieDonjon = false;
?>


 	<article class="piece <?php echo $numeroPiece; ?>" data-x="<?php echo $x; ?>" data-y="<?php echo $y; ?>">
 		<div class="contenu-description">
 		</div>
 			<div class="contenu-principal">
 				<hr>
			 	<p>
			 		<?php require_once('generation_contenu_piece.php');
			 		echo $description ?>			 		
				</p>
				<hr>
				<br><br>
				<form>
					<input type="button" id="bouton_nord" class="direction nord <?php if($arraySorties[0] == 'false') echo('desactive'); else echo('active'); ?> " value="nord" />
					</br></br>
		            <input type="button" id="bouton_ouest" class="direction ouest <?php if($arraySorties[3] == 'false') echo('desactive'); else echo('active'); ?>" value="ouest" />
		            <span>&nbsp&nbsp&nbsp&nbsp&nbsp</span>
		            <input type="button" id="bouton_est" class="direction est <?php if($arraySorties[2] == 'false') echo('desactive'); else echo('active'); ?>" value="est" />
		            </br> </br> 
		            <input type="button" id="bouton_sud" class="direction sud <?php if($arraySorties[1] == 'false') echo('desactive'); else echo('active'); ?>" value="sud" />
		            <br>
		            <?php 
		            	if($sortieDonjon == 'true') 
		            		echo('</br> <input type="button" id="bouton_sortie" class="direction sortie" value="Sortie" />');
		            ?>
		        </form>
			</div>		
		
		<div id="contenu-cote">
			<div class="contenu-cote-tiret"></div>
			<div class="description"><label id="ingredient"></label>
				<form id="interactions">
				</form>
			</div>
		</div>

	
	</article>

	<script></script>

