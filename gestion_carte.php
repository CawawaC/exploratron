<?php 
 //include("sessionCheck.php"); // décommenter pour interdire l'accès aux utilisateurs non connectés 
  include("header.php"); 
  require_once('config.php');

try{

	// $bdd = new PDO('mysql:host=db4free.net;dbname=db_exploratron;charset=utf8mb4', 'adminexploratron', '28112018');
	$bdd = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.';charset=utf8mb4', DB_USER, DB_PASSWORD);
}

catch (Exception $e){
    die('Erreur : ' . $e->getMessage());
}?>

<script type="text/javascript" src="js/libs/rhill-voronoi-core.min.js"></script>
<script type="text/javascript" src="js/Constants.js"></script>
<script type="text/javascript" src="js/Stats.js"></script>
<script type="text/javascript" src="js/Ingredient.js"></script>
<script type="text/javascript" src="js/carte.js"></script>
<script defer type="text/javascript" src="js/gestion_carte.js"></script>


<a href="hub.php"> retourner au menu</a>

<?php include("carte.php"); ?>

<input type="button" id="reveler" value="Révéler une nouvelle Région">
<input type="button" id="stock" value="Enregistrer carte">
<input type="button" id="ouvrir" value="Ouvrir carte sauvegardée">
<input type="button" id="effacer" value="Effacer la carte sauvegardée">
<input type="button" id="creer" value="Créer une nouvelle carte">
<input type="button" id="masquer" value="Masquer toutes les régions">
<input type="button" id="reveler_centre" value="Réveler les régions centrales">
<input type="button" id="reveler_tout" value="Réveler toutes les régions">


		

<?php include("footer.php"); ?>


<style>
    #container {
      width: 1080px;
      height: 720px;
      overflow: hidden;
      border: 1px solid;
      /*cursor: move;*/
    }
</style>