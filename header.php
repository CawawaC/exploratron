<?php include("config.php"); ?>

<!doctype html>

<html lang="fr">
<head>
  <meta charset="utf-8">

  <title>la maison perchée</title>
  <link rel="icon" type="image/png" href="medias/images/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="medias/images/favicon-32x32.png" sizes="32x32">
  <meta name="description" content="la maison perchée">
  <meta name="author" content="CawawaC/numero15">

  <link rel="stylesheet" href="css/style.css?v=1.0">
 <link href="https://fonts.googleapis.com/css?family=Neuton:200,400,400i,700|Raleway:300,400,700,900" rel="stylesheet"> 
 <script  type="text/javascript" src="js/libs/particles.js"></script>
</head>

<body>

	 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
   <script src="https://unpkg.com/popper.js@1"></script>
   <script src="https://unpkg.com/tooltip.js@1"></script>
   <script src="js/scripts.js"></script>
	 <?php mb_internal_encoding('UTF-8'); ?>
	 <div class="bg-img"></div>
   <div id="particles-js"></div>
   <div id="page">

    <section id='popup' class="ferme" >
    <div class="ombre cacherPopup">
      <div class="conteneur">
        <div class="contenu">
          <h1>titre</h1>
          <p>conentu du pop up</p>
          <input type="button" value="fermer" class="fermerPopUp"/>
        </div>
      </div>
    </div>
  </section>

<!--     <?php   echo '<pre>';
var_dump($_SESSION);
echo '</pre>';?>
 -->
